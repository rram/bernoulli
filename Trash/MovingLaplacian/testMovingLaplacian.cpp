// Sriramajayam

#include <cmath>
#include <algorithm>
#include <unordered_set>
#include <cassert>
#include <array>

// FE utilities from DG++
#include "P12DElement.h"
#include "Laplacian.h"
#include "PlottingUtils.h"
#include "MeshUtils.h"

// Meshing related utilities from UM++
#include "PlanarCircle.h"
#include "Triangulator.h"
#include "StdTriMesh.h"
#include "GeomTri2DQuality.h"
#include "TriVertOptimizer.h"
#include "Plotting.h"


// Returns an equilateral mesh and its mesh size
double GetMesh(CoordConn& MD, const double rIn, const double rOut);

// Returns a mesh for the requested domain
void GetMesh(const Mshr::Codim1Manifold& Geom,
	     const Mshr::Orientation triSide,
	     double hsize, 
	     CoordConn& MD);

// Determines Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const double rin, const double rout,
		     const double gval,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// Computes the functional being optimized
std::array<double, 3> ComputeFunctionals(const std::vector<Element*>& ElmArray,
					const LocalToGlobalMap& L2GMap, 
					 const double gval,
					const double* sol);


  int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create inner boundary, which is fixed
  const double InRad = 0.5;
  Mshr::PlanarCircle InCirc(std::vector<double>({0.,0.}),InRad);

  // Track the functional values
  std::fstream jfile;
  jfile.open((char*)"jvals.dat", std::ios::out);
  
  // Moving outer boundary
  char filename[100];
  for(int snum=0; snum<30; ++snum)
    {
      std::cout<<"\nSimulation number: "<<snum+1; std::fflush( stdout );

      // Create outer boundary, which is moving
      const double OutRad = 2.0 - double(snum)*0.04;
      Mshr::PlanarCircle OutCirc(std::vector<double>({0.,0.}), OutRad);
      
      // Create a background mesh of equilateral triangles
      CoordConn MD;
      const double hsize = GetMesh(MD, InRad, OutRad);
      PlotTecCoordConn("EQ.tec", MD);

      // Recover a mesh with the inner and outer domains removed
      GetMesh(InCirc, Mshr::Orientation::Plus, hsize, MD);
      GetMesh(OutCirc, Mshr::Orientation::Minus, hsize, MD);

      // Plot the mesh
      sprintf(filename, "MD-%02d.tec", snum);
      PlotTecCoordConn(filename, MD);

      // Set global coordinates
      Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
      Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

      // Create elements
      std::vector<Element*> ElmArray(MD.elements);
      for(int e=0; e<MD.elements; ++e)
	ElmArray[e] = new P12DElement<1>(MD.connectivity[3*e],
					 MD.connectivity[3*e+1],
					 MD.connectivity[3*e+2]);

      // Create local to global map
      const StandardP12DMap L2GMap(ElmArray);

      // Create operations
      std::vector<DResidue*> OpArray(MD.elements);
      for(int e=0; e<MD.elements; ++e)
	OpArray[e] = new Laplacian(ElmArray[e], 0);

      // Create PETSc data structures
      PetscData PD;
      InitializePetscData(L2GMap, L2GMap.GetTotalNumDof(), PD);
      VecZeroEntries(PD.solutionVEC);
      VecZeroEntries(PD.DOFArray);
      PC pc;
      KSPGetPC(PD.kspSOLVER, &pc);
      PCSetType(pc, PCLU);
      
      // Get boundary conditions
      const double gval = 1.;
      std::vector<int> boundary;
      std::vector<double> bvalues;
      GetDirichletBCs(MD, InRad, OutRad, gval, boundary, bvalues);

      // Assemble contributions from laplacian
      assert(DResidue::Assemble(OpArray, L2GMap, PD.DOFArray, &PD.resVEC, &PD.stiffnessMAT) &&
	     "Could not assemble matrix-vector system.\n");

      // Set Dirichlet BCs
      SetDirichletBCs(PD, boundary, bvalues);

      // Solve
      Solve(PD);

      // Plot this solution
      double* sol;
      VecScale(PD.solutionVEC, -1.); // Only increment in N-R iteration is computed
      VecGetArray(PD.solutionVEC, &sol);
      sprintf(filename, "sol-%02d.tec", snum);
      PlotTecCoordConnWithNodalFields(filename, MD, sol, 1);

      // Compute the functional values at this configuration
      auto Jvals = ComputeFunctionals(ElmArray, L2GMap, gval, sol);
      jfile<<OutRad<<"\t"<<Jvals[0]<<"\t"<<Jvals[1]<<"\t"<<Jvals[2]<<"\n";
      jfile.flush();
      
      // Clean up this run
      VecRestoreArray(PD.solutionVEC, &sol);
      DestroyPetscData(PD);
      for(int e=0; e<MD.elements; ++e)
	{
	  delete OpArray[e];
	  delete ElmArray[e];
	}
    }

  jfile.close();
  PetscFinalize();
  std::cout<<"\n---done---\n"; std::fflush( stdout );
  
}  

// Computes the functional being optimized
std::array<double, 3> ComputeFunctionals(const std::vector<Element*>& ElmArray,
					const LocalToGlobalMap& L2GMap,
					 const double gval, const double* sol)
{
  std::array<double, 3> Jvals({0.,0.,0.});
  const unsigned int nElements = int(ElmArray.size());
  const int field = 0;
  for(unsigned int e=0; e<nElements; ++e)
    {
      const auto* Elm = ElmArray[e];
      const int nShapes = Elm->GetDof(0);
      
      // Quadrature  weights here
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const unsigned int nQuad = Qwts.size();
      for(unsigned int q=0; q<nQuad; ++q)
	{
	  // Compute the field and its derivative here
	  double Uval = 0.;
	  double gradU[2] = {0.,0.};
	  for(int a=0; a<nShapes; ++a)
	    {
	      Uval += sol[L2GMap.Map(field,a,e)]*Elm->GetShape(field,q,a);
	      for(int J=0; J<2; ++J)
		gradU[J] += sol[L2GMap.Map(field,a,e)]*Elm->GetDShape(field,q,a,J);
	    }
	  
	  // Update functionals
	  Jvals[0] += Qwts[q]*(gradU[0]*gradU[0] + gradU[1]*gradU[1]);
	  Jvals[1] += Qwts[q]*gval*gval;
	}
    }
  Jvals[2] = Jvals[0] + Jvals[1];
  return Jvals;
}


double GetMesh(CoordConn& MD, const double rIn, const double rOut)
{
  assert(rIn<rOut && "GetMesh()- Expected inner radius to be smaller than outer.\n");
  
  MD.coordinates.reserve(7*2);
  MD.coordinates.push_back(0.);
  MD.coordinates.push_back(0.);
  const double rad = 1.1*rOut/sin(M_PI/3.);
  for(auto i=0; i<6; ++i)
    {
      MD.coordinates.push_back(rad*cos(double(i)*M_PI/3.));
      MD.coordinates.push_back(rad*sin(double(i)*M_PI/3.));
    }
  MD.coordinates.shrink_to_fit();
  std::vector<int> conn({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.connectivity = std::move(conn);
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 7;
  MD.elements = 6;

  // Mesh size required
  double h1 = rIn/2.;
  double h2 = (rOut-rIn)/6.;
  double hreq = (h1<h2) ? h1 : h2;
  int ndiv = std::ceil( log2(rad/hreq) );
  double hsize = rad;
  for(auto i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      hsize /= 2.;
    }
  assert(ndiv<7 && "Performing more than 7 subdivisions?\n");
  return hsize;
}


// Function to compute the relaxation direction at a vertex during mesh optimization
void fRelaxDir(const int iternum, const int nodenum, const Mshr::WMWorkspace& WmWs,
	       void* usrparams, double* rdir)
{
  if(iternum%2==0)
    { rdir[0] = 0.; rdir[1] = 1.; }
  else
    { rdir[0] = 1.; rdir[1] = 0.; }
}


// Function to identify vertices that need to be relaxed
void fRelaxNbd(const Mshr::WMWorkspace& WmWs,
	       const Mshr::Orientation triSide,
	       void* params, 
	       std::vector<int>& vertices)
{
  vertices.clear();

  // Get the mesh size
  const double hsize = *((double*)params);
  
  // Sign of SD that is relevant
  const Mshr::SDSignature trisign = (triSide==Mshr::Orientation::Minus) ? Mshr::SDSignature::Minus : Mshr::SDSignature::Plus;
  
  // Signed distances at vertices of the working mesh
  const auto& SD = WmWs.Get();
  for(auto& it:SD)
    if(it.second.sign==trisign)
      if(std::abs(it.second.value)<5.*hsize)
	vertices.push_back(it.first);
}


// Convert a working mesh to a background mesh format
void ConvertWorkingMesh(const Mshr::WorkingMesh& WM,
			std::vector<double>& Coord, 
			std::vector<int>& Conn)
{
  const auto& ElmRanges = WM.GetElements().GetIntervals();
  const int nIntervals = int(ElmRanges.size())/2;
    
  // Mapping from old to new node numbers
  int nElements = 0;
  std::unordered_map<int, int> Old2NewNumMap;
  for(int i=0; i<nIntervals; ++i)
    for(int e=ElmRanges[2*i]; e<=ElmRanges[2*i+1]; ++e)
      {
	++nElements;
	const auto* elmconn = WM.connectivity(e);
	for(int a=0; a<3; ++a)
	  Old2NewNumMap[elmconn[a]] = -1;
      }

  int nnodes = 0;
  for(auto& it:Old2NewNumMap)
    it.second = nnodes++;

  // Insert coordinates with new node numbers
  Coord.resize(2*nnodes);
  for(auto& it:Old2NewNumMap)
    {
      const auto& oldnum = it.first;
      const auto& newnum = it.second;
      const auto* coord = WM.coordinates(oldnum);
      for(int k=0; k<2; ++k)
	Coord[2*newnum+k] = coord[k];
    }
  
  // Insert elements with new node numbers
  Conn.reserve(3*nElements);
  for(int i=0; i<nIntervals; ++i)
    for(int e=ElmRanges[2*i]; e<=ElmRanges[2*i+1]; ++e)
      {
	const auto* elmconn = WM.connectivity(e);
	for(int a=0; a<3; ++a)
	  Conn.push_back( Old2NewNumMap[elmconn[a]] );
      }
  Conn.shrink_to_fit();
}
	


// Returns a mesh for the requested domain
void GetMesh(const Mshr::Codim1Manifold& Geom,
	     const Mshr::Orientation triSide,
	     double hsize,
	     CoordConn& MD)
{
  // Number nodes from 0
  for(auto& it:MD.connectivity)
    it = it-1;
  
  // Create background mesh in UM++ format
  Mshr::SeqCoordinates Coord(2, MD.coordinates);
  Mshr::StdTriConnectivity Conn(MD.connectivity);

  // Background mesh
  Mshr::StdTriMesh BG(Coord, Conn);

  // Workspace for background mesh: compute signed distances
  Mshr::UMWorkspace BgWs(BG, Geom);
  const auto nnodes = BG.GetNumNodes();
  for(unsigned int i=0; i<nnodes; ++i)
    BgWs.Evaluate(i);
  				  
  // Working mesh and its workspace
  Mshr::WorkingMesh WM(BG);
  Mshr::WMWorkspace WmWs(WM, Geom);

  // Create quality metric
  Mshr::GeomTri2DQuality<Mshr::WorkingMesh> Quality(WM);

  // Create triangulation optimizer
  Mshr::TriWMVertOptimizer TriOpt(WM, Quality);

  // Helper struct for optimization parameters
  const int nIterations = 20;
  std::function<decltype(fRelaxDir)> funcRelaxDir(&fRelaxDir);
  std::function<decltype(fRelaxNbd)> funcRelaxNbd(&fRelaxNbd);
  Mshr::TriOptParams TOPs({TriOpt, nIterations, &hsize, funcRelaxDir, funcRelaxNbd});

  // Input parameters for the triangulator
  const int nSteps = 3;
  Mshr::TriParams inParams(BgWs, nSteps, triSide, TOPs);

  // Output parameters from the triangulator
  std::vector<std::array<int,2>> PosElmsFaces;
  Mshr::TriResult outParams({WM, WmWs, PosElmsFaces});

  // Triangulate
  Mshr::TriangulateDomain(inParams, outParams);
  //Mshr::PlotTecWorkingMesh("WM.tec", 3, WM);

  // Convert working mesh to a background mesh
  MD.connectivity.clear();
  MD.coordinates.clear();
  ConvertWorkingMesh(WM, MD.coordinates, MD.connectivity);
  MD.nodes = MD.coordinates.size()/2;
  MD.elements = MD.connectivity.size()/3;
  
  // Number nodes starting from 1
  for(auto& it:MD.connectivity)
    it = it+1;
}

// Determines Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const double rin, const double rout,
		     const double gval, 
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  // Get the neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on boundary faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert( MD.connectivity[3*e+(f+i)%3]-1 );

  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  double rin2 = rin*rin;
  double rout2 = rout*rout;
  assert(rin2<rout2 && "GetDirichletBCs()- Inner radius larger than outer radius.\n");
  double rEPS = (rout2-rin2)/3.;
  for(auto& it:bdnodes)
    {
      const auto& x = MD.coordinates[2*it];
      const auto& y = MD.coordinates[2*it+1];
      double r2 = x*x+y*y;
      boundary.push_back( it );

      // Is this the inner or the outer boundary
      if(std::abs(r2-rin2)<rEPS)
	bvalues.push_back( 0. );
      else if(std::abs(r2-rout2)<rEPS)
	bvalues.push_back(gval);
      else
	assert(false && "GetDirichletBCs()- Boundary node does not lie on boundary.\n");
    }
  bvalues.shrink_to_fit();
  boundary.shrink_to_fit();
}
