// Sriramajayam

#include <MaxEnt.h>
#include <cmath>
#include <cassert>
#include <iostream>
#include <fstream>

int main()
{
  // Create a uniformly spaced node set
  std::vector<double> NodeSet;
  for(int i=0; i<=5; ++i)
    NodeSet.push_back( static_cast<double>(i) );
  const double gamma = 0.8;
  const int SPD = 1;
  const double Tol0 = 1.e-6;
  const double TolNR = 1.e-8;
  MaxEnt ME(SPD, gamma, NodeSet, Tol0, TolNR);

  // Check inputs
  assert(ME.GetSpatialDimension()==SPD);
  assert(std::abs(ME.GetFunctionTolerance()-Tol0)<1.e-10);
  assert(std::abs(ME.GetResidualTolerance()-TolNR)<1.e-10);
  assert(ME.GetNumFunctions()==static_cast<int>(NodeSet.size()));
  assert(ME.GetNumNodes()==static_cast<int>(NodeSet.size()));
  const auto& NS = ME.GetNodeSet();
  for(int i=0; i<ME.GetNumNodes(); ++i)
    assert(std::abs(NodeSet[i]-NS[i])<1.e-10);
  
  // Print the number of associated functions
  const auto nFuncs = ME.GetNumFunctions();
  
  // Check function evaluations
  std::vector<double> P(nFuncs);
  std::fstream pfile;
  pfile.open("maxent.dat", std::ios::out);
  for(int i=1; i<=99; ++i)
    {
      double x = 5.*static_cast<double>(i)/100.;
      //std::cout<<"\nx = "<<x; std::fflush( stdout );
      ME.Evaluate(&x, &P[0]);
      pfile<<x;
      for(int a=0; a<nFuncs; ++a)
	pfile<<" "<<P[a];
      pfile<<"\n";
    }
  pfile.flush(); pfile.close();
}
