// Sriramajayam

#include <bp_MaxEntCurveFit.h>
#include <cassert>
#include <iostream>
#include <PlottingUtils.h>
#ifdef _OPENMP
#include <omp.h>
#endif

using namespace bp;
using namespace geom;

// Constructor
MaxEntCurveFit::MaxEntCurveFit(const MaxEntCurveFitParams& FitParams)
  :SPD(2) //, Fit_Params(FitParams)
{
  Initialize(FitParams);
  ComputeDofs(FitParams);
}

// Helper to initialize all members and compute dofs
void MaxEntCurveFit::Initialize(const MaxEntCurveFitParams& FitParams)
{
  // Check values of input parmeters
  assert(FitParams.Tol0>0. && "bp::MaxEntCurveFit: unexpected Tol0");
  assert(FitParams.TolNR>0. && "bp::MaxEntCurveFit: unexpected TolNR");
  assert(FitParams.nnb>=0 && "bp::MaxEntCurveFit: unexpected nnb");
  assert(FitParams.gamma>1.e-3 && "bp::MaxEntCurveFit: unexpected gamma");
  assert(FitParams.alpha>1.e-5 && "bp::MaxEntCurveFit: unexpected value for alpha");
  assert(FitParams.nMaxThreads>0 && "bp::MaxEntCurveFit: unexpected value for nMaxThreads");
#ifndef _OPENMP
  assert(FitParams.nMaxThreads==1 && "bp::MaxEntCurveFit: unexpected value for nMaxThreads");
#endif
  assert(FitParams.ElmArray!=nullptr && "bp::MaxEntCurveFit: ElmArray is null");
  assert(FitParams.PC!=nullptr && "bp::MaxEntCurveFit: pointer to point cloud is null");
  assert(FitParams.normals!=nullptr && "bp::MaxEntCurveFit: pointer to normals is null");
  assert(FitParams.normals->size()==FitParams.PC->size() && "bp::MaxEntCurveFit: inconsistent length of points & normals");
  assert(FitParams.nodeset!=nullptr && "bp::MaxEntCurveFit: pointer to nodeset is null");

  // Bounding box for the node set
  const auto& NS = *FitParams.nodeset;
  const int nNodes = static_cast<int>(NS.size()/SPD);
  assert(nNodes>0 && "bp::MaxEntCurveFit: unexpected number of max-ent nodes");
  double xmin = NS[0], xmax = NS[0], ymin = NS[1], ymax = NS[1];
  for(int n=1; n<nNodes; ++n)
    {
      const auto& x = NS[SPD*n];
      const auto& y = NS[SPD*n+1];
      if(x<xmin) xmin = x;
      if(x>xmax) xmax = x;
      if(y<ymin) ymin = y;
      if(y>ymax) ymax = y;
    }

  // Max-ent function parameters
  MaxEnt2DFuncParams FuncParams;
  FuncParams.Tol0 = FitParams.Tol0;
  FuncParams.TolNR = FitParams.TolNR;
  FuncParams.nnb = FitParams.nnb;
  FuncParams.gamma = FitParams.gamma;
  FuncParams.bbcenter[0] = 0.5*(xmin+xmax);
  FuncParams.bbcenter[1] = 0.5*(ymin+ymax);
  // Add 10% buffer for search tree size
  FuncParams.bbsize = ((xmax-xmin)>(ymax-ymin)) ? 1.1*(xmax-xmin) : 1.1*(ymax-ymin);
  FuncParams.NodeSet.assign(NS.begin(), NS.end());
  FuncParams.nNodes = nNodes;

  // Create max-ent functions
  if(FitParams.verbose) std::cout<<"\nCreating max ent functions: "<<std::flush;
  ME = new MaxEnt2D(FuncParams, FitParams.nMaxThreads);

  // Setup search tree
  auto* stree = ME->GetTree();
  stree->Subdivide(NS);
  ME->SetupTree();

  // Create thread-safe data structures
  if(FitParams.verbose) std::cout<<"\nCreating thread-safe data structures: "<<std::flush;
  WrkSpc.resize(FitParams.nMaxThreads);
  const int nDof = nNodes;
  for(int thrnum=0; thrnum<FitParams.nMaxThreads; ++thrnum)
    {
      auto& ws = WrkSpc[thrnum];
      ws.kvalues.resize(nDof);
      ws.fvalues.resize(nDof);
      ws.dfvalues.resize(SPD*nDof);
      ws.d2fvalues.resize(SPD*SPD*nDof);
    }
  return;
}




// Helper method for computing dofs for fitting
void MaxEntCurveFit::ComputeDofs(const MaxEntCurveFitParams& FitParams)
{
  assert(ME!=nullptr);
  const int nDof = ME->GetNumFunctions();

  // Create PETSc data structures
  if(FitParams.verbose) std::cout<<"\nSetting up PETSc data structures: "<<std::flush;
  PetscData PD;
  std::vector<int> nnz(nDof);
  for(int i=0; i<nDof; ++i)
    { nnz[i] = static_cast<int>(ME->GetSupport(i).size());
      assert(nnz[i]>0 && "bp::MaxEntCurveFit::ComputeDofs- 0 support"); }
  PD.Initialize(nnz);
  
  // Zero out the residue
  PetscErrorCode ierr;
  ierr = VecZeroEntries(PD.resVEC); CHKERRV(ierr);

  // Set the sparsity pattern for the stiffness
  // Initialize expected fillin with 0s
  ierr = MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
  std::vector<double> zeros(nDof, 0.);
  for(int i=0; i<nDof; ++i)
    {
      const auto& supp = ME->GetSupport(i);
      ierr = MatSetValues(PD.stiffnessMAT, 1, &i, supp.size(), &supp[0], &zeros[0], INSERT_VALUES);
      CHKERRV(ierr);
    }
  ierr = MatAssemblyBegin(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  ierr = MatAssemblyEnd(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

  // Freeze the sparsity.
  // Violations occur because of inaccurate interations found in the quadtree
  ierr = MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_FALSE); CHKERRV(ierr);

  // Assemble the contribution from the hessian
  if(FitParams.verbose) std::cout<<"\nAssembling hessian contributions: "<<std::flush;
  AssembleHessian(FitParams, PD);

  // Assemble point-wise contributions
  if(FitParams.verbose) std::cout<<"\nAssembling pointwise constraints: "<<std::flush;
  AssemblePointConstraints(FitParams, PD);

  // Set the solver
  PC pc;
  ierr = KSPGetPC(PD.kspSOLVER, &pc); CHKERRV(ierr);
  ierr = PCSetType(pc, PCLU); CHKERRV(ierr);

  // Solve
  if(FitParams.verbose) std::cout<<"\nSolving: "<<std::flush;
  PD.Solve();
  double* sol;
  ierr = VecGetArray(PD.solutionVEC, &sol); CHKERRV(ierr);
  dofs.assign(sol, sol+nDof);
  ierr = VecRestoreArray(PD.solutionVEC, &sol); CHKERRV(ierr);

  // Clean up
  PD.Destroy();
  return;
}


// Return the local workspace
bp::detail::MaxEntCurveWorkspace& MaxEntCurveFit::GetLocalWorkspace() const
{
  int thrnum = 0;
  #ifdef _OPENMP
  thrnum = omp_get_thread_num();
  assert(thrnum<static_cast<int>(WrkSpc.size()));
  #endif
  return WrkSpc[thrnum];
}


// Assemble contributions from point constraints
void MaxEntCurveFit::AssemblePointConstraints(const MaxEntCurveFitParams& FitParams,
					      PetscData& PD) const
{
  const int nDof = ME->GetNumFunctions();
  const int nPoints = static_cast<int>(FitParams.PC->size()/SPD);
  const double* points = &(*FitParams.PC)[0];
  const double* normals = &(*FitParams.normals)[0];

  // Locks for assembly
  std::vector<omp_lock_t> rowlocks(nDof);
  for(int i=0; i<nDof; ++i)
    omp_init_lock(&rowlocks[i]);

  // Assemble residual at the end
  std::vector<double> rVec(nDof);
  std::fill(rVec.begin(), rVec.end(), 0.);
  
#pragma omp parallel default(shared)
  {
    // Local data structures for assembly
    auto& ws = GetLocalWorkspace();
    auto* fvalues = &ws.fvalues[0];
    auto* dfvalues = &ws.dfvalues[0];
    auto* colvalues = &ws.kvalues[0];
    double rval;
    int row, nFuncs;

    // Loop over points, accummulate contributions
#pragma omp for
    for(int p=0; p<nPoints; ++p)
      {
	// Normal here
	const auto* mynormal = &normals[SPD*p];
	
	// Compute shape functions and derivatives
	const auto& suppvec = ME->Evaluate(&points[SPD*p], fvalues, dfvalues);
	nFuncs = static_cast<int>(suppvec.size());
	for(int i=0; i<nFuncs; ++i)
	  {
	    row = suppvec[i];

	    // Residual
	    rval = 0.;
	    for(int k=0; k<SPD; ++k)
	      rval += dfvalues[SPD*i+k]*mynormal[k];
	    

	    // Stiffness
	    for(int j=0; j<nFuncs; ++j)
	      {
		colvalues[j] = fvalues[i]*fvalues[j];
		for(int k=0; k<SPD; ++k)
		  colvalues[j] += dfvalues[SPD*i+k]*dfvalues[SPD*j+k];
	      }
	    
	    // Start assembly for this row
	    omp_set_lock(&rowlocks[row]);
	    rVec[row] += rval;
	    MatSetValues(PD.stiffnessMAT, 1, &row, nFuncs, &suppvec[0], colvalues, ADD_VALUES);
	    omp_unset_lock(&rowlocks[row]);
	  }
      }
  }
  
  // delete locks
  for(int i=0; i<nDof; ++i)
    omp_destroy_lock(&rowlocks[i]);
  
  // Assemble the residual
  std::vector<int> indx(nDof);
  for(int i=0; i<nDof; ++i) indx[i] = i;
  PetscErrorCode ierr;
  ierr = VecSetValues(PD.resVEC, nDof, &indx[0], &rVec[0], ADD_VALUES); CHKERRV(ierr);

  // Finish up assembly
  ierr = VecAssemblyBegin(PD.resVEC); CHKERRV(ierr);
  ierr = VecAssemblyEnd(PD.resVEC); CHKERRV(ierr);
  ierr = MatAssemblyBegin(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  ierr = MatAssemblyEnd(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  
  // Done
  return;
}

// Helper method to compute contribution from hessian
void MaxEntCurveFit::AssembleHessian(const MaxEntCurveFitParams& FitParams,
				     PetscData& PD) const
{
  // Integrate over elements
  const auto& ElmArray = *FitParams.ElmArray;
  const int nElements = static_cast<int>(ElmArray.size());
  const double& alpha = FitParams.alpha;

  // Ensure that all rows are filled
  const int nDof = ME->GetNumFunctions();
  std::vector<bool> rowFilled(nDof, false);

  // Create locks for row-wise assembly
  std::vector<omp_lock_t> rowlocks(nDof);
  for(int i=0; i<nDof; ++i)
    omp_init_lock(&rowlocks[i]);

#pragma omp parallel default(shared)
  {
    // get the local workspace and values
    auto& ws = GetLocalWorkspace();
    auto* fvalues = &ws.fvalues[0];
    auto* d2fvalues = &ws.d2fvalues[0];
    auto* colvalues = &ws.kvalues[0];
    int row, nFuncs, nQuad;

#pragma omp for
    for(int e=0; e<nElements; ++e)
      {
	const auto* Elm = ElmArray[e];
	const auto& Qpts = Elm->GetIntegrationPointCoordinates(0);
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	nQuad = static_cast<int>(Qwts.size());

	for(int q=0; q<nQuad; ++q)
	  {
	    const auto* qpt = &Qpts[SPD*q];
	    const auto& qwt = Qwts[q];
	    
	    // Max-ent shape functions and hessians at this quadrature point
	    const auto& suppvec = ME->Evaluate(qpt, fvalues, nullptr, d2fvalues);
	    nFuncs = static_cast<int>(suppvec.size());

	    // Update stiffness
	    for(int a=0; a<nFuncs; ++a)
	      {
		row = suppvec[a];
		const auto* d2Na = &d2fvalues[SPD*SPD*a];
		for(int b=0; b<nFuncs; ++b)
		  {
		    const auto* d2Nb = &d2fvalues[SPD*SPD*b];
		    colvalues[b] = 0.;
		    for(int k=0; k<SPD*SPD; ++k)
		      colvalues[b] += d2Na[k]*d2Nb[k];
		    colvalues[b] *= alpha*qwt;
		  }
		// Assemble this row
		omp_set_lock(&rowlocks[row]);
		rowFilled[row] = true;
		MatSetValues(PD.stiffnessMAT, 1, &row, nFuncs, &suppvec[0], colvalues, ADD_VALUES);
		omp_unset_lock(&rowlocks[row]);
	      }
	  }
      }
  }

  // Destroy locks
  for(int i=0; i<nDof; ++i)
    omp_destroy_lock(&rowlocks[i]);

  // Check that all rows have been filled
  for(int i=0; i<nDof; ++i)
    assert(rowFilled[i] && "bp::AssembleHessian- unfilled row");

  // Finish up assembly
  PetscErrorCode ierr;
  ierr = MatAssemblyBegin(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  ierr = MatAssemblyEnd(PD.stiffnessMAT, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  
  // Done
  return;
}



// Evaluates the implicit function a point
void MaxEntCurveFit::Evaluate(const double* X, double& val,
			      double* dval, double* d2val) const
{
  // Get the local workspace
  auto& ws = GetLocalWorkspace();
  double* fvalues = &ws.fvalues[0];
  double* dfvalues = (dval!=nullptr) ? &ws.dfvalues[0] : nullptr;
  double* d2fvalues = (d2val!=nullptr) ? &ws.d2fvalues[0] : nullptr;
  const auto& suppvec = ME->Evaluate(X, fvalues, dfvalues, d2fvalues);
  const int nFuncs = static_cast<int>(suppvec.size());

  // Initialize values
  val = 0.;
  if(dval!=nullptr)
    for(int k=0; k<SPD; ++k) dval[k] = 0.;
  if(d2val!=nullptr)
    for(int k=0; k<SPD*SPD; ++k) d2val[k] = 0.;

  // Compute function and derivative values
  for(int i=0; i<nFuncs; ++i)
    {
      const auto& dofval = dofs[suppvec[i]];
      val += dofval*fvalues[i];
      if(dval!=nullptr)
	for(int k=0; k<SPD; ++k)
	  dval[k] += dofval*dfvalues[SPD*i+k];
      if(d2val!=nullptr)
	for(int k=0; k<SPD*SPD; ++k)
	  d2val[k] += dofval*d2fvalues[SPD*SPD*i+k];
    }
  return;
}


// Plot contouts of the solutoin
void MaxEntCurveFit::PlotTec(const char* filename, const CoordConn& MD) const
{
  // Evaluate function at the nodes
  std::vector<double> fvals(MD.nodes);
#pragma omp parallel for default(shared)
  for(int n=0; n<MD.nodes; ++n)
    Evaluate(&MD.coordinates[SPD*n], fvals[n]);

  PlotTecCoordConnWithNodalFields(filename, MD, &fvals[0], 1);
  return; 
}
    
