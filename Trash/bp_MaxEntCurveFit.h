// Sriramajayam

#ifndef BP_MAX_ENT_CURVE_FIT_H
#define BP_MAX_ENT_CURVE_FIT_H

#include <geom_MaxEnt.h>
#include <Element.h>
#include <External.h>
#include <PetscData.h>

namespace bp
{
  //! Helper struct to encapsulate data being passed
  struct MaxEntCurveFitParams
  {
    // Default constructor
    inline MaxEntCurveFitParams() {}

    // Disable copy & assignment
    MaxEntCurveFitParams(const MaxEntCurveFitParams&) = delete;
    MaxEntCurveFitParams& operator=(const MaxEntCurveFitParams&) = delete;

    double Tol0, TolNR; // Tolerances for max-ent functions
    int nnb; // Number of neighbors to use for distance calculations
    double gamma; // Factor controlling decay of max-ent functions
    double alpha; // Scaling factor for the hessian
    const std::vector<double>* nodeset; // Point cloud defining nodes for max-ent functions
    const std::vector<Element*>* ElmArray; // Triangle mesh elements to use for integration
    const std::vector<double>* PC; // point cloud to which to fit function
    const std::vector<double>* normals; // Normals corresponding PC
    int nMaxThreads; // Number of threads on which to assemble
    bool verbose; // Print details of what is being computed
  };
  
  namespace detail
    
  {
    // Helper struct for function evaluations
    struct MaxEntCurveWorkspace
    {
      std::vector<double> kvalues; // to aid in assembly
      std::vector<double> fvalues; // max-ent function values
      std::vector<double> dfvalues; // derivatives of max-ent functions
      std::vector<double> d2fvalues; // 2nd derivatives of max-ent functions
    };
  }

  // Class for fitting smoothed signed distance function using max-ent bases
  // to a given point cloud + normals
  class MaxEntCurveFit
  {
  public:
    //! \param[in] str Fitting parameters
    MaxEntCurveFit(const MaxEntCurveFitParams& str);

    //! Default destructor
    inline virtual ~MaxEntCurveFit() { delete ME; }

    //! Disable copy and assignment
    MaxEntCurveFit(const MaxEntCurveFit&) = delete;
    MaxEntCurveFit& operator=(const MaxEntCurveFit&) = delete;

    // returns the max-ent functions being used
    inline const geom::MaxEnt2D& GetMaxEnt2D() const
    { return* ME; }

    // returns the number of dofs
    inline int GetNumDofs() const
    { return ME->GetNumFunctions(); }

    // Evaluates the implicit function a point
    void Evaluate(const double* X, double& val,
		  double* dval=nullptr, double* d2val=nullptr) const;

    // Plot contouts of the solution
    void PlotTec(const char* filename, const CoordConn& MD) const;
    
  private:
    // Methods
    // Helper to initialize all members and compute dofs
    void Initialize(const MaxEntCurveFitParams& FitParams);

    // Helper method to assemble contributions to the stiffness from point data
    void AssemblePointConstraints(const MaxEntCurveFitParams&, PetscData& PD) const;

    // Helper method to compute contribution from hessian
    void AssembleHessian(const MaxEntCurveFitParams&, PetscData& PD) const;

    // Helper method to compute dofs
    void ComputeDofs(const MaxEntCurveFitParams&);
    
    // Access to the local workspace
    detail::MaxEntCurveWorkspace& GetLocalWorkspace() const;
    
    // Members
    const int SPD; // spatial dimension
    geom::MaxEnt2D* ME; // Max-ent function object
    std::vector<double> dofs; // Computed dof values
    mutable std::vector<detail::MaxEntCurveWorkspace> WrkSpc; // Workspace for function evaluations
  };
}

#endif
