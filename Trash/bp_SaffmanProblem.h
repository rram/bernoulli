// Sriramajayam

#ifndef BP_SAFFMAN_PROBLEM_H
#define BP_SAFFMAN_PROBLEM_H

#include <bp_BernoulliProblem2D.h>

namespace bp
{
  // Assumes a rectangular domain.
  // Assumes that boundary nodes propagate along the X-axis
  class SaffmanProblem: BernoulliProblem2D
  {
  public:
    //! Constructor
    //! \param[in] center Domain center
    //! \param[in] sizes Domain sizes along x and y directions
    //! \param[in] bg Background mesh to be used
    //! \param[in] bdpts Initial guess for outer boundary points. Copied
    //! \param[in] bdnormals Initial guess for outer boundary normals. Copied
    //! \param[in] opt1 Options for boundary approximation with max-ent functions
    //! \param[in] opt2 Options for meshing
    //! \param[in] opt3 Nonlinear-closest-point-solver parameters for an implicit geometry
    //! \param[in] nodeset Node set for max-ent functions
    SaffmanProblem(const double* center,
		   const double* sizes,
		   msh::StdTriMesh& bg,
		   std::vector<double>& bdpts,
		   std::vector<double>& bdnormals,
		   const SSD_Options& opt1,
		   const Meshing_Options& opt2,
		   const geom::NLSolverParams& opt3);

    //! Destructor
    virtual ~SaffmanProblem();

    //! Disable copy and assignment constructor
    SaffmanProbelm(const SaffmanProblem&) = delete;
    SaffmanProblem& operator=(const SaffmanProblem&) = delete;

    //! Update the boundary location
    //! \param[in] f_perturb Function that returns the perturbation magnitude
    virtual void UpdateBoundary(std::function<double(const double* X,
						     const double& dudn, void*)> f_perturb,
				void* params) override;

  protected:
    //! Helper function to setup the geometry and mesh
    virtual void SetupBoundaryAndMesh() override;
    
    // Helper function that updates the boundary given
    // the perturbations at the +ve vertices
    void UpdateBoundary_(const std::map<int, double>& pertMap);

  private:
    const double domain_center[2]; //!< Center of the simulation domain
    const double domain_sizes[2]; //!< Sizes of the simulation domain along x and y
  };
}

#endif
