// Sriramajayam

#ifndef BP_MAX_ENT_CURVE_H
#define BP_MAX_ENT_CURVE_H

#include <geom_LevelSetManifold.h>
#include <bp_MaxEntCurveFit.h>


namespace bp
{
  namespace detail
  {
    // Implicit function evaluation for 2D max-ent smoothed signed distance
    void MaxEntCurveLevelSetFunction(const double* X, double& fval,
				     double* dfval=nullptr, double* d2fval=nullptr,
				     void* params=nullptr);
  }

  //! Implementation for a curve defined by am implicit function defined
  //! as a linear combination of max-ent functions.
  class ImplicitMaxEntCurve: public geom::LevelSetManifold<2>
  {
  public:
    //! Constructor
    //! \param[in] params Nonlinear solver parameters
    //! \param[in] fit Curve fit
    //! \param[in] nmaxthr Max number of threads
  ImplicitMaxEntCurve(const geom::NLSolverParams params,
		      MaxEntCurveFit& fit, 
		      const int nmaxthr=1)
    :geom::LevelSetManifold<2>(params, bp::detail::MaxEntCurveLevelSetFunction, &fit, nmaxthr) {}
    
    //! Destructor
    inline virtual ~ImplicitMaxEntCurve() {}
    
    //! Copy constructor
    //! \param[in] obj Object to be copied
    inline ImplicitMaxEntCurve(const ImplicitMaxEntCurve& obj)
      :geom::LevelSetManifold<2>(obj) {}

    //! Cloning
    inline virtual ImplicitMaxEntCurve* Clone() const override
    { return new ImplicitMaxEntCurve(*this); }

    //! Name of this surface
    inline virtual std::string GetManifoldName() const override
    { return "Implicit-max-ent-curve"; }

  }; 
  
}


#endif

