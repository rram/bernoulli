// Sriramajayam

#include <mx_MaxEnt.h>
#include <bp_Bernoulli2DModule>
#include <geom_LevelSetManifold.h>
#include <MeshUtils.h>
#include <omp.h>

// Namespaces intentionally omitted for clarity

// Create a mesh of equilateral triangles over a hexagon with unit size.
// Its vertices will be the max-ent nodes
// connectivity: numbered from 0
double GetEquilateralMesh(const int nDiv,
			  std::vector<double>& coordinates,
			  std::vector<int>& connectivity);

// Level set function for inner circle
void InnerCircle(const double* X, double& F, double* dF, double* d2F, void* params);

// Solution details
struct SolDetails
{
  const double R0 = 0.2; // Fixed inner radius
  const double Reps = 0.1; // tolerance for distinguishing inner & outer boundaries
  const double U0 = 1.; // Dirichlet bcs on inner boundary
  const double U1 = 0.; // Dirichlet bcs on outer boundary
  const double fval = 4.; // Forcing term in the state problem
  const double gval = -2.0271851144019437; // Target boundary flux
} sol_details;

// Constant force field
bool ConstForceFunc(const std::vector<double> u, const std::vector<double> X,
		    std::vector<double>* f, std::vector<double>* df, void* params)
{ if(f->size()==0) f->resize(1);
  (*f)[0] = -1.*sol_details.fval; return true; } // -ve sign is a hack


// Dirchlet bcs
void DirichletBCs(const CoordConn& MD,
		  std::vector<int>& boundary, std::vector<double>& bvalues,
		  void* params);

// Boundary update function
double BoundaryUpdate(const double* X, const double& dudn, void* params);

// plot boundary points
void PlotPoints(const std::string filename, const std::vector<double>& BdPoints);

// Plot boundary fluxes
void PlotFluxes(const std::string filename, const CoordConn& MD,
		const std::map<int, double>& BdFluxes,
		const std::vector<int>& PosVerts);


int main(int argc, char** argv)
{
  // Spatial dimension
  constexpr int SPD = 2;
  
  // Intialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  int nThreads = 1;
  omp_set_num_threads(1);

  // Nonlinear geometrical solver parameters
  geom::NLSolverParams geom_nlparams({
      .ftol=1.e-6,
	.ttol=1.e-6,
	.max_iter=25});

  // Meshing parameters
  bp::Meshing_Options mesh_options({
      .nMaxThreads=1,
	.MaxVertValency=6,
	.MaxElmValency=6,
	.nSteps=5,
	.nIters=8});
  
  // Create an equilateral mesh to serve as the background mesh
  std::vector<double> Eq_coordinates;
  std::vector<int> Eq_connectivity;
  const double meshsize = GetEquilateralMesh(5, Eq_coordinates, Eq_connectivity);
  for(auto& x:Eq_coordinates) x*=0.95;
  msh::SeqCoordinates EqCoord(2, Eq_coordinates);
  msh::StdTriConnectivity EqConn(Eq_connectivity);
  //msh::StdTriMesh EqTri(EqCoord, EqConn);
  //msh::PlotTecStdTriMesh("Eq.tec", EqTri);

  // Level set function for inner circle
  //double inner_rad = sol_details.R0;
  //geom::LevelSetFunction f_inner_circ = InnerCircle;
  //geom::LevelSetManifold<SPD> Geom_inner_circ(geom_nlparams, f_inner_circ, &inner_rad, nThreads);
  
  // Create a background mesh with the inner circle extracted
  //std::vector<double> bg_coordinates;
  //std::vector<int> bg_connectivity;
  //bp::GetConformingMesh<SPD>(EqTri, Geom_inner_circ, mesh_options,
  //bg_coordinates, bg_connectivity);
  //for(auto& i:bg_connectivity) --i;
  //msh::SeqCoordinates BgCoord(2, bg_coordinates);
  //msh::StdTriConnectivity BgConn(bg_connectivity);
  //msh::StdTriMesh BG(BgCoord, BgConn);
  msh::StdTriMesh BG(EqCoord, EqConn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Initial guess for the radius of the outer boundary
  std::vector<double> bdPoints, bdNormals;
  const double PI = 4.*atan(1.);
   const int ninBdPoints = 40;
   for(int n=0; n<ninBdPoints; ++n)
     {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(ninBdPoints);
      bdPoints.push_back( sol_details.R0*std::cos(theta) );
      bdPoints.push_back( sol_details.R0*std::sin(theta) );
      bdNormals.push_back( -std::cos(theta) );
      bdNormals.push_back( -std::sin(theta) );
     }
   // outer boundary
   const int noutBdPoints = 20;
  for(int n=0; n<noutBdPoints; ++n)
    {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(noutBdPoints);
      bdPoints.push_back( 0.5*std::cos(theta) );
      bdPoints.push_back( 0.7*std::sin(theta) );
      bdNormals.push_back( std::cos(theta) );
      bdNormals.push_back( std::sin(theta) );
    }

  //const int nbdPoints = 50;
  /*for(int n=0; n<nbdPoints; ++n)
    {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(nbdPoints);
      bdPoints.push_back( 0.5*std::cos(theta) );
      bdPoints.push_back( 0.7*std::sin(theta) );
      bdNormals.push_back( std::cos(theta) );
      bdNormals.push_back( std::sin(theta) );
      }*/

  // SSD Options
  // Node set for max-ent calculations
  std::vector<double> maxent_coordinates;
  std::vector<int> maxent_connectivity;
  GetEquilateralMesh(2, maxent_coordinates, maxent_connectivity);
  std::vector<double> maxent_nodeset = maxent_coordinates;

  // Mesh for max-ent calculations
  msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
  msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
  bp::SSD_Options ssd_options({
      .Tol0=1.e-6,
	.TolNR=1.e-6,
	.nnb=1,
	.gamma=0.8,
	.alpha=0.01,
	.nodeset=&maxent_nodeset,
	.coordinates=&maxent_coordinates,
	.connectivity=&maxent_connectivity,
	.nMaxThreads=nThreads,
	.verbose=true});

  // Constant force field
  bp::ForceFieldValue<2> Frc(ConstForceFunc);

  // Dirichlet bcs
  std::function<void(const CoordConn&, std::vector<int>&, std::vector<double>&, void*)>
    f_dirichlet = DirichletBCs;

  // Boundary update function
  std::function<double(const double*, const double&, void*)> f_boundary_perturb = BoundaryUpdate;

  //msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
  {
    // Initialize bernoulli problem
    bp::BernoulliProblem2D bp2D(BG, bdPoints, bdNormals,
				ssd_options, mesh_options, geom_nlparams, Frc);
    
    for(int iter=0; iter<1; ++iter)
      {
	std::cout<<"\n\nIteration: "<<iter<< std::flush;
	
	// Plot the boundary points
	const auto& BdPoints = bp2D.GetBoundaryPoints();
	std::string filename = "bd-"+ std::to_string(iter) + ".dat";
	PlotPoints(filename, BdPoints);

	// Setup this iteration
	bp2D.SetupIteration();

	// Details of this iteration
	const auto& ssdfit = bp2D.GetSSD();
	const auto& MD = bp2D.GetConformingMesh();
	filename = "ssd-" + std::to_string(iter) + ".tec";
	ssdfit.PlotTec(filename.c_str(), MD);
	
	// Compute the state
	bp2D.ComputeState(f_dirichlet, &sol_details);

	const auto& UMap = bp2D.GetState();
	filename = "sol-" + std::to_string(iter) + ".tec";
	PlotTecCoordConn((char*)"MD.tec", MD);
	PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &UMap.Get()[0], 1);

	// Compute fluxes
	std::cout<<"\nComputing boundary fluxes..."<<std::flush;
	bp2D.ComputeBoundaryFluxes();
	const auto& PosVerts = bp2D.GetPositiveVertices();
	const auto& BdFluxes = bp2D.GetBoundaryFluxes();
	filename = "flux-" + std::to_string(iter) + ".dat";
	PlotFluxes(filename, MD, BdFluxes, PosVerts);
	
	// Update the boundary
	std::cout<<"\nUpdating the boundary..."<<std::flush;
	double dh = 0.95*0.2*meshsize;
	bp2D.UpdateBoundary(f_boundary_perturb, &dh);

	// Proceed to the next iteration
	std::cout<<"\nFinalizing..."<<std::flush;
	bp2D.FinalizeIteration();

	const auto& BdPoints_after = bp2D.GetBoundaryPoints();
	filename = "bdafter-"+ std::to_string(iter) + ".dat";
	PlotPoints(filename, BdPoints_after);
	
      }
  }
  
  // Done
  PetscFinalize();
}


// Create a background mesh of equilateral triangles
double GetEquilateralMesh(const int nDiv,
			  std::vector<double>& coord,
			  std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( std::cos(theta) );
      coord.push_back( std::sin(theta) );
    }
  double meshsize = 1.;

  // Subdivide
  for(int i=0; i<nDiv; i++)
    { msh::SubdivideTriangles(2, conn, coord);
      meshsize /= 2.; }

  return meshsize;
}


// Level set function for inner circle
void InnerCircle(const double* X, double& F, double* dF, double* d2F, void* params)
{
  assert(params!=nullptr);
  const double rad = (*static_cast<double*>(params));
  
  F = rad*rad - X[0]*X[0] - X[1]*X[1];
  if(dF!=nullptr)
    {
      dF[0] = -2.*X[0];
      dF[1] = -2.*X[1];
    }
  if(d2F!=nullptr)
    {
      d2F[0] = -2.; d2F[1] = 0.;
      d2F[2] = 0.; d2F[3] = -2.;
    }
  return;
}


// Dirchlet bcs
void DirichletBCs(const CoordConn& MD,
		  std::vector<int>& boundary, std::vector<double>& bvalues,
		  void* params)
{
  boundary.clear();
  bvalues.clear();
  
  // Get the neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify boundary nodes
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int* conn = &MD.connectivity[3*e];
	  bdnodes.insert(conn[f]-1);
	  bdnodes.insert(conn[(f+1)%3]-1);
	}

  // Set boundary values
  auto& sol_details = *static_cast<const SolDetails*>(params);
  for(auto& n:bdnodes)
    {
      boundary.push_back( n );
      const double* X = &MD.coordinates[2*n];
      double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      if(std::abs(rX-sol_details.R0)<sol_details.Reps)
	// Inner boundary
	bvalues.push_back( sol_details.U0 );
      else
	// Outer boundary
	bvalues.push_back( sol_details.U1 );
    }
  return;
}


// Boundary update function
double BoundaryUpdate(const double* X, const double& dudn, void* params)
{
  assert(params!=nullptr);
  const double& dh = *static_cast<double*>(params);
  const double gval = sol_details.gval;

  // Normalization factor for the velocity
  const double vnorm = 0.25;

  // Velocity of this node
  double vel = -(gval*gval-dudn*dudn);

  // Perturbation for this node
  return dh*vel/vnorm;
}


// plot boundary points
void PlotPoints(const std::string filename, const std::vector<double>& BdPoints)
{
  std::fstream stream;
  stream.open(filename.c_str(), std::ios::out);
  const int nPoints = static_cast<int>(BdPoints.size())/2;
  for(int p=0; p<nPoints; ++p)
    stream << BdPoints[2*p]<<" "<<BdPoints[2*p+1]<<"\n";
  stream.close();
}

// Plot boundary fluxes
void PlotFluxes(const std::string filename, const CoordConn& MD,
		const std::map<int, double>& BdFluxes,
		const std::vector<int>& PosVerts)
{
  std::fstream stream;
  stream.open(filename.c_str(), std::ios::out);
  for(auto& n:PosVerts)
    {
      const double* X = &MD.coordinates[2*n];
      const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      if(rX>sol_details.R0+sol_details.Reps)
	{
	  auto it = BdFluxes.find(n);
	  assert(it!=BdFluxes.end());
	  double theta = std::atan2(X[1], X[0]);
	  stream << theta <<" "<<it->second<<"\n";
	}
    }
  stream.close();
}
      
	
