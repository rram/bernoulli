// Sriramajayam

#include <MaxEnt.h>
#include <cassert>
#include <algorithm>
#include <cmath>
#include <iostream>

// Initialize data struct
MaxEnt::SolStruct::SolStruct(const int SPD)
{
  lambda.resize(SPD);
  Res.resize(SPD);
  Jac.resize(SPD*SPD);
  InvJac.resize(SPD*SPD);
}

// Constructor
MaxEnt::MaxEnt(const int spd, const double gmma,
	       const std::vector<double>& pts,
	       const double epsFunc, const double epsRes)
  :SPD(spd), gamma(gmma), NodeSet(pts),
   nNodes(NodeSet.size()/SPD),
   Tol0(epsFunc), TolNR(epsRes), 
   SolData(SPD)
{
  // Compute the distances between the nodes
  assert(NodeSet.size()%SPD==0 &&
	 "MaxEnt::MaxEnt- Inconsisent length of node set.");
  
  std::vector<double> Dist(nNodes,0.);
  beta.resize(nNodes);
  for(int n=0; n<nNodes; ++n)
    {
      std::fill(Dist.begin(), Dist.end(), 0.);
      // Compute distance to each node
      for(int m=0; m<nNodes; ++m)
	if(m!=n)
	  {
	    for(int k=0; k<SPD; ++k)
	      Dist[m] += (NodeSet[SPD*n+k]-NodeSet[SPD*m+k])*(NodeSet[SPD*n+k]-NodeSet[SPD*m+k]);
	    Dist[m] = sqrt(Dist[m]);
	  }

      // Set an inconsequential value for the self-distance
      Dist[n] = Dist[(n+1)%nNodes];
      
      // Find the parameter beta for this node based on the closest node
      double minDist = *std::min_element(Dist.begin(), Dist.end());
      assert(minDist>0. && "MaxEnt::MaxEnt- Likely redundant node in input point set.");
      beta[n] = gamma/(minDist*minDist);

      // TODO: Cut-off distance for this node
      //double Rc = sqrt(-std::log(Tol0)/beta[n]);
    }
}
  
// Destructor
MaxEnt::~MaxEnt() {}


// Copy constructor
MaxEnt::MaxEnt(const MaxEnt& Obj)
  :SPD(Obj.SPD), gamma(Obj.gamma),
   NodeSet(Obj.NodeSet),
   nNodes(Obj.nNodes),
   Tol0(Obj.Tol0), TolNR(Obj.TolNR),
   beta(Obj.beta), SolData(Obj.SPD)
{}


// Computes the function Z(x,lambda)
double MaxEnt::GetZ(const double* X, const double* lambda) const
{
  double Z = 0.;
  double Za;
  for(int a=0; a<nNodes; ++a)
    {
      const auto* Xa = &NodeSet[SPD*a];
      const auto& bta = beta[a];
      Za = 0.;
      for(int k=0; k<SPD; ++k)
	Za += (lambda[k]*(X[k]-Xa[k])-bta*(X[k]-Xa[k])*(X[k]-Xa[k]));
      Z += std::exp(Za);
    }
  return Z;
}


// Computes the residual for shape function evaluate
void MaxEnt::Residual(SolStruct& data)
{
  const auto* X = data.X;
  const auto* lambda = &data.lambda[0];
  auto& res = data.Res;
  auto& jac = data.Jac;
  
  // Compute Z(X,lambda)
  auto& Z = data.Z;
  Z = GetZ(X, lambda);

  // Compute the residual and jacobian
  double pb;
  std::fill(res.begin(), res.end(), 0.);
  std::fill(jac.begin(), jac.end(), 0.);
  for(int b=0; b<nNodes; ++b)
    {
      pb = 0.;
      const auto* Xb = &NodeSet[SPD*b];
      for(int k=0; k<SPD; ++k)
	pb += (-beta[b]*(X[k]-Xb[k])*(X[k]-Xb[k]) + lambda[k]*(X[k]-Xb[k]));
      pb = std::exp(pb)/Z;

      for(int k=0; k<SPD; ++k)
	{
	  res[k] += pb*(X[k]-Xb[k]);
	  for(int L=0; L<SPD; ++L)
	    jac[SPD*k+L] += pb*(X[k]-Xb[k])*(X[L]-Xb[L]);
	}
    }

  for(int k=0; k<SPD; ++k)
    for(int L=0; L<SPD; ++L)
      jac[SPD*k+L] -= res[k]*res[L];

  // Invert the jacobian if deemed necessary
  if(SPD==1)
    {
      assert(std::abs(jac[0])>1.e-6 && "MaxEnt::Residual- Jacobian is not invertible.");
      data.InvJac[0] = 1./jac[0];
    }
  else if(SPD==2)
    {
      double det = jac[0]*jac[3]-jac[1]*jac[2];
      //std::cout<<det<<" "; std::fflush( stdout );
      assert(std::abs(det)>1.e-10 && "MaxEnt::Residual- Jacobian is not invertible");
      auto& InvJac = data.InvJac;
      InvJac[0] = jac[3]/det;
      InvJac[1] = -jac[1]/det;
      InvJac[2] = -jac[2]/det;
      InvJac[3] = jac[0]/det;
    }
  else
    assert(false && "MaxEnt::Residual- 3D case not implemented.\n");

  return;
}


// Main functionality
void MaxEnt::Evaluate(const double* X, double* P)
{
  // Newton iteration to compute lambda
  auto& lambda = SolData.lambda;
  std::fill(lambda.begin(), lambda.end(), 0.);
  auto& res = SolData.Res;
  auto& ijac = SolData.InvJac;
  SolData.X = X;
  int nIter = 0;
  double resNorm;
  while(true)
    {
      // Compute the residual here
      Residual(SolData);
      resNorm = 0.;
      for(int k=0; k<SPD; ++k)
	resNorm += std::abs(res[k]);
      if(resNorm<TolNR)
	break;

      // Otherwise, update
      for(int k=0; k<SPD; ++k)
	for(int L=0; L<SPD; ++L)
	  lambda[k] -= ijac[SPD*k+L]*res[L];
      ++nIter;

      assert(nIter<20 && "MaxEnt::GetVal- Exceeded 20 iteration");
    }

  // Compute the shape functions at the computed solution
  for(int a=0; a<nNodes; ++a)
    {
      const auto* Xa = &NodeSet[SPD*a];
      P[a] = 0.;
      for(int k=0; k<SPD; ++k)
	P[a] += (-beta[a]*(X[k]-Xa[k])*(X[k]-Xa[k])+lambda[k]*(X[k]-Xa[k]));
      P[a] = std::exp(P[a])/SolData.Z;
    }
  return;
}
  
// Returns the intrinsic parameter beta associated with this node
double MaxEnt::GetBeta(const int a) const
{ return beta[a]; }

// Returns the number of shape functions
int MaxEnt::GetNumFunctions() const
{ return nNodes; }

// Returns the number of nodes
int MaxEnt::GetNumNodes() const
{ return nNodes; }

// Returns the tolerance used for shape function values
double MaxEnt::GetFunctionTolerance() const
{ return Tol0; }

// Returns the tolrance used for Newton-iterations
double MaxEnt::GetResidualTolerance() const
{ return TolNR; }

// Returns the spatial dimension
int MaxEnt::GetSpatialDimension() const
{ return SPD; }

// Returns the node set
const std::vector<double>& MaxEnt::GetNodeSet() const
{ return NodeSet; }
  
