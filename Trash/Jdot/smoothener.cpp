// Sriramajayam

// Solve the equation M+alpha(K) = bd-Flux on an annulus
// Apply 0 Dirichlet bcs on the inner boundary
// Apply flux bcs on the outer boundary

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <omp.h>
#include <algorithm>
#include <random>

using namespace bp;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(-0.01,0.01);

// Problem details
struct SolDetails
{
  static constexpr double R0 = 0.2;
  static constexpr double R1 = 0.7;
  static constexpr double fluxval = 0.5;
};

// Constant flux on the outer boundary
bool ConstFlux(const std::vector<double> u, const std::vector<double> X,
	       std::vector<double>* F, std::vector<double>* dF, void* params)
{ if(F->size()<1) F->resize(1);
  (*F)[0] = 1.;//+dis(gen);//std::sin(X[0]);//+dis(gen); //SolDetails::fluxval;
  return true;
}
  
// Returns the mesh size
double GetMeshSize(const CoordConn& MD);

// Project boundary nodes onto annulus
void ProjectBdNodes(CoordConn& MD);

// Get nodes on the inner boundary, (numbered from 0)
void GetInnerBoundary(const CoordConn& MD, std::set<int>& bdnodes);

// Get segments on the outer boundary (numbered from 0)
void GetOuterBoundary(const CoordConn& MD, std::vector<int>& bdsegments);

// Plot the solution on the outer boundary
void PlotOuterBdSolution(const char* filename, const CoordConn& MD, const double* wvals);

// Compute the h1 norm of a function (u^2 + alpha grad(u).grad(u))
double H1Norm(const std::vector<Laplacian*>& LapArray,
	      const std::vector<L2Norm*>& L2Array,
	      const ScalarMap& WMap,
	      const double alpha);

int main(int argc, char** argv)
{
  omp_set_num_threads(1);
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh to use
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);
  double meshsize = GetMeshSize(MD);
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(NULL, (char*)"-ndiv", &ndiv, &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      meshsize /= 2.;
      ProjectBdNodes(MD);
    }
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Create scalar map
  ScalarMap WMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; WMap.Set(n, &val); }
  WMap.SetInitialized();

  // Create elements and operations
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> LapArray(MD.elements);
  std::vector<L2Norm*> L2Array(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess sma(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      LapArray[e] = new Laplacian(ElmArray[e], sma, 0);
      L2Array[e] = new L2Norm(ElmArray[e], sma, 0);
    }
  StandardP12DMap L2GMap(ElmArray);

  // Create segments along the outer boundary (nodes numbered from 0)
  std::vector<int> OutBdSegments;
  GetOuterBoundary(MD, OutBdSegments);
  const int nOutBdSegments = static_cast<int>(OutBdSegments.size()/2);
  ForceFieldValue<2> Flux(ConstFlux);
  std::vector<Element*> OutBdElmArray(nOutBdSegments);
  std::vector<BodyForceWork*> OutBdFrcArray(nOutBdSegments);
  std::vector<int> Fields({0});
  for(int e=0; e<nOutBdSegments; ++e)
    {
      const int* conn = &OutBdSegments[2*e];
      OutBdElmArray[e] = new P12DSegment<1>(conn[0]+1, conn[1]+1);
      ScalarMapAccess sma(std::vector<int>({conn[0], conn[1]}));
      OutBdFrcArray[e] = new BodyForceWork(OutBdElmArray[e], Flux, Fields, sma);
    }
  StandardP12DSegmentMap OutBdL2GMap(OutBdElmArray);

  // Dirichlet bcs (numbered from 0)
  std::set<int> dirichletnodes;
  GetInnerBoundary(MD, dirichletnodes);
  std::vector<int> dirichletboundary(dirichletnodes.begin(), dirichletnodes.end());
  std::vector<double> dirichletvalues(dirichletnodes.size(), 0.);
  
  // Create assemblers
  StandardAssembler<Laplacian> LapAsm(LapArray, L2GMap);
  StandardAssembler<L2Norm> L2Asm(L2Array, L2GMap);
  StandardAssembler<BodyForceWork> FluxAsm(OutBdFrcArray, OutBdL2GMap);

  // Create PETSc data structures
  std::vector<int> nz;
  LapAsm.CountNonzeros(nz);
  PetscData PD;
  PD.Initialize(nz);
  
  // Assemble M+alpha(K)
  const double alpha = 0.0001;//4.*meshsize*meshsize;
  LapAsm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT);
  MatScale(PD.stiffnessMAT, alpha);
  L2Asm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT, false);
  VecZeroEntries(PD.resVEC);

  // Assemble flux bcs
  FluxAsm.Assemble(&WMap, PD.resVEC);

  // Set dirichlet bcs
  //PD.SetDirichletBCs(dirichletboundary, dirichletvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution
  double* inc;
  //VecScale(PD.solutionVEC, -1.); // We solve (M+alpha*K) + R = 0
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    WMap.Set(n, &inc[n]);
  WMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Normalize the solution by the H1 norm
  double norm = H1Norm(LapArray, L2Array, WMap, alpha);
  const double* wvals = &WMap.Get()[0];
  for(int n=0; n<MD.nodes; ++n)
    { double val = wvals[n]/norm; WMap.Set(n, &val); }
  WMap.SetInitialized();
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields((char*)"wvals.tec", MD, &WMap.Get()[0], 1);

  // Plot the solution on the outer boundary
  PlotOuterBdSolution((char*)"bdsol.dat", MD, &WMap.Get()[0]);
  
  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:LapArray) delete it;
  for(auto& it:L2Array) delete it;
  for(auto& it:OutBdElmArray) delete it;
  for(auto& it:OutBdFrcArray) delete it;
  PD.Destroy();
  PetscFinalize();
}
  
// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = SolDetails::R0;
  const double R1 = SolDetails::R1;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    const double R = (r<0.5*(R0+R1)) ? R0 : R1;
	    for(int k=0; k<2; ++k)
	      MD.coordinates[2*n+k] = X[k]*R/r;
	  }
  return;
}
      

// Get segments on the outer boundary (numbered from 0)
void GetOuterBoundary(const CoordConn& MD, std::vector<int>& bdsegments)
{
  bdsegments.clear();
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r>Ravg)
	    for(int i=0; i<2; ++i)
	      bdsegments.push_back(MD.connectivity[3*e+(f+i)%3]-1);
	}
  return;
}
				  
	  

// Get nodes on the inner boundary, (numbered from 0)
void GetInnerBoundary(const CoordConn& MD, std::set<int>& bdnodes)
{
  bdnodes.clear();
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	{
	  const int n = MD.connectivity[3*e+(f+i)%3]-1;
	  const double* X = &MD.coordinates[2*n];
	  double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r<Ravg)
	    bdnodes.insert(n);
	}
  return;
}
 
// Plot the solution on the outer boundary
void PlotOuterBdSolution(const char* filename, const CoordConn& MD, const double* wvals)
{
  // Get the list of boundary segments
  std::vector<int> bdsegments;
  GetOuterBoundary(MD, bdsegments);
  std::set<int> bdnodes(bdsegments.begin(), bdsegments.end());
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  std::vector<double> u, xvec(2), fvec(1);
  for(auto& n:bdnodes)
    {
      const double* X = &MD.coordinates[2*n];
      xvec[0] = X[0]; xvec[1] = X[1];
      double theta = std::atan2(X[1], X[0]);

      // Exact flux here
      ConstFlux(u, xvec, &fvec, nullptr, nullptr);
      pfile<<theta<<" "<<fvec[0]<<" "<<wvals[n]<<"\n";
    }
	pfile.flush();
      pfile.close();
      return;
}  

  // Returns the mesh size
  double GetMeshSize(const CoordConn& MD)
  {
    double hsum = 0.;
    int nedges = 0;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      {
	const int n0 = MD.connectivity[3*e+f%3]-1;
	const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	const double* X = &MD.coordinates[2*n0];
	const double* Y = &MD.coordinates[2*n1];
	hsum += (X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]);
	++nedges;
      }
  hsum /= static_cast<double>(nedges);
  return std::sqrt(hsum);
}

// Compute the h1 norm of a function: (u^2 + alpha grad(u).grad(u))
double H1Norm(const std::vector<Laplacian*>& LapArray, const std::vector<L2Norm*>& L2Array,
	      const ScalarMap& WMap, const double alpha)
{
  double Energy = 0.;
  for(auto& it:LapArray)
    Energy += 2.*alpha*it->GetEnergy(&WMap);
  for(auto& it:L2Array)
    Energy += 2.*it->GetEnergy(&WMap);
  return std::sqrt(Energy);
}
