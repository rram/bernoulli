// Sriramajayam

#ifndef GET_ME_A_MESH_H
#define GET_ME_A_MESH_H

#include <um_TriangleModule>
#include <geom_LevelSetManifold.h>
#include <algorithm>

// Triangulate the given geometry using the given background mesh
void Triangulate(msh::StdTriMesh& BG, geom::LevelSetManifold<2>& Geom, const char* filename)
{
  // Workspace for the background mesh
  um::SDWorkspace<msh::StdTriMesh, decltype(Geom)> BGws(BG, Geom);

  // Signed distances at all the ndoes
  const int nbgnodes = BG.GetNumNodes();
  std::vector<int> allnodes(nbgnodes);
  for(int i=0; i<nbgnodes; ++i)
    allnodes[i] = i;
  BGws.Evaluate(allnodes);
  //um::PlotSignedDistances("bgsd.tec", BGws);
  
  // Create domain triangulator
  um::DomainTriangulator<msh::StdTriMesh, decltype(Geom)> Mshr(BGws);

  // Get the working mesh
  auto& WM = Mshr.GetWorkingMesh();
  
  // Plot the working submesh as an orphan
  um::PlotTecWorkingMesh("wm.tec", WM, true);

  // Positive vertices
  const auto& PosVerts = Mshr.GetPositiveVertices();

  // Interior nodes of the working mesh
  std::vector<int> wmnodes;
  WM.GetNodes(wmnodes);
  std::sort(wmnodes.begin(), wmnodes.end());
  std::vector<int> intnodes({});
  std::set_difference(wmnodes.begin(), wmnodes.end(), PosVerts.begin(), PosVerts.end(),
		      std::back_inserter(intnodes));

  // 1-ring data for the working mesh
  dvr::ValencyHint vhint({7,7});
  dvr::OneRingData<decltype(WM)> RD(WM, wmnodes, vhint);

  // Quality metric for the working mesh
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);

  // Max-min solver for relaxing vertices
  dvr::SolverSpecs solspecs({1,2,3}); // nthreads, nextrema, nintersections
  dvr::ReconstructiveMaxMinSolver<decltype(WM)> mmsolver(WM, RD, solspecs);

  // Vertex optimizer
  dvr::SeqMeshOptimizer Opt;

  // Relaxation direction generator
  dvr::RelaxationDirGenerator rdir_gen;
  rdir_gen.f_rdir = dvr::CartesianDirections<2>;
  // rdir_gen.params should be set to equal the iteration counter

  // Project & relax in steps
  const int nSteps = 5;
  const int nIters = 8;
  for(int step=1; step<=nSteps; ++step)
    {
      // Project boundary vertices
      const double alpha = static_cast<double>(step)/static_cast<double>(nSteps);
      Mshr.Project(alpha);
      
      // Relax interior vertices
      for(int iter=0; iter<nIters; ++iter)
	{
	  rdir_gen.params = &iter;
	  Mshr.Relax(intnodes, Opt, Quality, mmsolver, rdir_gen);
	}
    }
  um::PlotTecWorkingMesh("wm-int.tec", WM, true);

  // Relax boundary + interior vertices
  um::MeshGeomPair<decltype(WM), decltype(Geom)> MGPair(WM, Geom);
  dvr::RelaxationDirGenerator tgtdirgen;
  tgtdirgen.f_rdir = um::TangentDirection2D<decltype(MGPair)>;
  tgtdirgen.params = &MGPair;
  dvr::ProjPartitionedMaxMinSolver<decltype(WM)> bdmmsolver(WM, RD, solspecs);
  um::BoundaryProjector<decltype(Geom)> bp(Geom);
  for(int iter=0; iter<nIters; ++iter)
    {
      // Boundary vertex relaxation+projection
      Mshr.Relax(PosVerts, Opt, Quality, bdmmsolver, tgtdirgen, &bp);
      // Interior vertex relaxation
      rdir_gen.params = &iter;
      Mshr.Relax(intnodes, Opt, Quality, mmsolver, rdir_gen);
    }
  um::PlotTecWorkingMesh(filename, WM, true);
  return;
}


#endif
