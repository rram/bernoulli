// Sriramajayam

// Solve the equation: Lap(u) = 4 on an annulus with Dirichlet bcs on the inner and outer bds.
// Form the function w = g^2-(du/dn)^2 on the outer boundary.
// Smoothen w by resolving Lap(w_hat) + alpha w_hat = 0 with bcs
//                         w_hat = 0 on the inner boundary
//                         flux = w on the outer boundary

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <omp.h>

using namespace bp;

// State problem details
struct StateSolDetails
{
  static constexpr double R0 = 0.2;
  static constexpr double R1 = 0.7;
  static constexpr double U0 = 1.0;
  static constexpr double U1 = 0.0;
  static constexpr double fval = 4.;
  static constexpr double gval = 2.0271851144019437;
  static double GetExactSolution(const double* X);
};


// Constant body force function
bool BodyForceFunc(const std::vector<double> u, const std::vector<double> X,
		   std::vector<double>* F, std::vector<double>* dF, void* params)
{ if(F->size()<1) F->resize(1);
  (*F)[0] = StateSolDetails::fval;
  return true;
}


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD);

// Assign dirichlet BCs
void GetStateDirichletBCs(const CoordConn& MD, 
			  std::vector<int>& boundary, std::vector<double>& bvalues);

// Compute the L2 norm of the error
double ComputeStateL2Error(const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap, const double* uvals);


// Solve the state problem
void SolveState(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		std::vector<Laplacian*>& LapArray,
		std::vector<BodyForceWork*>& BFrcArray,
		const LocalToGlobalMap& L2GMap,
		ScalarMap& UMap);

// Enumerate segments on the outer boundary
void GetNeumannFluxes(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* sol,
		      std::vector<int>& OutBdSegments, std::vector<double>& fluxvals);

// Boundary flux forcing
bool NeumannFluxFunc(const std::vector<double> u, const std::vector<double> X,
		     std::vector<double>* F, std::vector<double>* dF, void* params)
{
  assert(params!=nullptr);
  double* fval = static_cast<double*>(params); assert(fval!=nullptr);
  if(F->size()<1) F->resize(1);
  (*F)[0] = *fval;
  return true;
}

// Boundary flux forcing for a constant
bool ConstantNeumannFluxFunc(const std::vector<double> u, const std::vector<double> X,
			     std::vector<double>* F, std::vector<double>* dF, void* params)
{
  if(F->size()<1) F->resize(1);
  (*F)[0] = 0.1;
  return true;
}

// Visualize the boundary fluxes
void PlotNeumannFluxes(const char* filename, const CoordConn& MD,
		       const std::vector<int>& segments, const std::vector<double>& fluxes);

// Assign dirichlet BCs
void GetSmoothenerDirichletBCs(const CoordConn& MD, 
			       std::vector<int>& boundary,
			       std::vector<double>& bvalues);

// Enumerate segments on the outer boundary
void GetSmoothenerNeumannFluxes(const CoordConn& MD,
				const std::vector<Element*>& ElmArray,
				const LocalToGlobalMap& L2GMap, const double* sol,
				std::vector<int>& BdSegments, std::vector<double>& fluxvals);

// Solve the smoothener problem
void SolveSmoothener(const CoordConn& MD, const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		     std::vector<Laplacian*>& LapArray, std::vector<L2Norm*>& L2Array,
		     const std::vector<Element*>& BdElmArray, const LocalToGlobalMap& BdL2GMap,
		     std::vector<BodyForceWork*>& NeumannFrcArray, ScalarMap& WMap);

int main(int argc, char** argv)
{
  omp_set_num_threads(1);

  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh to use
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      ProjectBdNodes(MD);
    }
  PlotTecCoordConn((char*)"MD.tec", MD);
  
  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements & operations
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> LapArray(MD.elements);
  std::vector<L2Norm*> L2Array(MD.elements);
  ForceFieldValue<2> BFrc(BodyForceFunc);
  std::vector<int> Field({0});
  std::vector<BodyForceWork*> BFrcArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      LapArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
      BFrcArray[e] = new BodyForceWork(ElmArray[e], BFrc, Field, SMAccess);
      L2Array[e] = new L2Norm(ElmArray[e], SMAccess, 0);
    }

  // Local to global map for domain elements
  StandardP12DMap L2GMap(ElmArray);
  
  // Compute the state solution satisfying Lap(u) = f, with dirichlet bcs on boundaries
  ScalarMap UMap(MD.nodes);
  SolveState(MD, ElmArray, LapArray, BFrcArray, L2GMap, UMap);
  PlotTecCoordConnWithNodalFields((char*)"uvals.tec", MD, &UMap.Get()[0], 1);
  std::cout<<"\nL2 error for state solution: "
	   <<ComputeStateL2Error(ElmArray, L2GMap, &UMap.Get()[0])<<std::flush;


  // Compute the Neumann segments and the fluxes to impose on the outer boundary
  std::vector<int> BdSegments;
  std::vector<double> fluxes;
  GetNeumannFluxes(MD, ElmArray, L2GMap, &UMap.Get()[0], BdSegments, fluxes);
  PlotNeumannFluxes((char*)"fluxes.dat", MD, BdSegments, fluxes);
  const int nBdSegments = static_cast<int>(BdSegments.size()/2);
  assert(static_cast<int>(fluxes.size())==nBdSegments);
  std::vector<Element*> BdElmArray(nBdSegments);
  std::vector<ForceFieldValue<2>*> NeumannFrcArray(nBdSegments);
  std::vector<BodyForceWork*> BdFrcArray(nBdSegments);
  ForceFieldValue<2> ConstFrc(ConstantNeumannFluxFunc);
  for(int e=0; e<nBdSegments; ++e)
    {
      const int* conn = &BdSegments[2*e];
      BdElmArray[e] = new P12DSegment<1>(conn[0], conn[1]);
      //fluxes[e] = 0.1;
      NeumannFrcArray[e] = new ForceFieldValue<2>(NeumannFluxFunc, &fluxes[e]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1}));
      //BdFrcArray[e] = new BodyForceWork(BdElmArray[e], *NeumannFrcArray[e], Field, SMAccess);
      BdFrcArray[e] = new BodyForceWork(BdElmArray[e], ConstFrc, Field, SMAccess);
    }
  // Local to global map for outer-boundary elements
  StandardP12DSegmentMap BdL2GMap(BdElmArray);

  // Compute the smoothened solution
  ScalarMap WMap(MD.nodes);
  SolveSmoothener(MD, ElmArray, L2GMap, LapArray, L2Array,
		  BdElmArray, BdL2GMap, BdFrcArray, WMap);
  PlotTecCoordConnWithNodalFields((char*)"wvals.tec", MD, &WMap.Get()[0], 1);

    // Compute the Neumann segments and the fluxes to impose on the outer boundary
  std::vector<double> newfluxes;
  GetSmoothenerNeumannFluxes(MD, ElmArray, L2GMap, &WMap.Get()[0], BdSegments, newfluxes);
  PlotNeumannFluxes((char*)"newfluxes.dat", MD, BdSegments, newfluxes);
  
  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:LapArray) delete it;
  for(auto& it:BFrcArray) delete it;
  for(auto& it:BdElmArray) delete it;
  for(auto& it:NeumannFrcArray) delete it;
  for(auto& it:BdFrcArray) delete it;
}


// Exact state solution for this problem
double StateSolDetails::GetExactSolution(const double* X)
{
  double f = fval;
  double r = std::sqrt(X[0]*X[0] + X[1]*X[1]);
  return ((f*(R0 - R1)*(R0 + R1) + 4.*(U0 - U1))*std::log(r) +
	  (-(f*std::pow(r,2)) + f*R1*R1 + 4.*U1)*std::log(R0) + 
	  (f*(r - R0)*(r + R0) - 4.*U0)*std::log(R1))/(4.*(std::log(R0) - std::log(R1)));
}


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = StateSolDetails::R0;
  const double R1 = StateSolDetails::R1;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    const double R = (r<0.5*(R0+R1)) ? R0 : R1;
	    for(int k=0; k<2; ++k)
	      MD.coordinates[2*n+k] = X[k]*R/r;
	  }
  return;
}


// Assign dirichlet BCs
void GetStateDirichletBCs(const CoordConn& MD, 
			  std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  
  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert( MD.connectivity[3*e+(f+i)%3]-1 );
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      bvalues.push_back( StateSolDetails::GetExactSolution(&MD.coordinates[2*it]) );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}


// Solve the state problem
void SolveState(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		std::vector<Laplacian*>& LapArray,
		std::vector<BodyForceWork*>& BFrcArray,
		const LocalToGlobalMap& L2GMap,
		ScalarMap& UMap)
{
  // Initialize the scalar field to 0
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();
  
  // Assemblers
  StandardAssembler<Laplacian> AsmLap(LapArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(BFrcArray, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);
  
  // Get Dirichlet BCs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetStateDirichletBCs(MD, boundary, bvalues);
  
  // Update the state with Dirichlet bcs
  const int nbcs = static_cast<int>(boundary.size());
  for(int i=0; i<nbcs; ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0);

  // Create PESTc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);

  // Assemble laplacian and forcing contributions
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  MatScale(PD.stiffnessMAT, -1.);
  VecScale(PD.resVEC, -1.);
  AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero residual
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution and update the state
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Clean up
  PD.Destroy();
  return;
}


// Compute the L2 norm of the error
double ComputeStateL2Error(const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap, const double* uvals)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double L2Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nDof = ElmArray[e]->GetDof(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  double uex = StateSolDetails::GetExactSolution(&Qpts[2*q]);

	  // FE solution
	  double uh = 0.;
	  const auto& Shp = ElmArray[e]->GetShape(0);
	  for(int a=0; a<nDof; ++a)
	    uh += uvals[L2GMap.Map(0,a,e)]*Shp[q*nDof+a];

	  // Update the L2 error
	  L2Err += Qwts[q]*(uex-uh)*(uex-uh);
	}
    }
  return std::sqrt(L2Err);
}


// Enumerate segments on the outer boundary
void GetNeumannFluxes(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* sol,
		      std::vector<int>& BdSegments, std::vector<double>& fluxvals)
{
  BdSegments.clear();
  fluxvals.clear();
  const double gval = StateSolDetails::gval;
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n];
	  const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r>0.5*(StateSolDetails::R0+StateSolDetails::R1))
	    {
	      // Note this boundary segment
	      int n0 = MD.connectivity[3*e+f]-1;
	      int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      BdSegments.push_back(n0+1);
	      BdSegments.push_back(n1+1);

	      // Gradient of the solution in this element
	      double gradU[2] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int j=0; j<2; ++j)
		  gradU[j] += sol[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,j);
	      
	      // Use the normal at the mid-point
	      double Y[2];
	      for(int i=0; i<2; ++i)
		Y[i] = 0.5*(MD.coordinates[2*n0+i]+MD.coordinates[2*n1+i]);
	      double rY = std::sqrt(Y[0]*Y[0]+Y[1]*Y[1]);
	      double normal[] = {Y[0]/rY,Y[1]/rY};
	      double dudn = gradU[0]*normal[0] + gradU[1]*normal[1];
	      fluxvals.push_back(gval*gval-dudn*dudn);
	    }
	}
  return;
}


// Enumerate segments on the outer boundary
void GetSmoothenerNeumannFluxes(const CoordConn& MD,
				const std::vector<Element*>& ElmArray,
				const LocalToGlobalMap& L2GMap, const double* sol,
				std::vector<int>& BdSegments, std::vector<double>& fluxvals)
{
  BdSegments.clear();
  fluxvals.clear();

  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n];
	  const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r>0.5*(StateSolDetails::R0+StateSolDetails::R1))
	    {
	      // Note this boundary segment
	      int n0 = MD.connectivity[3*e+f]-1;
	      int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      BdSegments.push_back(n0+1);
	      BdSegments.push_back(n1+1);

	      // Gradient of the solution in this element
	      double gradU[2] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int j=0; j<2; ++j)
		  gradU[j] += sol[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,j);
	      
	      // Use the normal at the mid-point
	      double Y[2];
	      for(int i=0; i<2; ++i)
		Y[i] = 0.5*(MD.coordinates[2*n0+i]+MD.coordinates[2*n1+i]);
	      double rY = std::sqrt(Y[0]*Y[0]+Y[1]*Y[1]);
	      double normal[] = {Y[0]/rY,Y[1]/rY};
	      double dudn = gradU[0]*normal[0] + gradU[1]*normal[1];
	      fluxvals.push_back(dudn);
	    }
	}
  return;
}


// Visualize the boundary fluxes
void PlotNeumannFluxes(const char* filename, const CoordConn& MD,
		       const std::vector<int>& segments, const std::vector<double>& fluxes)
{
  const int nsegments = static_cast<int>(segments.size()/2);
  std::fstream pfile;
  pfile.open(filename, std::ios::out);
  for(int e=0; e<nsegments; ++e)
    {
      const int n0 = segments[2*e]-1;
      const int n1 = segments[2*e+1]-1;
      double X[2];
      for(int k=0; k<2; ++k)
	X[k] = 0.5*(MD.coordinates[2*n0+k]+MD.coordinates[2*n1+k]);
      double theta = std::atan2(X[1],X[0]);
      pfile<<theta<<" "<<fluxes[e]<<"\n";
    }
  pfile.close();
}


// Assign dirichlet BCs
void GetSmoothenerDirichletBCs(const CoordConn& MD, 
			       std::vector<int>& boundary,
			       std::vector<double>& bvalues)
{
  const double Ravg = 0.5*(StateSolDetails::R0+StateSolDetails::R1);
  
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  
  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  {
	    const double n = MD.connectivity[3*e+(f+i)%3]-1;
	    const double* X = &MD.coordinates[2*n];
	    double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    if(r<Ravg) bdnodes.insert(n);
	  }
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      bvalues.push_back( 0. );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}


// Solve the smoothener problem
void SolveSmoothener(const CoordConn& MD, const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		     std::vector<Laplacian*>& LapArray, std::vector<L2Norm*>& L2Array,
		     const std::vector<Element*>& BdElmArray, const LocalToGlobalMap& BdL2GMap,
		     std::vector<BodyForceWork*>& NeumannFrcArray, ScalarMap& WMap)
{
  // Initialize WMap to zero
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; WMap.Set(n, &val); }
  WMap.SetInitialized();

  // Create assembler
  StandardAssembler<Laplacian> LapAsm(LapArray, L2GMap);
  StandardAssembler<L2Norm> L2Asm(L2Array, L2GMap);
  StandardAssembler<BodyForceWork> NeumannAsm(NeumannFrcArray, BdL2GMap);
  std::vector<int> nz;
  LapAsm.CountNonzeros(nz);

  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);

  // Get dirichlet bcs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetSmoothenerDirichletBCs(MD, boundary, bvalues);

  // Update WMap
  const int nbcs = static_cast<int>(boundary.size());
  std::cout<<"\nnbcs: "<<nbcs;
  for(int i=0; i<nbcs; ++i)
    WMap.Increment(boundary[i], &bvalues[i]);
  WMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Assemble
  LapAsm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT);
  const double alpha = 0.1;//0.01;
  MatScale(PD.stiffnessMAT, alpha);
  L2Asm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT, false); // Don't zero
  VecZeroEntries(PD.resVEC); // RHS is 0
  NeumannAsm.Assemble(&WMap, PD.resVEC); //Zero

  // Set dirichlet bcs
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Update the solution
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    WMap.Increment(n, &inc[n]);
  WMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Clean up
  PD.Destroy();
  return;
}
