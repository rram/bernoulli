// Sriramajayam

// Solve the equation: Lap(u) = 4 on an annulus with Dirichlet bcs on the inner and outer bds.
// Form the function w = g^2-(du/dn)^2 on the outer boundary.
// Smoothen w by resolving Lap(w_hat) + alpha w_hat = 0 with bcs
//                         w_hat = 0 on the inner boundary
//                         flux = w on the outer boundary

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <omp.h>

using namespace bp;

// State problem details
struct SolDetails
{
  static constexpr double R0 = 0.2;
  static constexpr double R1 = 0.7;
  static constexpr double U0 = 1.0;
  static constexpr double U1 = 0.0;
  static constexpr double fval = 4.;
  static constexpr double gval = 2.0271851144019437;
  static double GetExactStateSolution(const double* X);
};

// Constant body force function
bool ConstBodyForceFunc(const std::vector<double> u, const std::vector<double> X,
			std::vector<double>* F, std::vector<double>* dF, void* params)
{ if(F->size()<1) F->resize(1);
  (*F)[0] = SolDetails::fval;
  return true;
}

// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD);

// Returns the mesh size
double GetMeshSize(const CoordConn& MD);

// Gets the nodes on the boundary, (numbered from 0)
void GetBoundary(const CoordConn& MD, std::set<int>& bdnodes);

// Get nodes on the inner boundary, (numbered from 0)
void GetInnerBoundary(const CoordConn& MD, std::set<int>& bdnodes);

// Get segments on the outer boundary (numbered from 0)
void GetOuterBoundary(const CoordConn& MD, std::vector<int>& bdsegments);

// Compute the h1 norm of a function (u^2 + alpha grad(u).grad(u))
double H1Norm(const std::vector<Laplacian*>& LapArray,
	      const std::vector<L2Norm*>& L2Array,
	      const ScalarMap& WMap,
	      const double alpha);

// Compute the L2 norm of the error
double ComputeStateL2Error(const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap, const double* uvals);

// Solve the state problem
void SolveState(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		std::vector<Laplacian*>& LapArray,
		std::vector<BodyForceWork*>& FrcArray,
		const LocalToGlobalMap& L2GMap,
		ScalarMap& UMap);

// Compute the velocity on the outer boundary for a given state solution
void ComputeBoundaryVelocity(const CoordConn& MD, const std::vector<Element*>& ElmArray,
			     const LocalToGlobalMap& L2GMap, const double* uvals,
			     std::vector<int>& bdsegments, std::vector<double>& vel);
  
// Boundary flux forcing
bool NeumannFluxFunc(const std::vector<double> u, const std::vector<double> X,
		     std::vector<double>* F, std::vector<double>* dF, void* params)
{
  assert(params!=nullptr);
  double* fval = static_cast<double*>(params); assert(fval!=nullptr);
  if(F->size()<1) F->resize(1);
  (*F)[0] =  *fval;
  std::cout<<"\n"<<*fval<<std::flush;
  return true;
}

// Compute the h1 norm of a function: (u^2 + alpha grad(u).grad(u))
double H1Norm(const std::vector<Laplacian*>& LapArray, const std::vector<L2Norm*>& L2Array,
	      const ScalarMap& WMap, const double alpha);

// Solve for the smoothened velocity field
void SolveVelocity(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		   const LocalToGlobalMap& L2GMap,
		   std::vector<Laplacian*>& LapArray, std::vector<L2Norm*>& L2Array,
		   const std::vector<Element*>& FluxElmArray, const LocalToGlobalMap& FluxL2GMap,
		   std::vector<BodyForceWork*>& FluxOpArray, ScalarMap& WMap);

// Plot the solution on the outer boundary
void PlotOuterBdSolution(const char* filename, const CoordConn& MD, const double* wvals);

// Plot the prescribed velocity field
void PlotVelocity(const char* filename, const CoordConn& MD,
		  const std::vector<int>& FluxSegments, const std::vector<double>& FluxVals);

int main(int argc, char** argv)
{
  omp_set_num_threads(1);
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh to use
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(NULL, (char*)"-ndiv", &ndiv, &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      ProjectBdNodes(MD);
    }
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Create scalar map for state solution
  ScalarMap UMap(MD.nodes);

  // Setup elements and operations for state solution
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> LapArray(MD.elements);
  ForceFieldValue<2> BFrc(ConstBodyForceFunc);
  std::vector<int> Field({0});
  std::vector<BodyForceWork*> FrcArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      LapArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
      FrcArray[e] = new BodyForceWork(ElmArray[e], BFrc, Field, SMAccess);
    }
  StandardP12DMap L2GMap(ElmArray);
  SolveState(MD, ElmArray, LapArray, FrcArray, L2GMap, UMap);
  PlotTecCoordConnWithNodalFields((char*)"uvals.tec", MD, &UMap.Get()[0], 1);
  std::cout<<"\nL2 error for state solution: "
	   <<ComputeStateL2Error(ElmArray, L2GMap, &UMap.Get()[0])<<std::flush;

  // Compute the velocity values on the outer boundary segments (one per segment)
  std::vector<int> FluxSegments;
  std::vector<double> Vel;
  ComputeBoundaryVelocity(MD, ElmArray, L2GMap, &UMap.Get()[0], FluxSegments, Vel);
  const int nFluxSegments = static_cast<int>(FluxSegments.size()/2);
  PlotVelocity((char*)"vel.dat", MD, FluxSegments, Vel);
  
  // Create operations for computing smoothened-velocity
  std::vector<L2Norm*> L2Array(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ScalarMapAccess sma({conn[0]-1, conn[1]-1, conn[2]-1});
      L2Array[e] = new L2Norm(ElmArray[e], sma, Field[0]);
    }
  std::vector<Element*> FluxElmArray(nFluxSegments);
  std::vector<ForceFieldValue<2>*> FluxFrcArray(nFluxSegments);
  std::vector<BodyForceWork*> FluxOpArray(nFluxSegments);
  for(int e=0; e<nFluxSegments; ++e)
    {
      const auto* conn = &FluxSegments[2*e];
      FluxElmArray[e] = new P12DSegment<1>(conn[0]+1, conn[1]+1);
      ScalarMapAccess sma({conn[0], conn[1]});
      FluxFrcArray[e] = new ForceFieldValue<2>(NeumannFluxFunc, &Vel[e]);
      FluxOpArray[e] = new BodyForceWork(FluxElmArray[e], *FluxFrcArray[e], Field, sma);
    }
  StandardP12DSegmentMap FluxL2GMap(FluxElmArray);

  // Solve for the smoothened velocity field
  ScalarMap WMap(MD.nodes);
  SolveVelocity(MD, ElmArray, L2GMap, LapArray, L2Array,
		FluxElmArray, FluxL2GMap, FluxOpArray, WMap);
  PlotTecCoordConnWithNodalFields((char*)"wvals.tec", MD, &WMap.Get()[0], 1);
  PlotOuterBdSolution((char*)"vstar.dat", MD, &WMap.Get()[0]);
  
  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:LapArray) delete it;
  for(auto& it:FrcArray) delete it;
  for(auto& it:FluxElmArray) delete it;
  for(auto& it:L2Array) delete it;
  for(auto& it:FluxFrcArray) delete it;
  for(auto& it:FluxOpArray) delete it;
  PetscFinalize();
}


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = SolDetails::R0;
  const double R1 = SolDetails::R1;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    const double R = (r<0.5*(R0+R1)) ? R0 : R1;
	    for(int k=0; k<2; ++k)
	      MD.coordinates[2*n+k] = X[k]*R/r;
	  }
  return;
}				  
	  



// Get nodes on the inner+outer boundary, (numbered from 0)
void GetBoundary(const CoordConn& MD, std::set<int>& bdnodes)
{
  bdnodes.clear();
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);
  return;
}


// Returns the mesh size
double GetMeshSize(const CoordConn& MD)
{
  double hsum = 0.;
  int nedges = 0;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      {
	const int n0 = MD.connectivity[3*e+f%3]-1;
	const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	const double* X = &MD.coordinates[2*n0];
	const double* Y = &MD.coordinates[2*n1];
	hsum += (X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]);
	++nedges;
      }
  hsum /= static_cast<double>(nedges);
  return std::sqrt(hsum);
}



// Solve the state problem
void SolveState(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		std::vector<Laplacian*>& LapArray,
		std::vector<BodyForceWork*>& FrcArray,
		const LocalToGlobalMap& L2GMap,
		ScalarMap& UMap)
{
  // Initialize the scalar field to 0
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();
  
  // Assemblers
  StandardAssembler<Laplacian> AsmLap(LapArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(FrcArray, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);
  
  // Dirichlet bcs
  std::set<int> dirichletnodes;
  GetBoundary(MD, dirichletnodes);
  for(auto& n:dirichletnodes)
    {
      const double* X = &MD.coordinates[2*n];
      double val = SolDetails::GetExactStateSolution(X);
      UMap.Set(n, &val);
    }
  UMap.SetInitialized();
  
  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);

  // Assemble Laplacian & forcing
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  MatScale(PD.stiffnessMAT, -1.);
  VecScale(PD.resVEC, -1.);
  AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero residual

  // Impose Dirichlet bcs
  std::vector<int> dirichletboundary(dirichletnodes.begin(), dirichletnodes.end());
  std::vector<double> dirichletvalues(dirichletnodes.size(), 0.);
  PD.SetDirichletBCs(dirichletboundary, dirichletvalues);
  
  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution and update the state
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Clean up
  PD.Destroy();
  return;
}



// Get nodes on the inner boundary, (numbered from 0)
void GetInnerBoundary(const CoordConn& MD, std::set<int>& bdnodes)
{
  bdnodes.clear();
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  {
	    const int n = MD.connectivity[3*e+(f+i)%3]-1;
	    const double* X = &MD.coordinates[2*n];
	    double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    if(r<Ravg)
	      bdnodes.insert(n);
	  }
  return;
}





// Get segments on the outer boundary (numbered from 0)
void GetOuterBoundary(const CoordConn& MD, std::vector<int>& bdsegments)
{
  bdsegments.clear();
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r>Ravg)
	    for(int i=0; i<2; ++i)
	      bdsegments.push_back(MD.connectivity[3*e+(f+i)%3]-1);
	}
  return;
}

// Exact state solution for this problem
double SolDetails::GetExactStateSolution(const double* X)
{
  double f = fval;
  double r = std::sqrt(X[0]*X[0] + X[1]*X[1]);
  return ((f*(R0 - R1)*(R0 + R1) + 4.*(U0 - U1))*std::log(r) +
	  (-(f*std::pow(r,2)) + f*R1*R1 + 4.*U1)*std::log(R0) + 
	  (f*(r - R0)*(r + R0) - 4.*U0)*std::log(R1))/(4.*(std::log(R0) - std::log(R1)));
}


// Compute the L2 norm of the error
double ComputeStateL2Error(const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap, const double* uvals)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double L2Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nDof = ElmArray[e]->GetDof(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  double uex = SolDetails::GetExactStateSolution(&Qpts[2*q]);

	  // FE solution
	  double uh = 0.;
	  const auto& Shp = ElmArray[e]->GetShape(0);
	  for(int a=0; a<nDof; ++a)
	    uh += uvals[L2GMap.Map(0,a,e)]*Shp[q*nDof+a];

	  // Update the L2 error
	  L2Err += Qwts[q]*(uex-uh)*(uex-uh);
	}
    }
  return std::sqrt(L2Err);
}


// Compute the velocity on the outer boundary for a given state solution
void ComputeBoundaryVelocity(const CoordConn& MD, const std::vector<Element*>& ElmArray,
			     const LocalToGlobalMap& L2GMap, const double* uvals,
			     std::vector<int>& bdsegments, std::vector<double>& vel)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  const double gval = SolDetails::gval;
  bdsegments.clear();
  vel.clear();
  for(int e=0; e<MD.nodes; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>Ravg) // This segment is on the outer boundary
	    {
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      bdsegments.push_back(n0);
	      bdsegments.push_back(n1);
	      
	      // Normal to this segment 
	      const double* Y = &MD.coordinates[2*n1];
	      double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]));
	      double normal[] = {(Y[1]-X[1])/lenXY, -(Y[0]-X[0])/lenXY};
	      
	      // gradient of the solution in this element
	      double gradU[2] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int i=0; i<2; ++i)
		  gradU[i] += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,i);
	      
	      // Flux
	      double dudn = gradU[0]*normal[0]+gradU[1]*normal[1];
	      
	      // velocity
	      vel.push_back( gval*gval-dudn*dudn );
	    }
	}
  return;
}

// Solve for the smoothened velocity field
void SolveVelocity(const CoordConn& MD, const std::vector<Element*>& ElmArray,
		   const LocalToGlobalMap& L2GMap,
		   std::vector<Laplacian*>& LapArray, std::vector<L2Norm*>& L2Array,
		   const std::vector<Element*>& FluxElmArray, const LocalToGlobalMap& FluxL2GMap,
		   std::vector<BodyForceWork*>& FluxOpArray, ScalarMap& WMap)
{
  // Initialize smoothener to 0
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; WMap.Set(n, &val); }
  WMap.SetInitialized();
  
  // Mesh size
  const double meshsize = GetMeshSize(MD);
  
  // Dirichlet bcs (numbered from 0)
  std::set<int> dirichletnodes;
  GetInnerBoundary(MD, dirichletnodes);
  std::vector<int> dirichletboundary(dirichletnodes.begin(), dirichletnodes.end());
  std::vector<double> dirichletvalues(dirichletnodes.size(), 0.);
  
  // Create assemblers
  StandardAssembler<Laplacian> LapAsm(LapArray, L2GMap);
  StandardAssembler<L2Norm> L2Asm(L2Array, L2GMap);
  StandardAssembler<BodyForceWork> FluxAsm(FluxOpArray, FluxL2GMap);

  // Create PETSc data structures
  std::vector<int> nz;
  LapAsm.CountNonzeros(nz);
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  VecZeroEntries(PD.DOFArray);
  
  // Assemble M+alpha(K)
  const double alpha = 4.*meshsize*meshsize;
  LapAsm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT);
  MatScale(PD.stiffnessMAT, alpha);
  L2Asm.Assemble(&WMap, PD.resVEC, PD.stiffnessMAT, false);
  VecZeroEntries(PD.resVEC);

  // Assemble flux bcs
  FluxAsm.Assemble(&WMap, PD.resVEC);

  // Set dirichlet bcs
  PD.SetDirichletBCs(dirichletboundary, dirichletvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution
  double* inc;
  //VecScale(PD.solutionVEC, -1.); // We solve (M+alpha*K) + R = 0
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    WMap.Set(n, &inc[n]);
  WMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Normalize the solution by the H1 norm
  double norm = H1Norm(LapArray, L2Array, WMap, alpha);
  const double* wvals = &WMap.Get()[0];
  for(int n=0; n<MD.nodes; ++n)
    { double val = wvals[n]/norm; WMap.Set(n, &val); }
  WMap.SetInitialized();
  
  // Clean up
  PD.Destroy();
  return;
}

// Compute the h1 norm of a function: (u^2 + alpha grad(u).grad(u))
double H1Norm(const std::vector<Laplacian*>& LapArray, const std::vector<L2Norm*>& L2Array,
	      const ScalarMap& WMap, const double alpha)
{
  double Energy = 0.;
  for(auto& it:LapArray)
    Energy += 2.*alpha*it->GetEnergy(&WMap);
  for(auto& it:L2Array)
    Energy += 2.*it->GetEnergy(&WMap);
  return std::sqrt(Energy);
}


// Plot the solution on the outer boundary
void PlotOuterBdSolution(const char* filename, const CoordConn& MD, const double* wvals)
{
  // Get the list of boundary segments
  std::vector<int> bdsegments;
  GetOuterBoundary(MD, bdsegments);
  std::set<int> bdnodes(bdsegments.begin(), bdsegments.end());
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  for(auto& n:bdnodes)
    {
      const double* X = &MD.coordinates[2*n];
      double theta = std::atan2(X[1], X[0]);
      pfile<<theta<<" "<<" "<<wvals[n]<<"\n";
    }
  pfile.flush();
  pfile.close();
  return;
}  


// Plot the prescribed velocity field
void PlotVelocity(const char* filename, const CoordConn& MD,
		  const std::vector<int>& FluxSegments, const std::vector<double>& FluxVals)
{
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  const int nSeg = static_cast<int>(FluxVals.size());
  for(int e=0; e<nSeg; ++e)
    {
      const int n0 = FluxSegments[2*e];
      const int n1 = FluxSegments[2*e+1];
      double X[2];
      for(int k=0; k<2; ++k)
	X[k] = 0.5*(MD.coordinates[2*n0+k]+MD.coordinates[2*n1+k]);
      double theta = std::atan2(X[1],X[0]);
      pfile<<theta<<" "<<FluxVals[e]<<"\n";
    }
  pfile.flush();
  pfile.close();
}
