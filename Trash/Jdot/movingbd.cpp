// Sriramajayam

#include "triangulate.h"
#include <um_TriangleModule>
#include <bp_Laplacian2DModule>
#include <geom_PlanarCircle.h>
#include <geom_LevelSetManifold.h>
#include <ConstantForceField.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <omp.h>

// Implicit function for an annular domain
struct AnnulusSpecs
{
  const geom::PlanarCircle inCirc;
  const geom::PlanarCircle outCirc;
  AnnulusSpecs(const std::vector<double> center, const double R0, const double R1)
    :inCirc(center, R0, false), outCirc(center, R1, true) {}
};
void AnnulusLevelSetFunction(const double*, double& F, double*, double*, void*);

// Create an equilateral background mesh
// Returns the mesh size
double GetEquilateralMesh(std::vector<double>& coordinates, std::vector<int>& connectivity);

// Helper struct for solution information
struct SolDetails
{
  std::vector<double> center;
  double R0, R1;
  double fval, gval, U0, U1; 
  double Jval;
  double L2Err;
};

// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol,
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Returns the exact solution
double GetExactSolution(const double* X, const SolDetails& sol);

// Compute the L2 norm of the error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* uvals,
		      const SolDetails& sol);

// Compute J
double ComputeJ(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		const double* uvals, const SolDetails& sol);

// Compute the finite element solution
void Solve(const CoordConn& MD, SolDetails& sol);

int main(int argc, char** argv)
{
  omp_set_num_threads(1);

  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Coordinates, connectivity and background mesh
  constexpr int SPD = 2;
  std::vector<int> connectivity;
  std::vector<double> coordinates;
  const double meshsize = GetEquilateralMesh(coordinates, connectivity);
  std::cout<<"\nMesh size = "<<meshsize<<std::flush;
  msh::SeqCoordinates Coord(SPD, coordinates);
  msh::StdTriConnectivity Conn(connectivity);
  msh::StdTriMesh BG(Coord, Conn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Geometry of an annulus
  const double R0 = 0.2;
  const double R1 = 0.75;
  AnnulusSpecs annSpecs(std::vector<double>({0.0,0.0}), R0, R1);
  geom::LevelSetFunction annFunc = AnnulusLevelSetFunction;
  geom::NLSolverParams nlparams({1.e-6, 1.e-6, 20}); // ftol, ttol, max_iter
  geom::LevelSetManifold<SPD> annGeom(nlparams, annFunc, &annSpecs);

  // Get a mesh for the annular region
  Triangulate(BG, annGeom, (char*)"annmesh.tec");
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"annmesh.tec", MD);

  // Details of the problem to solve
  SolDetails sol;
  sol.center = std::vector<double>({0.,0.});
  sol.R0 = R0; sol.R1 = R1;
  sol.fval = 4.;
  sol.gval = 2.0271851144019437;
  sol.U0 = 1.; sol.U1 = 0.5;

  // Solve
  Solve(MD, sol);

  // Print solution details
  std::cout<<"\nMesh size: "<<meshsize
	   <<"\nL2 error: "<<sol.L2Err//<<"\nJ: "<<sol.Jval
	   <<std::flush;
  
  // Finalize PETSc
  PetscFinalize(); 
}


// Compute the finite element solution
void Solve(const CoordConn& MD, SolDetails& sol)
{
  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    ElmArray[e] = new P12DElement<1>(MD.connectivity[3*e], MD.connectivity[3*e+1], MD.connectivity[3*e+2]);

  // Local to global map
  StandardP12DMap L2GMap(ElmArray);

  // Create a scalar field, initialize to 0
  bp::ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();

  // Create a constant force field
  std::vector<double> fval({-sol.fval});
  ConstantForceField Frc(fval);

  // Create operations
  std::vector<bp::Laplacian*> OpArray(MD.elements);
  std::vector<BodyForceWork*> FrcOps(MD.elements);
  std::vector<int> Fields({0});
  for(int e=0; e<MD.elements; ++e)
    {
      bp::ScalarMapAccess SMAccess
	(std::vector<int>({MD.connectivity[3*e]-1,   MD.connectivity[3*e+1]-1, MD.connectivity[3*e+2]-1}));
      OpArray[e] = new bp::Laplacian(ElmArray[e], SMAccess, 0);
      FrcOps[e] = new BodyForceWork(ElmArray[e], Frc, Fields, SMAccess);
    }

  // Assemblers
  StandardAssembler<bp::Laplacian> AsmLap(OpArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(FrcOps, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);

  // Get Dirichlet bcs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, sol, boundary, bvalues);

  // Update the state using the boundary conditions
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);

  // Assemble laplacian and forcing contributions
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  AsmFrc.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT, false); // Don't zero.

  // BCs
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution and update state
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
 
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);

  // Compute the L2 norm of the error
  sol.L2Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get())[0], sol);

  // Compute J
  sol.Jval = ComputeJ(ElmArray, L2GMap, &UMap.Get()[0], sol);

  // Clean up
  for(int e=0; e<MD.elements; ++e)
    {
      delete ElmArray[e];
      delete OpArray[e];
      delete FrcOps[e];
    }
  PD.Destroy();
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol,
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(auto e=0; e<MD.elements; ++e)
    for(auto f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(auto i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      bvalues.push_back( GetExactSolution(&MD.coordinates[2*it], sol) );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
}


// Returns the exact solution
double GetExactSolution(const double* X, const SolDetails& sol)
{
  //return std::exp(X[0])*std::sin(X[1]);
  auto& R0 = sol.R0; auto& R1 = sol.R1;
  auto& center = sol.center;
  auto& U0 = sol.U0; auto& U1 = sol.U1;
  double f = sol.fval;
  double r = std::sqrt((X[0]-center[0])*(X[0]-center[0]) + (X[1]-center[1])*(X[1]-center[1]));
  
  return ((f*(R0 - R1)*(R0 + R1) + 4.*(U0 - U1))*std::log(r) +
	  (-(f*std::pow(r,2)) + f*R1*R1 + 4.*U1)*std::log(R0) + 
	  (f*(r - R0)*(r + R0) - 4.*U0)*std::log(R1))/(4.*(std::log(R0) - std::log(R1)));
}



// Compute the L2 norm of the error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* uvals,
		      const SolDetails& sol)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double L2Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nDof = ElmArray[e]->GetDof(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  double uex = GetExactSolution(&Qpts[2*q], sol);

	  // FE solution
	  double uh = 0.;
	  const auto& Shp = ElmArray[e]->GetShape(0);
	  for(int a=0; a<nDof; ++a)
	    uh += uvals[L2GMap.Map(0,a,e)]*Shp[q*nDof+a];

	  // Update the L2 error
	  L2Err += Qwts[q]*(uex-uh)*(uex-uh);
	}
    }
  return std::sqrt(L2Err);
}



// Compute J
double ComputeJ(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		const double* uvals, const SolDetails& sol)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double Jval = 0.;
  const auto& fval = sol.fval;
  const auto& gval = sol.gval;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const int nDof = ElmArray[e]->GetDof(0);
      const auto& Shp = ElmArray[e]->GetShape(0);
      const auto& DShp = ElmArray[e]->GetDShape(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Solution and its derivative
	  double du[2] = {0.,0.};
	  double u = 0.;
	  for(int a=0; a<nDof; ++a)
	    {
	      auto& ua = uvals[L2GMap.Map(0,a,e)];
	      u += ua*Shp[q*nDof+a];
	      for(int i=0; i<2; ++i)
		du[i] += ua*DShp[q*nDof*2 + 2*a + i];
	    }
	  // Update J
	  Jval += Qwts[q]*(du[0]*du[0]+du[1]*du[1]-2.*fval*u+gval*gval);
	}
    }
  return Jval;
}
	    

  
// Create an equilateral background mesh
double GetEquilateralMesh(std::vector<double>& coordinates, std::vector<int>& connectivity)
{
  double center[] = {0.01, 0.01};
  coordinates.clear(); connectivity.clear();
  coordinates.push_back(center[0]);
  coordinates.push_back(center[1]);
  for(auto i=0; i<6; ++i)
    {
      double angle = static_cast<double>(i)*M_PI/3.;
      coordinates.push_back(center[0]+std::cos(angle));
      coordinates.push_back(center[1]+std::sin(angle));
    }
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  connectivity.assign(connarray, connarray+6*3);

  // Subdivide
  int ndiv = 4;
  double meshsize = 1.;
  PetscBool flag;
  PetscOptionsGetInt(NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    {
      msh::SubdivideTriangles(2, connectivity, coordinates);
      meshsize /= 2.;
    }
  return meshsize;
}


// Level set function for an annulus
void AnnulusLevelSetFunction(const double* X, double& F, double* dF, double* d2F, void* params)
{
  const AnnulusSpecs* annParams = static_cast<const AnnulusSpecs*>(params);
  assert(annParams!=nullptr && "AnnulusLevelSetFunction- could not cast annulus parameters");

  // Distance of the given point to the inner & outer boundaries
  double sd1, sd2;
  annParams->inCirc.GetSignedDistance(X, sd1);
  annParams->outCirc.GetSignedDistance(X, sd2);
  if(std::abs(sd1)<std::abs(sd2))
    annParams->inCirc.GetSignedDistance(X, F, dF, d2F);
  else
    annParams->outCirc.GetSignedDistance(X, F, dF, d2F);
  return;
}



