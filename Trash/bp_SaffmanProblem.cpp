// Sriramajayam

#include <bp_SaffmanProblem.h>

namespace vssc
{
  // Constructor
  SaffmanProblem::SaffmanProblem(const double* center,
				 const double* sizes,
				 msh::StdTriMesh& bg,
				 std::vector<double>& bdpts,
				 std::vector<double>& bdnormals,
				 const SSD_Options& opt1,
				 const Meshing_Options& opt2,
				 const geom::NLSolverParams& opt3)
    :domain_center(center, center+2),
     domain_sizes(sizes, sizes+2),
     BernoulliProblem2D(bg, bdpts, bdnormals, opt1, opt2, opt3) {}

  // Destructor
  SaffmanProblem::~SaffmanProblem() {}


  // Computes a sequnential chain of vertices
  // Assumes that the chain is connected
  // VertConnMap[a] = b codifies the edge a --> b
  void GetPositiveVertexChain(const std::map<int, int>& VertConnMap,
			      std::vector<int>& VertChain)
  {
    // Use a list
    std::list<int> VertList;
    VertList.clear();

    // Use an arbitrary starting vertex 
    auto it = VertConnMap.begin();
    // Build the chain
    int curr_vert = it.first;
    while(it!=VertConnMap.end())
      {
	VertList.push_back(it.first);
	curr_vert = it.second;
	it = VertConnMap.find(curr_vert);
      }

    // Build the inverse edge connection map
    std::map<int, int> InvVertConnMap({});
    for(auto& connit:VertConnMap)
      InvVertConnMap[connit.second] = connit.first;
    
    // Proceed in the reverse direction starting from VertList[0]
    curr_vert = *VertList.begin();
    it = InvVertConnMap.find(curr_vert);
    while(it!=InvVertConnMap.end())
      {
	curr_vert = it.second;
	VertList.push_front(curr_vert);
	it = InvVertConnMap.find(curr_vert);
      }

    // Convert the list to a vector
    VertChain.clear();
    for(auto& itL:VertList)
      VertChain.push_back( *itL );

    // Check the computed chain
    const int nVerts = static_cast<int>(VertChain.size());
    assert(InvVertConnMap.find(VertChain[0])==InvVertConnMap.end());
    assert(VertConnMap.find(VertChain[nVerts-1])==VertConnMap.end());
    for(int i=0; i<nVerts-1; ++i)
      {
	auto itconn = VertConnMap.find(VertChain[i]);
	assert(itconn!=VertConnMap.end());
	assert(itconn->second==VertChain[i+1]);
      }
    return;	
  }

  
  // Helper function to setup the geometry and mesh
  void SaffmanProblem::SetupBoundaryAndMesh()
  {
    // Limit signed distance calculations to a vicinity of the boundary
    // Region of interest = bounding box for the boundary points + buffer
    const int nBdPoints = static_cast<int>(BdPoints.size()/2);
    double xmin = BdPoints[0];
    double xmax = BdPoints[0];
    double ymin = BdPoints[1];
    double ymax = BdPoints[1];
    for(int n=1; n<BdPoints; ++n)
      {
	const double& x = BdPoints[2*n];
	const double& y = BdPoints[2*n+1];
	if(x<xmin) xmin = x;
	if(x>xmax) xmax = x;
	if(y<ymin) ymin = y;
	if(y>ymax) ymax = y;
      }
    xmin -= 0.05*domain_sizes[0];
    xmax += 0.05*domain_sizes[0];
    ymin -= 0.05*domain_sizes[1];
    ymax += 0.05*domain_sizes[2];
				
  

    /*
    const double center[] = {0.5*(xmin+xmax), 0.5*(ymin+ymax)};
    double xsize = xmax-xmin;
    double ysize = ymax-ymin;
    double buffer = (xsize>ysize) ? 0.1*xsize : 0.1*ysize;
    xsize += buffer;
    ysize += buffer;

    // Create a mesh over the rectangular bounding box
    std::vector<double> maxent_coordinates({0.,0.,
	  -xsize/2.,-ysize/2., xsize/2.,-ysize/2.,
	  xsize/2.,ysize/2., -xsize/2.,ysize/2.});
    for(int n=0; n<5; ++n)
      for(int k=0; k<2; ++k)
	maxent_coordinates[2*n+k] += center[k];
    std::vector<int> maxent_connectivity = ({0,1,2, 0,2,3, 0,3,4, 0,4,1});
    for(int i=0; i<3; ++i)
      msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
    std::vector<double> maxent_nodeset = maxent_coordinates;
    for(int i=0; i<2; ++i)
      msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
    */

    // Compute the SSD for the set of boundary points and normals
    const auto& ssdopts = GetSSDOptions();
    GetSmoothedSignedDistance(BdPoints, BdNormals, ssdopts, BdFit);

    // Create an implicit geometry
    const auto& geomopts = GetGeomSolverParams();
    Geom = new geom::ImplicitMaxEntManifold<mx::MaxEnt2D>(BdFit->GetMaxEntFunctions(),
							  BdFit->GetDofs(),
							  geomopts,
							  1); // Number of threads

    // Get a conforming mesh for the immersed geometry
    // -----------------------------------------------
    const auto& mshopts = GetMeshingOptions();
    
    // Create a workspace for the background mesh
    um::SDWorkspace<msh::StdOrphanMesh, geom::ImplicitManifold>  BGws(BG, Geom);
    
    // Evaluate the implicit function at all nodes.
    // Signed distances are not required outside the region of interest
    const int nbgnodes = BG.GetNumNodes();
    um::SignedDistance sd;
    for(int i=0; i<nbgnodes; ++i)
      {
	const double* X = BG.coordinates(i);
	if(X[0]>xmin && X[0]<xmax && X[1]>ymin && X[1]<ymax)
	  {
	    Geom.GetImplicitFunction(X, sd.value);
	    if(sd.value>0.) sd.sign = um::SDSignature::Plus;
	    else sd.sign = um::SDSignature::Minus;
	    BGws.Set(i, sd);
	  }
      }

    // Create domain triangulator
    um::DomainTriangulator<msh::StdOrphanMesh, geom::ImplicitManifold> Mshr(BGws);
    
    // Get the working mesh
    um::WorkingMesh<msh::StdOrphanMesh>& WM = Mshr.GetWorkingMesh();

    // Positive vertices
    const auto& PosVerts = Mshr.GetPositiveVertices();
    
    // Identify end points in the chain of positive vertices
    // These will have to be reprojected onto the domain boundary
    
    
    

  }
  
}
