// Sriramajayam

#include <bp_MaxEntCurve.h>

namespace bp{
  namespace detail{
    
    // Implicit function evaluation for 2D max-ent smoothed signed distance
    void MaxEntCurveLevelSetFunction(const double*X, double& fval,
				     double* dfval, double* d2fval, void* params)
    {
      assert(params!=nullptr && "bp::ImplicitMaxEntCurve::GetImplicitFunction- params is null");
      auto* Fit = static_cast<MaxEntCurveFit*>(params);
      assert(Fit!=nullptr && "bp::ImplicitMaxEntCurve::GetImplicitFunction- Could not cast parameters");
      Fit->Evaluate(X, fval, dfval, d2fval);
    }
    
  }
}
  
