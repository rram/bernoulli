// Sriramajayam

#include <cmath>
#include <algorithm>
#include <unordered_set>
#include <cassert>
#include <array>

// FE utilities from DG++, local
#include "P12DElement.h"
#include "PlottingUtils.h"
#include "MeshUtils.h"
#include "Laplacian.h"

// Meshing related utilities from UM++
#include "PlanarCircle.h"
#include "Triangulator.h"
#include "StdTriMesh.h"
#include "GeomTri2DQuality.h"
#include "TriVertOptimizer.h"

// Create a mesh of equilateral triangles
double GetMesh(CoordConn& MD, const double rIn, const double rOut);


// Triangulates a given domain
void GetMesh(const Mshr::Codim1Manifold& Geom, const Mshr::Orientation triSide,
	     double hsize, CoordConn& MD);

// Determines the Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const double rin, const double rout, const double hval,
		     std::vector<int>& boundary,  std::vector<double>& bvalues);

// Compute smoothed-nodal fluxes
void ComputeBoundaryFluxes(const CoordConn& MD, const double& InRad, const double& OutRad,
			   std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap,
			   Vec& res,
			   std::vector<std::array<int, 2>>& BdFaces,
			   std::unordered_map<int, double>& fluxes);

  
// Compute the L2-norm of the error in the solution
double ComputeL2Error(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		      const double* sol, 
		      const double InRad, const double OutRad, const double hval);

// Compute the H1 seminorm of the error
double ComputeH1Error(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		      const double* sol, const double InRad, const double OutRad, const double hval);

// Compute the L2 norm of the boundary normal flux
double ComputeL2FluxError(const CoordConn& MD, const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
			  const double* sol, const double InRad, const double OutRad, const double hval);

// Compute the error in postprocessed fluxes
double ComputeL2FluxError(const CoordConn& MD,
			  const double InRad, const double OutRad, const double hval,
			  const std::vector<std::array<int,2>>& BdFaces,
			  const std::unordered_map<int, double>& Fluxes);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create inner boundary which is fixed
  const double InRad = 0.5;
  Mshr::PlanarCircle InCirc(std::vector<double>({0.,0.}), InRad);

  // Create outer boundary which is moving
  const double OutRad = 1.2;
  Mshr::PlanarCircle OutCirc(std::vector<double>({0.,0.}), OutRad);
  
  // Create a background mesh of equilateral triangles
  CoordConn MD;
  const double hsize = GetMesh(MD, InRad, OutRad);

  // Recover a mesh with the inner and outer domains removed
  GetMesh(InCirc, Mshr::Orientation::Plus, hsize, MD);
  GetMesh(OutCirc, Mshr::Orientation::Minus, hsize, MD);
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements and operations
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<DResidue*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      ElmArray[e] = new P12DElement<1>(MD.connectivity[3*e],
				       MD.connectivity[3*e+1],
				       MD.connectivity[3*e+2]);
      OpArray[e] = new Laplacian(ElmArray[e], 0);
    }

  // Local to global map
  StandardP12DMap L2GMap(ElmArray);

  // Create PETSc data structures
  PetscData PD;
  InitializePetscData(L2GMap, L2GMap.GetTotalNumDof(), PD);
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);

  // Get dirichlet BCs
  const double hval = 1.;
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, InRad, OutRad, hval, boundary, bvalues);

  // Assemble matrix vector syste,
  assert(DResidue::Assemble(OpArray, L2GMap, PD.DOFArray, &PD.resVEC, &PD.stiffnessMAT) &&
	 "Could not assemble matrix-vector system.\n");
  
  // Set Dirichlet BCs
  SetDirichletBCs(PD, boundary, bvalues);

  // Solve
  Solve(PD);
  VecScale(PD.solutionVEC, -1.);

  // Recompute the residual
  VecSet(PD.resVEC, 0.);
  assert(DResidue::Assemble(OpArray, L2GMap, PD.solutionVEC, &PD.resVEC, nullptr) &&
	 "Could not assemble residual.\n");
  VecAssemblyBegin(PD.resVEC);
  VecAssemblyEnd(PD.resVEC);

  // Compute smoothed-nodal fluxes
  std::unordered_map<int, double> fluxes;
  std::vector<std::array<int, 2>> BdFaces;
  ComputeBoundaryFluxes(MD, InRad, OutRad, OpArray, L2GMap, PD.resVEC, BdFaces, fluxes);
  
  double* sol;
  VecGetArray(PD.solutionVEC, &sol);
  PlotTecCoordConnWithNodalFields((char*)"sol.tec", MD, sol, 1);
  
  // Compute the L2-norm of the error in the solution
  double L2Err = ComputeL2Error(ElmArray, L2GMap, sol, InRad, OutRad, hval);
  std::cout<<"\n"<<L2Err; std::fflush( stdout );

  // Compute H1-semi norm of the error
  double H1Err = ComputeH1Error(ElmArray, L2GMap, sol, InRad, OutRad, hval);
  std::cout<<"\n"<<H1Err; std::fflush( stdout );

  // Compute the L2 norm of the boundary normal flux
  double FluxErr = ComputeL2FluxError(MD, ElmArray, L2GMap, sol, InRad, OutRad, hval);
  std::cout<<"\n"<<FluxErr; std::fflush( stdout );

  // Compute the error in postprocessed fluxes
  double PPFluxErr = ComputeL2FluxError(MD, InRad, OutRad, hval, BdFaces, fluxes);
  std::cout<<"\n"<<PPFluxErr; std::fflush( stdout );
  
  // Clean up
  VecRestoreArray(PD.solutionVEC, &sol);
  DestroyPetscData(PD);
  for(int e=0; e<MD.elements; ++e)
    {
      delete ElmArray[e];
      delete OpArray[e];
    }
  PetscFinalize();
  std::cout<<"\n---done---\n"; std::fflush( stdout );

}

// Create a mesh of equilateral triangles
double GetMesh(CoordConn& MD, const double rIn, const double rOut)
{
  assert(rIn<rOut && "GetMesh()- Expected inner radius < outer radius.\n");
  MD.coordinates.reserve(2*7);
  MD.coordinates.push_back(0.);
  MD.coordinates.push_back(0.);
  const double rad = 1.1*rOut/sin(M_PI/3.);
  for(int i=0; i<6; ++i)
    {
      MD.coordinates.push_back( rad*cos(double(i)*M_PI/3.) );
      MD.coordinates.push_back( rad*sin(double(i)*M_PI/3.) );
    }
  MD.coordinates.shrink_to_fit();
  MD.connectivity.assign({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 7;
  MD.elements = 6;

  // Mesh size required
  double h1 = rIn/2.;
  double h2 = (rOut-rIn)/6.;
  double hreq = (h1<h2) ? h1 : h2;
  int ndiv = std::ceil( log2(rad/hreq) );
  double hsize = rad;
  ndiv++;
  // ndiv can also be set from the commandline
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, (char*)"-ndiv", &ndiv, &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      hsize /= 2.;
    }
  assert(ndiv<8 && "Performing more than 8 subdivision.\n");
  return hsize;
}


// Function to compute the relaxation direction
void fRelaxDir(const int iternum, const int nodenum, const Mshr::WMWorkspace& WmWs,
	       void* usrparams, double* rdir)
{
  if(iternum%2==0)
    { rdir[0] = 0.; rdir[1] = 1.; }
  else
    { rdir[0] = 1.; rdir[1] = 0.; }
  return;
}

// Function to identify the vertices to relax during optimization
void fRelaxNbd(const Mshr::WMWorkspace& WmWs,
	       const Mshr::Orientation triSide,
	       void* usrparams,
	       std::vector<int>& vertices)
{
  vertices.clear();

  // Get the mesh size
  const double hsize = *((double*)usrparams);

  // Sign representing the triangulated side
  const Mshr::SDSignature trisign = (triSide==Mshr::Orientation::Minus)? Mshr::SDSignature::Minus : Mshr::SDSignature::Plus;

  // Use only a subset of vertices of the working mesh where signed distances are known
  const auto& SD = WmWs.Get();
  for(auto& it:SD)
    if(it.second.sign==trisign)
      if(std::abs(it.second.value)<5.*hsize)
	vertices.push_back(it.first);
}


// Convert a working mesh to CoordConn format
void ConvertWorkingMesh(const Mshr::WorkingMesh& WM,
			std::vector<double>& Coord,
			std::vector<int>& Conn)
{
  const auto& ElmRanges = WM.GetElements().GetIntervals();
  const int nIntervals = int(ElmRanges.size())/2;

  // Mapping from old to new node numbers
  int nElements = 0;
  std::unordered_map<int, int> Old2NewNumMap;
  for(int i=0; i<nIntervals; ++i)
    for(int e=ElmRanges[2*i]; e<=ElmRanges[2*i+1]; ++e)
      {
	++nElements;
	const auto* elmconn = WM.connectivity(e);
	for(int a=0; a<3; ++a)
	  Old2NewNumMap[elmconn[a]] = -1;
      }
  int nnodes = 0;
  for(auto& it:Old2NewNumMap)
    it.second = nnodes++;

  // Insert coordinates with new node numbers
  Coord.resize(2*nnodes);
  for(auto& it:Old2NewNumMap)
    {
      const auto& oldnum = it.first;
      const auto& newnum = it.second;
      const auto* coord = WM.coordinates(oldnum);
      for(int k=0; k<2; ++k)
	Coord[2*newnum+k] = coord[k];
    }

  // Insert elements with new node numbers
  Conn.reserve(3*nElements);
  for(int i=0; i<nIntervals; ++i)
    for(int e=ElmRanges[2*i]; e<=ElmRanges[2*i+1]; ++e)
      {
	const auto* elmconn = WM.connectivity(e);
	for(int a=0; a<3; ++a)
	  Conn.push_back( Old2NewNumMap[elmconn[a]] );
      }
  Conn.shrink_to_fit();
}
  
	    
	  

// Triangulates a given domain
void GetMesh(const Mshr::Codim1Manifold& Geom,
	     const Mshr::Orientation triSide,
	     double hsize,
	     CoordConn& MD)
{
  // Renumber nodes in the background mesh starting from 0
  for(auto& it:MD.connectivity)
    it = it-1;

  // Create background mesh in UM++ format
  Mshr::SeqCoordinates Coord(2, MD.coordinates);
  Mshr::StdTriConnectivity Conn(MD.connectivity);
  Mshr::StdTriMesh BG(Coord, Conn);

  // Workspace for background mesh with signed distances
  Mshr::UMWorkspace BgWs(BG, Geom);
  const unsigned int nnodes = BG.GetNumNodes();
  for(unsigned int i=0; i<nnodes; ++i)
    BgWs.Evaluate(i);

  // Working mesh and a workspace for it
  Mshr::WorkingMesh WM(BG);
  Mshr::WMWorkspace WmWs(WM, Geom);

  // Create quality metric
  Mshr::GeomTri2DQuality<decltype(WM)> Quality(WM);

  // Create optimizer for the working mesh
  Mshr::TriWMVertOptimizer TriOpt(WM, Quality);

  // Helper struct for optimization parameters
  const int nIterations = 20;
  std::function<decltype(fRelaxDir)> funcRelaxDir(&fRelaxDir);
  std::function<decltype(fRelaxNbd)> funcRelaxNbd(&fRelaxNbd);
  Mshr::TriOptParams  TOPs({TriOpt, nIterations, &hsize, funcRelaxDir, funcRelaxNbd});

  // Input parameters for the triangulator
  const int nSteps = 3;
  Mshr::TriParams inParams(BgWs, nSteps, triSide, TOPs);

  // Output parameters from the triangulator
  std::vector<std::array<int,2>> PosElmsFaces;
  Mshr::TriResult outParams({WM, WmWs, PosElmsFaces});

  // Triangulate
  Mshr::TriangulateDomain(inParams, outParams);

  // Convert working mesh into sequentially numbered CoordConn format
  MD.connectivity.clear();
  MD.coordinates.clear();
  ConvertWorkingMesh(WM, MD.coordinates, MD.connectivity);
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = int(MD.coordinates.size()/2);
  MD.elements = int(MD.connectivity.size())/3;
  for(auto& it:MD.connectivity)
    ++it;
  
  return;
}


// Determines the Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const double rin, const double rout,
		     const double hval,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  assert(rin<rout && "GetDirichletBCs: expected rIn < rOut");
  
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on boundary faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert( MD.connectivity[3*e+(f+i)%3]-1 );

  boundary.clear();
  bvalues.clear();
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  double rin2 = rin*rin;
  double rout2 = rout*rout;
  double rEPS = (rout2-rin2)/3.;
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      
      const auto& x = MD.coordinates[2*it];
      const auto& y = MD.coordinates[2*it+1];
      double r2 = x*x+y*y;
      if(std::abs(r2-rin2)<rEPS)
	bvalues.push_back( hval );
      else if(std::abs(r2-rout2)<rEPS)
	bvalues.push_back( 0. );
      else
	assert(false && "GetDirichletBCs- node does not lie on boundary.\n");
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
}


// Compute the L2-norm of the error in the solution
double ComputeL2Error(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		      const double* sol, const double InRad, const double OutRad, const double hval)
{
  double L2Err = 0.;
  const unsigned int nElements = ElmArray.size();
  for(unsigned int e=0; e<nElements; ++e)
    {
      const auto* Elm = ElmArray[e];
      const int nShapes = Elm->GetDof(0);
      const auto& Qwts = Elm->GetIntegrationWeights(0);
      const auto& Qpts = Elm->GetIntegrationPointCoordinates(0);
      const int nQuad = int(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  double r = sqrt(Qpts[2*q]*Qpts[2*q] + Qpts[2*q+1]*Qpts[2*q+1]);
	  double Uex = hval*log(r/OutRad)/log(InRad/OutRad);

	  // Computed solution
	  double Uh = 0.;
	  for(int a=0; a<nShapes; ++a)
	    Uh += sol[L2GMap.Map(0,a,e)]*Elm->GetShape(0,q,a);

	  // Update error
	  L2Err += Qwts[q]*(Uh-Uex)*(Uh-Uex);
	}
    }
  return sqrt(L2Err);
}


// Compute the H1 seminorm of the error
double ComputeH1Error(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		      const double* sol, const double InRad, const double OutRad, const double hval)
{
  double H1Err = 0.;
  const unsigned int nElements = ElmArray.size();
  for(unsigned int e=0; e<nElements; ++e)
    {
      const auto* Elm = ElmArray[e];
      const int nShapes = Elm->GetDof(0);
      const auto& Qwts = Elm->GetIntegrationWeights(0);
      const auto& Qpts = Elm->GetIntegrationPointCoordinates(0);
      const int nQuad = int(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  // Derivatives of exact solution
	  double r2 = (Qpts[2*q]*Qpts[2*q] + Qpts[2*q+1]*Qpts[2*q+1]);
	  double factor = hval/log(InRad/OutRad);
	  double dUex[] = {factor*Qpts[2*q]/r2, factor*Qpts[2*q+1]/r2};
	  
	  // Derivatives of computed solution
	  double dUh[] = {0.,0.};
	  for(int a=0; a<nShapes; ++a)
	    for(int J=0; J<2; ++J)
	      dUh[J] += sol[L2GMap.Map(0,a,e)]*Elm->GetDShape(0,q,a,J);

	  // Update error
	  H1Err += Qwts[q]*( (dUh[0]-dUex[0])*(dUh[0]-dUex[0]) + (dUh[1]-dUex[1])*(dUh[1]-dUex[1]) );
	}
    }
  return sqrt(H1Err);
}


// Compute the L2 norm of the boundary normal flux
double ComputeL2FluxError(const CoordConn& MD,
			  const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
			  const double* sol, const double InRad, const double OutRad, const double  hval)
{
  assert(InRad<OutRad && "ComputeL2FluxError()- Expected rIn < rOut.\n");

  // Element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Tolerance to identify faces along rOut
  double rEPS = (OutRad-InRad)/3.;
  std::vector<std::array<int,2>> BdFaces;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  bool flag = true;
	  for(int i=0; i<2 && true==flag; ++i)
	    {
	      int n = MD.connectivity[3*e+(f+i)%3]-1;
	      double x = MD.coordinates[2*n];
	      double y = MD.coordinates[2*n+1];
	      double r = sqrt(x*x+y*y);
	      if(std::abs(r-OutRad)>rEPS)
		flag = false;
	    }

	  // Does this face lie along the outer boundary
	  if(flag==true)
	    BdFaces.push_back( std::array<int,2>({e,f}) );
	}

  // Integrate error along boundary faces
  double L2Err = 0.;

  for(auto& it:BdFaces)
    {
      const auto e = it[0];
      const auto f = it[1];

      // Computed solution: Use the fact that gradients are piecewise constants
      const auto* Elm = ElmArray[e];
      const int nShapes = Elm->GetDof(0);
      double gradU[] = {0.,0.};
      for(int i=0; i<nShapes; ++i)
	for(int J=0; J<2; ++J)
	  gradU[J] += sol[L2GMap.Map(0,i,e)]*Elm->GetDShape(0,0,i,J);

      // Nodes on this face
      const int a = MD.connectivity[3*e+f]-1;
      const int b = MD.connectivity[3*e+(f+1)%3]-1;
      const double A[] = {MD.coordinates[2*a], MD.coordinates[2*a+1]};
      const double B[] = {MD.coordinates[2*b], MD.coordinates[2*b+1]};

      // Length of this face
      double len = sqrt( (A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]) );

      // Normal to this face
      double normal[] = {(B[1]-A[1])/len, (A[0]-B[0])/len};

      // Normal flux
      double dudN = gradU[0]*normal[0] + gradU[1]*normal[1];

      // Exact solution
      double dudNEx = (hval/log(InRad/OutRad))*(1./OutRad);
     
      // Update error
      L2Err += len*std::abs((dudN-dudNEx)*(dudN-dudNEx));
      //L2Err += len*((dudN-dudNEx)*(dudN+dudNEx));
    }
  return sqrt(L2Err);
}


// Compute smoothed-nodal fluxes
void ComputeBoundaryFluxes(const CoordConn& MD, const double& InRad, const double& OutRad,
			   std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap,
			   Vec& res,
			   std::vector<std::array<int, 2>>& BdFaces,
			   std::unordered_map<int, double>& Fluxes)
{

  // Identify the list of boundary faces
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double rEPS = (OutRad-InRad)/3.;
  
  // Identify boundary faces and nodes
  BdFaces.clear();
  Fluxes.clear(); // Store lumped masses here
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  // Does this face lie along the outer boundary
	  const int a = MD.connectivity[3*e+f]-1;
	  const int b = MD.connectivity[3*e+(f+1)%3]-1;
	  const double A[] = {MD.coordinates[2*a], MD.coordinates[2*a+1]};
	  double rA = sqrt(A[0]*A[0] + A[1]*A[1]);
	  const double B[] = {MD.coordinates[2*b], MD.coordinates[2*b+1]};
	  double rB = sqrt(B[0]*B[0] + B[1]*B[1]);
	  if(std::abs(rA-OutRad)<rEPS && std::abs(rB-OutRad)<rEPS)
	    {
	      // Record this edge
	      BdFaces.push_back( std::array<int,2>({e, f}) );

	      // Update lumped masses at the nodes
	      double len = sqrt( (A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]) );

	      auto itA = Fluxes.find(a);
	      if(itA==Fluxes.end()) Fluxes[a] = 0.5*len;
	      else itA->second += 0.5*len;

	      auto itB = Fluxes.find(b);
	      if(itB==Fluxes.end()) Fluxes[b] = 0.5*len;
	      else itB->second += 0.5*len;
	    }
	}
  
  // Get the values of residuals
  double* resvals;
  VecGetArray(res, &resvals);
  for(auto& it:Fluxes)
    {
      const int n = it.first;
      const double mass = it.second;
      it.second = resvals[n]/mass;
    }
  VecRestoreArray(res, &resvals);
}


// Compute the error in postprocessed fluxes
double ComputeL2FluxError(const CoordConn& MD,
			  const double InRad, const double OutRad, const double hval,
			  const std::vector<std::array<int,2>>& BdFaces,
			  const std::unordered_map<int, double>& Fluxes)
{
  double L2Err = 0.;

  // Exact value of flux:
  double Fex = hval/(OutRad*log(InRad/OutRad));
  
  // Quadrature rule
  const Line_1 Qd;
  const int nQuad = Qd.Bulk->GetNumberQuadraturePoints();

  for(auto& it:BdFaces)
    {
      // This face
      const auto& e = it[0];
      const auto& f = it[1];
      const int a = MD.connectivity[3*e+f]-1;
      const int b = MD.connectivity[3*e+(f+1)%3]-1;
      const double A[] = {MD.coordinates[2*a], MD.coordinates[2*a+1]};
      const double B[] = {MD.coordinates[2*b], MD.coordinates[2*b+1]};
      const double len = sqrt((A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]));
      const auto itA = Fluxes.find(a);
      assert(itA!=Fluxes.end());
      const double sigmaA = itA->second;
      const auto itB = Fluxes.find(b);
      assert(itB!=Fluxes.end());
      const double sigmaB = itB->second;
      
      // Integrate
      for(int q=0; q<nQuad; ++q)
	{
	  const double lambda = Qd.Bulk->GetQuadraturePoint(q)[0];
	  
	  // Flux value here
	  double sigma = lambda*sigmaA + (1.-lambda)*sigmaB;

	  // Update L2 error
	  L2Err += len*Qd.Bulk->GetQuadratureWeights(q)*std::abs((sigma-Fex)*(sigma-Fex));
	}
    }
  return sqrt(L2Err);
}
			  
