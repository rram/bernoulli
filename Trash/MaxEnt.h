// Sriramajayam

#ifndef MAX_ENT_HPP
#define MAX_ENT_HPP

#include <vector>

//! Class to evaluate functions and derivatives of max-ent functions
//! \todo Introduce cut-off for evaluation. Will need to use boost rtrees
class MaxEnt
{
 public:
  //! Constructor
  //! \param[in] spd Spatial dimension
  //! \param[in] gmma Value of gamma to use. Recommended is 0.8
  //! \param[in] pts Nodes defining the interpolation over the convex hull
  //! \param[in] epsFunc Tolerance to use for function evaluation
  //! \param[in] epsRes Tolerance to use for Newton iterations
  MaxEnt(const int spd, const double gmma,
	 const std::vector<double>& pts,
	 const double epsFunc, const double epsRes);
  
  //! Destructor
  virtual ~MaxEnt();

  //! Copy constructor
  //! \param Obj object to be copied
  MaxEnt(const MaxEnt& Obj);

  //! Main functionality
  //! Computes the values of shape functions at a piont
  //! \param[in] X Cartesian coordinates of point at which to evaluate functions
  //! \param[out] val Computed shape function
  void Evaluate(const double* X, double* val);

  //! Returns the intrinsic parameter beta associated with this node
  //! \param[in] a Shape function number
  double GetBeta(const int a) const;

  //! Returns the number of shape functions
  int GetNumFunctions() const;

  //! Returns the number of nodes
  int GetNumNodes() const;
    
  //! Returns the tolerance used for shape function values
  double GetFunctionTolerance() const;

  //! Returns the tolrance used for Newton-iterations
  double GetResidualTolerance() const;

  //! Returns the spatial dimension
  int GetSpatialDimension() const;

  //! returns the node set
  const std::vector<double>& GetNodeSet() const;
  
 private:

  // Helper struct
  struct SolStruct
  {
    SolStruct(const int SPD);
    double Z;
    std::vector<double> Res, Jac, InvJac;
    std::vector<double> lambda;
    const double* X;
  };
   
  //! Computes the function Z(x,lambda)
  double GetZ(const double* X, const double* lambda) const;
  
  //! Computes the residual
  //! \param[in,out] data Data for this evaluation.
  //! Residue and jacobian are updated
  void Residual(SolStruct& data);
  
  // Members
  const int SPD; //!< Spatial dimension
  const double gamma; //!< Value of gamma used for the interpolation
  std::vector<double> NodeSet; //!< Set of nodes defining the interpolation
  const int nNodes; //!< Number of nodes
  const double Tol0; //!< Tolerance value for shape functions
  const double TolNR; //!< Tolerance for residual in Newton-iterations
  std::vector<double> beta; //!< Value of beta for each node
  SolStruct SolData; //!< Helper struct
};


#endif
