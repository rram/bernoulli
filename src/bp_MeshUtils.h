// Sriramajayam

#ifndef BP_MESH_UTILS_H
#define BP_MESH_UTILS_H

#include <mx_MaxEntSSD.h>
#include <geom_ImplicitMaxEntManifold.h>
#include <msh_TriangleModule>
#include <msh_TetrahedronModule>

namespace bp
{
  // Struct with options for fitting
  struct SSD_Options
  {
    double Tol0; //!< Tolerance for basis function cut off
    double TolNR; //!< Tolerance of NR iterations
    int nnb; //!< Number of neighbors for cutoff radius
    double gamma; //!< Smoothness of the max-ent function
    double alpha; //!< Hessian multiplying factor
    std::vector<double>* nodeset; //!< Max-ent node set
    std::vector<double>* coordinates; //!< Coordinates of the mesh to use for hessian calcs
    std::vector<int>* connectivity; //!< Connectivity of the mesh to use for hessian calcs
    int nMaxThreads; //!< Number of threads to use
    bool verbose; //!< Print stuff or not
  };

  // Struct with options for meshing
  struct Meshing_Options
  {
    int nMaxThreads; //!< Number of threads to use
    int MaxVertValency; //!< Estimated max number of vertices in a 1-ring
    int MaxElmValency; //!< Estimated max number of elements in a 1-ring
    int nSteps; //!< Number of projection steps
    int nIters; //!< Number of relaxation iterations
  };
  
  // Interface to compute the max-ent fit for a given oriented point cloud
  template<int SPD>
    void GetSmoothedSignedDistance(const std::vector<double>& points,
				   const std::vector<double>& normals,
				   const SSD_Options& options,
				   mx::MaxEntSSD<SPD>*& Fit);
  

  // Interface to compute a mesh conforming to a given implicit manifold
  //! Connectivities are numbered from 1
  template<int SPD>
    void GetConformingMesh(msh::StdOrphanMesh& BG,
			   geom::ImplicitManifold& Geom,
			   const Meshing_Options& options,
			   std::vector<double>& coordinates,
			   std::vector<int>& connectivity,
			   std::vector<int>& posverts,
			   std::vector<int>& poselmsnodes);

}

#endif
