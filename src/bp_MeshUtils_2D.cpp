// Sriramajayam

#include <bp_MeshUtils.h>
#include <P12DElement.h>
#include <type_traits>
#include <um_TriangleModule>
#include <algorithm>
#include <omp.h>

namespace bp
{
  // Interface to compute the max-ent fit for a given oriented point cloud
  template<>
  void GetSmoothedSignedDistance<2>(const std::vector<double>& points,
				    const std::vector<double>& normals,
				    const SSD_Options& options,
				    mx::MaxEntSSD<2>*& Fit)
  {
    // Create elements to integrate the Hessian
    Triangle<2>::SetGlobalCoordinatesArray(*(options.coordinates));
    const int nElements = static_cast<int>(options.connectivity->size()/3);
    std::vector<Element*> ElmArray(nElements);
    for(int e=0; e<nElements; ++e)
      {
	const int* conn = &(*options.connectivity)[3*e];
	ElmArray[e] = new P12DElement<1>(conn[0]+1, conn[1]+1, conn[2]+1);
      }

    // Function fitting parameters
    mx::MaxEntSSDParams params;
    params.Tol0 = options.Tol0;
    params.TolNR = options.TolNR;
    params.nnb = options.nnb;
    params.gamma = options.gamma;
    params.alpha = options.alpha;
    params.nodeset = options.nodeset;
    params.ElmArray = &ElmArray;
    params.PC = &points;
    params.normals = &normals;
    params.nMaxThreads = options.nMaxThreads;
    params.verbose = options.verbose;

    // Fit function
    Fit = new mx::MaxEntSSD<2>(params);

    // Clean up
    for(auto& e:ElmArray) delete e;
    return;
  }



  // Anonymous namespace for creating an Orphan mesh from a working mesh
  namespace
  {
    void Cast2OrphanMesh(const um::WorkingMesh<msh::StdOrphanMesh>& WM, // Working mesh
			 const std::vector<int>& PosVerts, // List of positive vertices in WM
			 const std::list<std::pair<int, int>>& PosElmFacePairs, // List of positive elements and their positive faces
			 msh::StdTriMesh MD, // Mesh to be returned
			 std::vector<int>& NewPosVerts, // Renumbered list of positive vertices
			 std::vector<int>& NewPosElmFacePairs // Renumbered list of positive elements and local nodes on their positive faces
			 )
    {
      // Check that the mesh MD is empty
      assert((MD.GetNumNodes()==0 && MD.GetNumElements()==0) &&
	     "bp::Cast2OrphanMesh: expected an empty mesh");
      const auto& RefMesh = WM.GetReferenceMesh();
      
      // Get the list of all nodes in this mesh
      std::vector<int> wmnodes;
      WM.GetNodes(wmnodes);
      const int nVerts = static_cast<int>(wmnodes.size());
      
      // Insert coordinates into MD
      std::unordered_map<int, int> Old2NewNodeNumMap({});
      for(int i=0; i<nVerts; ++i)
	{
	  MD.Insert(i, WM.coordinates(wmnodes[i]));
	  Old2NewNodeNumMap.insert({wmnodes[i],i});
	}

      // Insert renumbered connectivities into MD
      const int NPE = RefMesh.GetNumNodesPerElement();
      std::vector<int> newconn(NPE);
      int ecount = 0;
      std::map<int, int> Old2NewElmNumMap({});
      const auto& ElmList = WM.GetElements();
      for(auto& elm:ElmList)
	{
	  Old2NewElmNumMap[elm] = ecount;
	  
	  // Connectivity of this element in WM
	  const auto* conn = WM.connectivity(elm);

	  // Renumbered connectivity
	  for(int a=0; a<NPE; ++a)
	    {
	      const auto& it = Old2NewNodeNumMap.find(conn[a]);
	      assert(it!=Old2NewNodeNumMap.end());
	      newconn[a] = it->second;
	    }

	  // Insert this element into MD
	  MD.Insert(ecount++, &newconn[0]);
	}

      // Renumber the list of positive vertices
      NewPosVerts.clear();
      for(auto& v:PosVerts)
	  {
	    auto it=Old2NewNodeNumMap.find(v);
	    assert(it!=Old2NewNodeNumMap.end() && "bp::Cast2OrphanMesh- Could not locate positive vertex");
	    NewPosVerts.push_back(it->second);
	  }

      // Renumber the list of positive elements and their faces
      NewPosElmFacePairs.clear();
      for(auto& ef:PosElmFacePairs)
	{
	  const int& elm = ef.first;
	  const int& face = ef.second;

	  // New element number
	  auto it = Old2NewElmNumMap.find(elm);
	  assert(it!=Old2NewElmNumMap.end() && "bp::Cast2OrphanMesh- Could not locate element");
	  NewPosElmFacePairs.push_back(it->second);

	  // Local node numbers of this face
	  const auto* locnodes = WM.GetLocalFaceNodes(face);
	  NewPosElmFacePairs.push_back( locnodes[0] );
	  NewPosElmFacePairs.push_back( locnodes[1] );
	}

      return;
    }
  }
  
  // Interface to compute a mesh conforming to a given implicit manifold
  template<>
  void GetConformingMesh<2>(msh::StdOrphanMesh& BG,
			    geom::ImplicitManifold& Geom,
			    const Meshing_Options& options,
			    std::vector<double>& coordinates,
			    std::vector<int>& connectivity,
			    std::vector<int>& posverts,
			    std::vector<int>& poselmsnodes)
  {
    // Set the number of thread
    omp_set_num_threads(options.nMaxThreads);
    
    // Create a workspace for the background mesh
    um::SDWorkspace<msh::StdOrphanMesh, geom::ImplicitManifold>  BGws(BG, Geom);
    
    // Evaluate the implicit function at all node. Signed distances are not required
    const int nbgnodes = BG.GetNumNodes();
#pragma omp parallel default(shared)
    {
      um::SignedDistance sd;
      std::map<int, um::SignedDistance> sdvals({});
#pragma omp for
      for(int i=0; i<nbgnodes; ++i)
	{
	  const double* X = BG.coordinates(i);
	  Geom.GetImplicitFunction(X, sd.value);
	  if(sd.value>0.) sd.sign = um::SDSignature::Plus;
	  else sd.sign = um::SDSignature::Minus;
	  sdvals[i] = sd;
	}

      // Merge one thread at a time
#pragma omp critical
      {
	for(auto& it:sdvals)
	  BGws.Set(it.first, it.second);
      }
    }


    // Create domain triangulator
    um::DomainTriangulator<msh::StdOrphanMesh, geom::ImplicitManifold> Mshr(BGws);
    
    // Get the working mesh
    um::WorkingMesh<msh::StdOrphanMesh>& WM = Mshr.GetWorkingMesh();

    // Positive vertices
    const auto& PosVerts = Mshr.GetPositiveVertices();

    // Boundary vertices in the background mesh
    std::set<int> bg_bdnodes({});
    const int nbgelements = BG.GetNumElements();
    for(int e=0; e<nbgelements; ++e)
      {
	const int* myconn = BG.connectivity(e);
	const int* elmnbs = BG.GetElementNeighbors(e);
	for(int f=0; f<3; ++f)
	  if(elmnbs[2*f]<0)
	    {
	      // This is a free face
	      const int* locfacenodes = BG.GetLocalFaceNodes(f);
	      for(int i=0; i<2; ++i)
		bg_bdnodes.insert(myconn[locfacenodes[i]]);
	    }
      }

    // Union of boundary nodes and positive vertices
    std::vector<int> fixednodes({});
    std::set_union(PosVerts.begin(), PosVerts.end(), bg_bdnodes.begin(), bg_bdnodes.end(),
		   std::back_inserter(fixednodes));
    
    // Interior nodes to relax
    std::vector<int> wmnodes;
    WM.GetNodes(wmnodes);
    std::sort(wmnodes.begin(), wmnodes.end());
    std::vector<int> Ir({});
    std::set_difference(wmnodes.begin(), wmnodes.end(), fixednodes.begin(), fixednodes.end(),
			std::back_inserter(Ir));

    // 1-ring data for the working mesh
    dvr::ValencyHint vhint ({.MaxVertValency=options.MaxVertValency,
	  .MaxElmValency=options.MaxElmValency});
    dvr::OneRingData<um::WorkingMesh<msh::StdOrphanMesh>> RD(WM, wmnodes, vhint);

    // Quality metric
    dvr::GeomTri2DQuality<um::WorkingMesh<msh::StdOrphanMesh>> Quality(WM);

    // Max-min solver for relaxing vertices
    dvr::SolverSpecs solspecs({.nThreads=1, .nExtrema=2, .nIntersections=3});
    dvr::ReconstructiveMaxMinSolver<um::WorkingMesh<msh::StdOrphanMesh>> mmsolver(WM, RD, solspecs);

    // Vertex optimizer
    dvr::SeqMeshOptimizer Opt;

    // Relaxation direction generator
    dvr::RelaxationDirGenerator rdir_gen;
    rdir_gen.f_rdir = dvr::CartesianDirections<2>;

    // Project and relax in steps
    for(int step=1; step<=options.nSteps; ++step)
      {
	// Project boundary vertices in parallel
	omp_set_num_threads(options.nMaxThreads);
	const double alpha = static_cast<double>(step)/static_cast<double>(options.nSteps);
	Mshr.Project(alpha);

	// Relax inner vertices sequentially
	omp_set_num_threads(1);
	for(int iter=0; iter<options.nIters; ++iter)
	  {
	    rdir_gen.params = &iter;
	    Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen);
	  }
      }
    
    // Positive element-face pairs
    const auto& PosElmFacePairs = Mshr.GetPositiveElmFacePairs(); 
    
    // mesh to be returned
    // Create an orphan StdTriMesh for this working mesh
    msh::SeqCoordinates Coord(2);
    msh::StdTriConnectivity Conn;
    msh::StdTriMesh MD(Coord, Conn);
    posverts.clear();
    poselmsnodes.clear();
    Cast2OrphanMesh(WM, PosVerts, PosElmFacePairs, MD, posverts, poselmsnodes);
    
    // Coordinates
    coordinates.clear();
    const int nNodes = Coord.GetNumNodes();
    coordinates.resize(2*nNodes);
    for(int n=0; n<nNodes; ++n)
      {
	const double* X = Coord.coordinates(n);
	for(int k=0; k<2; ++k)
	  coordinates[2*n+k] = X[k];
      }
    Coord.Clear();
    
    // Connectivity
    connectivity.clear();
    const int nElements = Conn.GetNumElements();
    connectivity.resize(3*nElements);
    for(int e=0; e<nElements; ++e)
      {
	const int* conn = Conn.connectivity(e);
	for(int a=0; a<3; ++a)
	  connectivity[3*e+a] = conn[a]+1;
      }
    Conn.Clear();	  

    return;
  }

}
