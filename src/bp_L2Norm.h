// Sriramajayam

#ifndef BP_MASS_MATRIX_H
#define BP_MASS_MATRIX_H

#include <ElementalOperation.h>
#include <bp_LaplacianUtils.h>

namespace bp
{
  //! Class for computing the L2 energy norm and its linearizations
  class L2Norm: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element
    //! \param[in] field Field number to use
    inline L2Norm(const Element* elm,
		      const ScalarMapAccess sma,
		      const int field=0)
      :DResidue(), Elm(elm), SMAccess(sma), Fields({field}) {}

    //! Destructor
    inline virtual ~L2Norm() {}

    //! Copy constructor
    //! \param[in] Obj Object to be copied
    inline L2Norm(const L2Norm& Obj)
      :DResidue(Obj), Elm(Obj.Elm), SMAccess(Obj.SMAccess), Fields(Obj.Fields) {}
      
    //! Cloning
    inline virtual L2Norm* Clone() const override
    { return new L2Norm(*this); }

    //! Returns the fields used
    inline virtual const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a field
    inline virtual int GetFieldDof(const int field) const override
    { return Elm->GetDof(Fields[field]); }

    //! Return the element
    inline const Element* GetElement() const
    { return Elm; }

    //! Compute the energy
    //! \param[in] state Configuration at which to compute the energy
    double GetEnergy(const void* state) const;
    
    //! Compute the element force vector
    //! \param[in] config State at which to compute diagonal mass.
    //! \param[out] funcval Element force vector
    inline virtual void GetVal(const void* state,
			       std::vector<std::vector<double>>* funcval) const override
    { GetDVal(state, funcval, nullptr); }

    //! Compute the element mass matrix
    //! \param[in] config State at which to compute diagonal mass. 
    //! \param[out] funcval Computed element force vector
    //! \param[out] dfuncval Computed consistent mass matrix
    virtual void GetDVal(const void* state,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test. Checks linearization of the L2 norm
    //! \param[in] state Configuration at which to perform consistency tests
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to check 
    virtual bool ConsistencyTest(const void* state,
				 const double pertEPS, const double tolEPS) const override;

  private:
    const Element* Elm; //!< Pointer to element object
    const ScalarMapAccess SMAccess; //!< How to access data for this operation
    const std::vector<int> Fields;
  };
}

#endif
