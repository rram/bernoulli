// Sriramajayam

namespace bp
{
// Struct with options for fitting
  struct SSD_Options
  {
    double Tol0; //!< Tolerance for basis function cut off
    double TolNR; //!< Tolerance of NR iterations
    int nnb; //!< Number of neighbors for cutoff radius
    double gamma; //!< Smoothness of the max-ent function
    double alpha; //!< Hessian multiplying factor
    int nMaxThreads; //!< Number of threads to use
    bool verbose; //!< Print stuff or not
   
  };

  // Interface to compute the max-ent fit for a given oriented point cloud
  template<int SPD>
    void GetSmoothedSignedDistance(const std::vector<double>& points,
				   const std::vector<double>& normals,
				   const msh::StdOrphanMesh& CC,
				   const SSD_Options& options,
				   geom::MaxEntSSD<SPD>*& Fit)
    {
      
    }

}
