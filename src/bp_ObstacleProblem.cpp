// Sriramajayam

#include <bp_ObstacleProblem.h>
#include <MeshUtils.h>

namespace bp
{
  // Constructor
  ObstacleProblem::ObstacleProblem(msh::StdTriMesh& bg,
				   std::vector<double>& bdpts,
				   std::vector<double>& bdnormals,
				   const SSD_Options& opt1,
				   const Meshing_Options& opt2,
				   const geom::NLSolverParams& opt3,
				   const ForceFieldValue<2>* frc)
    :BernoulliProblem2D(bg, bdpts, bdnormals, opt1, opt2, opt3, frc),
     PMap(nullptr),
     PFluxMap({}) {}

  // Destructor
  ObstacleProblem::~ObstacleProblem() {}

  // Helper method to setup a run
  void ObstacleProblem::SetupIteration()
  {
    // Setup everything other than PMap
    BernoulliProblem2D::SetupIteration();
    
    // Create the pressure map & initialize to 0
    const int nNodes = GetConformingMesh().nodes;
    PMap = new ScalarMap(nNodes);
    for(int n=0; n<nNodes; ++n)
      { double val=0.; PMap->Set(n, &val); }
    PMap->SetInitialized();

    // -- done --
    return;
  }

  // Helper method to setup a run
  void ObstacleProblem::SetupIteration(const std::vector<double>& points,
				       const std::vector<double>& normals)
  {
    // Setup everything other than PMap
    BernoulliProblem2D::SetupIteration(points, normals);

        // Create the pressure map & initialize to 0
    const int nNodes = GetConformingMesh().nodes;
    PMap = new ScalarMap(nNodes);
    for(int n=0; n<nNodes; ++n)
      { double val=0.; PMap->Set(n, &val); }
    PMap->SetInitialized();

    // -- done --
    return;
  }

  // Helper method to finalize a run
  void ObstacleProblem::FinalizeIteration()
  { BernoulliProblem2D::FinalizeIteration();
    delete PMap; }
  
  
  // Main functionality: compute the state. Overloaded
  void ObstacleProblem::ComputeState(const BCParams& bc)
  {
    // Compute the u-field
    BernoulliProblem2D::ComputeState(bc);
    
    // Compute the pressure field without external forcings
    // -----------------------------------------------------
    
    // Set dirichlet bcs
    const int ndirichlet = static_cast<int>(bc.dirichlet_dofs.size());
    assert(ndirichlet==static_cast<int>(bc.dirichlet_values.size()) &&
	   "bp::ObstacleProblem::ComputeState- Unexpected number of dirichlet bcs");
    for(int i=0; i<ndirichlet; ++i)
      PMap->Set(bc.dirichlet_dofs[i], &bc.dirichlet_values[i]);
    PMap->SetInitialized();
    std::vector<double> zero(ndirichlet);
    std::fill(zero.begin(), zero.end(), 0.);

    // Access the assembler and PETSc data structures
    auto& Asm = GetLaplacianAssembler();
    auto& PData = GetPetscData();
    
    // Assemble
    Asm.Assemble(PMap, PData.resVEC, PData.stiffnessMAT);

    // Set Dirichlet BCs
    PData.SetDirichletBCs(bc.dirichlet_dofs, zero);

    // Solve
    PData.Solve();

    // retrieve the solution
    PetscErrorCode ierr = VecScale(PData.solutionVEC, -1.); CHKERRV(ierr);
    double* sol;
    ierr = VecGetArray(PData.solutionVEC, &sol); CHKERRV(ierr);
    const int nTotalDofs = GetLocalToGlobalMap().GetTotalNumDof();
    for(int n=0; n<nTotalDofs; ++n)
      PMap->Increment(n, &sol[n]);
    PMap->SetInitialized();
    ierr = VecRestoreArray(PData.solutionVEC, &sol); CHKERRV(ierr);

    // -- done --
    return;
  }

  // Access the adjoint solution at the  current iteration
  const ScalarMap& ObstacleProblem::GetAdjointState() const
  { return *PMap; }

  // Compute the boundary fluxes at the previously computed state
  void ObstacleProblem::ComputeBoundaryFluxes()
  {
    // Compute fluxes for the u-solution
    BernoulliProblem2D::ComputeBoundaryFluxes();

    // Compute pressure fluxes
    // --------------------------
    auto& rVec = GetPetscData().resVEC;
    auto& Asm = GetLaplacianAssembler();

    // Assemble KU
    assert(PMap->IsInitialized() &&
	   "bp::ObstacleProblem::ComputeBoundaryFluxes- Pressure map not initialized");
    Asm.Assemble(PMap, rVec);
    auto ierr = VecAssemblyBegin(rVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(rVec); CHKERRV(ierr);

    // Compute nodal massess for all boundary nodes
    PFluxMap.clear();
    const auto& MyMD = GetConformingMesh();
    std::vector<std::vector<int>> EN;
    GetCoordConnFaceNeighborList(MyMD, EN);
    for(int e=0; e<MyMD.elements; ++e)
      for(int f=0; f<3; ++f)
	if(EN[e][2*f]<0)
	  {
	    // This segment is on the boundary
	    const int n0 = MyMD.connectivity[3*e+f]-1;
	    const double* X = &MyMD.coordinates[2*n0];
	    const int n1 = MyMD.connectivity[3*e+(f+1)%3]-1;
	    const double* Y = &MyMD.coordinates[2*n1];
	    const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	    const double mass = 0.5*lenXY;

	    // Update mass for node 0
	    auto it0 = PFluxMap.find(n0);
	    if(it0==PFluxMap.end()) { PFluxMap.insert(std::make_pair(n0, mass)); }
	    else { it0->second += mass; }

	    // Update mass for node 1
	    auto it1 = PFluxMap.find(n1);
	    if(it1==PFluxMap.end()) { PFluxMap.insert(std::make_pair(n1, mass)); }
	    else { it1->second += mass; }
	  }

    // recover fluxes
    for(auto& it:PFluxMap)
      { const int n = it.first;
	const double mass = it.second;
	double res;
	ierr = VecGetValues(rVec, 1, &n, &res); CHKERRV(ierr);
	it.second = res/mass;
      }

    // -- done --
    return;
  }


  // Update the boundary location
  void ObstacleProblem::UpdateBoundary
  (std::function<double(const double* X, const double& dudn, void*)> f_perturb, void* params)
  { assert(false && "bp::ObstacleProblem::UpdateBoundary- Incorrect call"); }
  
  
  void ObstacleProblem::UpdateBoundary
  (std::function<double(const double* X, const double* normal, const double& dudn,
			const double& dpdn, void*)> f_perturb, void* params)
  {
    // Access the set of +ve vertices, elm+node pairings
    const auto& MyPosVerts = GetPositiveVertices();
    const auto& MyMD = GetConformingMesh();
    
    // Boundary fluxes for the u-field
    const auto& UFluxMap = GetBoundaryFluxes();

    // Compute boundary perturbations
    std::map<int, double> pertMap({});
    const auto& BdGeom = GetBoundary();
    double sdval;
    double normal[2];
    for(auto& n:MyPosVerts)
      {
	// Coordinates of this point and normal to the boundary here
	const double* X = &MyMD.coordinates[2*n];
	BdGeom.GetSignedDistance(X, sdval, normal);
	
	// U-flux
	auto it_u = UFluxMap.find(n);
	assert(it_u!=UFluxMap.end() &&
	       "bp::ObstacleProblem::UpdateBoundary- u-flux not computed at +ve vertex");
	const double& dudn = it_u->second;

	// P-flux
	auto it_p = PFluxMap.find(n);
	assert(it_p!=PFluxMap.end() &&
	       "bp::ObstacleProblem::UpdateBoundary- p-flux not computed at +ve vertex");
	const double& dpdn = it_p->second;
	
	// Compute boundary perturbation
	pertMap[n] = f_perturb(X, normal, dudn, dpdn, params);
      }

    // Update the boundary points and normals
    UpdateBoundary_(pertMap);

    // -- done --
    is_boundary_updated = true;
  }

  // Access the pressure flux
  const std::map<int, double>& ObstacleProblem::GetAdjointStateFluxes() const
  { return PFluxMap; }
    
}
