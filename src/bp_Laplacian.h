// Sriramajayam

#ifndef BP_LAPLACIAN_H
#define BP_LAPLACIAN_H

#include <ElementalOperation.h>
#include <bp_LaplacianUtils.h>

namespace bp
{
  //! Class for computing the element stiffness matrix and force vector
  //! for the laplace operator
  class Laplacian: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element
    //! \param[in] field Field numer to use for the scalar field
    inline Laplacian(const Element* elm, const ScalarMapAccess sma, const int field=0)
      :DResidue(), Elm(elm), SMAccess(sma), Fields({field}) {}

    //! Destructor
    inline virtual ~Laplacian() {}

    //! Copy constructor
    //! \param[in] Obj Object to be copied
    inline Laplacian(const Laplacian& Obj)
      :DResidue(Obj), Elm(Obj.Elm), SMAccess(Obj.SMAccess), Fields(Obj.Fields) {}

    //! Cloning
    inline virtual Laplacian* Clone() const override
    { return new Laplacian(*this); }

    //! Return the fields used
    inline virtual const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a field
    inline virtual int GetFieldDof(const int field) const override
    { return Elm->GetDof(Fields[field]); }
  
    //! Return the element
    inline const Element* GetElement() const
    { return Elm; }

    //! Compute the energy
    //! \param config State at which to compute energy
    double GetEnergy(const void* state) const;
  
    //! Compute the element force vector
    //! \param config State at which to compute residual
    //! \param funcval Output. Computed elemental force vector
    inline virtual void GetVal(const void* state, 
			       std::vector<std::vector<double>>* funcval) const override
    { GetDVal(state, funcval, nullptr); }

    //! Compute the element stiffness matrix and force vector
    //! \param config State at which to compute residual/dresidual
    //! \param funcval Output. Computed elemental force vector
    //! \param dfuncval Output. Computed elemental stiffness matrix
    virtual void GetDVal(const void* state, 
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const;

    //! Consistency test
    //! \param[in] state State at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbation
    //! \param[in] tolEPS Tolerance to use for consistency check
    virtual bool ConsistencyTest(const void* state,
				 const double pertEPS,
				 const double tolEPS) const override; 
  private:
    const Element* Elm; //!< Pointer to element object
    const ScalarMapAccess SMAccess; //!< How to access data for this operation
    const std::vector<int> Fields;
  };
}


#endif
