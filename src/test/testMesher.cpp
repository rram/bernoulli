// Sriramajayam

#include <bp_MeshUtils.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <geom_ImplicitMaxEntManifold.h>

// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coordinates, std::vector<int>& connectivity);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  const int nThreads = 1;
  omp_set_num_threads(nThreads);

  // Read mesh. Its vertices will be the max-ent nodes
  CoordConn CC;
  CC.nodes_element = 3;
  CC.spatial_dimension = 2;
  ReadTecplotFile((char*)"square.msh", CC);
  SubdivideTriangles(CC.connectivity, CC.coordinates, CC.nodes, CC.elements);
  std::vector<double> nodeset = CC.coordinates;

  // Mesh for integration
  for(int i=0; i<2; ++i)
    SubdivideTriangles(CC.connectivity, CC.coordinates, CC.nodes, CC.elements);
    
  // Points for fitting & normals
  std::vector<double> points({}), normals({});
  double rad = 0.25;
  const int nPoints = 50;
  for(int i=0; i<nPoints; ++i)
    {
      double theta = static_cast<double>(i)*2.*M_PI/static_cast<double>(nPoints);
      points.push_back( rad*std::cos(theta) );
      points.push_back( rad*std::sin(theta) );
      normals.push_back( std::cos(theta) );
      normals.push_back( std::sin(theta) );
    }
  
  // Fitting options
  bp::SSD_Options ssd_options({
      .Tol0=1.e-6,
	.TolNR=1.e-6,
	.nnb=1,
	.gamma=0.8,
	.alpha=0.01,
	.nodeset=&nodeset,
	.coordinates=&CC.coordinates,
	.connectivity=&CC.connectivity,
	.nMaxThreads=1,
	.verbose=true});
  
  // Compute smoothed signed distance
  mx::MaxEntSSD<2>* Fit;
  bp::GetSmoothedSignedDistance<2>(points, normals, ssd_options, Fit);
  
  // Plot SSD computed
  for(int n=0; n<CC.nodes; ++n)
    for(int k=0; k<2; ++k)
      CC.coordinates[2*n+k] *= 0.95;
  Fit->PlotTec((char*)"sol.tec", CC);

  // Create an implicit manifold
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::ImplicitMaxEntManifold<mx::MaxEnt2D> Geom(Fit->GetMaxEntFunctions(),
						  Fit->GetDofs(),
						  nlparams, nThreads);
  
  // Create a background mesh
  std::vector<int> bgconn;
  std::vector<double> bgcoord;
  GetEquilateralMesh(bgcoord, bgconn);
  msh::SeqCoordinates BGCoord(2, bgcoord);
  msh::StdTriConnectivity BGConn(bgconn);
  msh::StdTriMesh BG(BGCoord, BGConn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Meshing options
  bp::Meshing_Options mesh_options({
      .nMaxThreads=1, 
	.MaxVertValency=6,
	.MaxElmValency=6,
	.nSteps=4,
	.nIters=10});

  // Create a conforming mesh
  std::vector<int> conf_conn;
  std::vector<double> conf_coord;
  std::vector<int> PosVerts, PosElmsNodes;
  bp::GetConformingMesh<2>(BG, Geom, mesh_options, conf_coord, conf_conn, PosVerts, PosElmsNodes);
  for(auto& i:conf_conn) --i;
  msh::SeqCoordinates MDcoord(2, conf_coord);
  msh::StdTriConnectivity MDconn(conf_conn);
  msh::StdTriMesh MD(MDcoord, MDconn);
  msh::PlotTecStdTriMesh("MD.tec", MD);

  // Consistency check for PosVerts and PosElmsNodes
  std::set<int> PosVertsSet1(PosVerts.begin(), PosVerts.end());
  std::set<int> PosVertsSet2({});
  const int nPosElms = static_cast<int>(PosElmsNodes.size()/3);
  for(int i=0; i<nPosElms; ++i)
    {
      const int e = PosElmsNodes[3*i];
      const int* conn = MD.connectivity(e);
      const int a0 = PosElmsNodes[3*i+1];
      const int a1 = PosElmsNodes[3*i+2];
      PosVertsSet2.insert( conn[a0] );
      PosVertsSet2.insert( conn[a1] );
    }
  assert(PosVertsSet1==PosVertsSet2);
}


// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coord,
			std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( 0.4*std::cos(theta) );
      coord.push_back( 0.4*std::sin(theta) );
    }

  // Subdivide
  for(int i=0; i<5; i++)
    msh::SubdivideTriangles(2, conn, coord);

  return;
}
