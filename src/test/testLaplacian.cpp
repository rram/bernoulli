// Sriramajayam

#include <bp_Laplacian.h>
#include <P12DElement.h>
#include <random>
#include <cassert>

using namespace bp;

int main()
{
  // Create one triangle
  std::vector<double> coordinates({1.,0.,0.,1.,0.,0.});
  Triangle<2>::SetGlobalCoordinatesArray(coordinates);
  P12DElement<1> Elm({1,2,3});

  // Random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(-1.,1.);

  // State a random state
  const int nNodes = 3;
  ScalarMap state(nNodes);
  double val;
  for(int a=0; a<3; ++a)
    {
      val = dist(gen);
      state.Set(a, &val);
    }
  state.SetInitialized();

  // Access to scalar map
  ScalarMapAccess SMA(std::vector<int>({0,1,2}));

  // Tolerances
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;
  
  // Test class
  Laplacian Op(&Elm, SMA, 0);
  assert(Op.GetField().size()==1 && "Unexpected number of fields");
  assert(Op.GetField()[0]==0 && "Unexpected field number");
  assert(Op.GetFieldDof(0)==3 && "Unexpected number of dofs");
  assert(Op.GetElement()==&Elm && "Unexpected element returned");
  assert(Op.ConsistencyTest(&state, pertEPS, tolEPS) && "Failed consistency test");
  
  // Copy
  Laplacian Copy(Op);
  assert(Copy.GetField()==Op.GetField() && "Unexpected fields in copy");
  assert(Copy.GetFieldDof(0)==Op.GetFieldDof(0) && "Unexpected number of dofs in copy");
  assert(Copy.GetElement()==Op.GetElement() && "Unexpected element returned in copy");
  assert(Copy.ConsistencyTest(&state, pertEPS, tolEPS) && "Failed consistency test in copy");

  // Cloning
  auto* Clone = Copy.Clone();
  assert(Clone->GetField()==Op.GetField() && "Unexpected fields in clone.");
  assert(Clone->GetFieldDof(0)==Op.GetFieldDof(0) && "Unexpected number of dofs in clone.");
  assert(Clone->GetElement()==Op.GetElement() && "Unexpected element returned in clone.");
  assert(Clone->ConsistencyTest(&state, pertEPS, tolEPS) && "Failed consistency test in clone.");
  
  delete Clone;
}

