// Sriramajayam

#include <mx_MaxEntSSD.h>
#include <msh_TriangleModule>
#include <dvr_TriangleModule>
#include <um_TriangleModule>
#include <geom_ImplicitMaxEntManifold.h>
#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <random>

// Aliases
using MaxEntCurveFit = mx::MaxEntSSD<2>;
using MaxEntCurve = geom::ImplicitMaxEntManifold<mx::MaxEnt2D>;

// Namespace omitted intentionally

// Computes a max-ent curve fit for a circle
void GetSmoothedSignedDistance(const CoordConn& MD, const int nThreads,
			       MaxEntCurveFit*& Fit);

// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coordinates, std::vector<int>& connectivity);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  const int nThreads = 1;//omp_get_max_threads()/2;
  omp_set_num_threads(nThreads);
  
  // Read mesh. Its vertices will be the max-ent nodes
  CoordConn CC;
  CC.nodes_element = 3;
  CC.spatial_dimension = 2;
  ReadTecplotFile((char*)"square.msh", CC);
  for(int i=0; i<1; ++i)
    SubdivideTriangles(CC.connectivity, CC.coordinates, CC.nodes, CC.elements);

  MaxEntCurveFit* Fit = nullptr;
  GetSmoothedSignedDistance(CC, nThreads, Fit);
  assert(Fit!=nullptr);
  
  // Sequential calcs from here on
  omp_set_num_threads(1);
  
  // create implicit curve
  geom::NLSolverParams nlparams({1.e-6, 1.e-6, 25}); // ftol, ttol, max_iter
  MaxEntCurve Geom(Fit->GetMaxEntFunctions(),
		   Fit->GetDofs(),
		   nlparams, 1);

  // Create a background mesh of equilateral triangles
  GetEquilateralMesh(CC.coordinates, CC.connectivity); // Connectivites numbered from 0
  CC.nodes = static_cast<int>(CC.coordinates.size()/2);
  CC.elements = static_cast<int>(CC.connectivity.size()/3);
  msh::SeqCoordinates Coord(2, CC.coordinates);
  msh::StdTriConnectivity Conn(CC.connectivity);
  msh::StdTriMesh BG(Coord, Conn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Workspace for background mesh
  um::SDWorkspace<decltype(BG), decltype(Geom)> BGws(BG, Geom);

  // Evaluate the implicit function at all nodes. The signed distance is not required
  const int nbgnodes = BG.GetNumNodes();
  for(int i=0; i<nbgnodes; ++i)
    {
      const auto* X = BG.coordinates(i);
      um::SignedDistance sd;
      Geom.GetImplicitFunction(X, sd.value);
      sd.sign = (sd.value>0.) ? um::SDSignature::Plus : um::SDSignature::Minus;
      BGws.Set(i, sd);
    }
  
  // Create domain triangulator
  um::DomainTriangulator<decltype(BG), decltype(Geom)> Mshr(BGws);

  // Get the working mesh
  auto& WM = Mshr.GetWorkingMesh();
  um::PlotTecWorkingMesh("wm.tec", WM, true);

  // Positive vertices
  const auto& PosVerts = Mshr.GetPositiveVertices();

  // Interior nodes of the working mesh (to relax)
  std::vector<int> wmnodes;
  WM.GetNodes(wmnodes);
  std::sort(wmnodes.begin(), wmnodes.end());
  std::vector<int> Ir({});
  std::set_difference(wmnodes.begin(), wmnodes.end(), PosVerts.begin(), PosVerts.end(),
		      std::back_inserter(Ir));

  // 1-ring data for the working mesh
  dvr::ValencyHint vhint({6,6});
  dvr::OneRingData<decltype(WM)> RD(WM, wmnodes, vhint);

  // Quality metric for the working mesh
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);

  // Max-min solver for relaxing vertices
  dvr::SolverSpecs solspecs({1,2,3}); // nThreads, nextrema, nintersections
  dvr::ReconstructiveMaxMinSolver<decltype(WM)> mmsolver(WM, RD, solspecs);

  // Vertex optimizer
  dvr::SeqMeshOptimizer Opt;

  // Relaxation direction generator
  dvr::RelaxationDirGenerator rdir_gen;
  rdir_gen.f_rdir = dvr::CartesianDirections<2>;

  // Project and relax in steps
  const int nSteps = 5;
  const int nIters = 8;
  for(int step=1; step<nSteps; ++step)
    {
      // Project boundary vertices
      const double alpha = static_cast<double>(step)/static_cast<double>(nSteps);
      Mshr.Project(alpha);

      // Relax interior verts
      for(int iter=0; iter<nIters; ++iter)
	{
	  rdir_gen.params = &iter;
	  Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen);
	}
    }
  um::PlotTecWorkingMesh("wm-1.tec", WM, true);

  // Not relaxing boundary nodes at this time. Need to limit perturbation magnitude.
  // relax boundary + interior vertices
  um::MeshGeomPair<decltype(WM), decltype(Geom)> MGPair(WM, Geom);
  dvr::RelaxationDirGenerator tgtdirgen;
  tgtdirgen.f_rdir = um::TangentDirection2D<decltype(MGPair)>;
  tgtdirgen.params = &MGPair;
  dvr::ProjPartitionedMaxMinSolver<decltype(WM)> bdmmsolver(WM, RD, solspecs);
  um::BoundaryProjector<decltype(Geom)> bdproj(Geom);
  for(int iter=0; iter<nIters; ++iter)
    {
      // Boundary relaxation+projection
      //Mshr.Relax(PosVerts, Opt, Quality, bdmmsolver, tgtdirgen, &bdproj);
      // Relax interior verts
      rdir_gen.params = &iter;
      Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen);
    }
  um::PlotTecWorkingMesh("wm-2.tec", WM, true);

  
  // Clean up
  delete Fit;
  PetscFinalize();
}


// Computes a max-ent curve fit for a circle
void GetSmoothedSignedDistance(const CoordConn& CC, const int nThreads, MaxEntCurveFit*& Fit)
{
  // Nodes for max-ent functions
  std::vector<double> NodeSet(CC.coordinates);

  // Subdivide mesh for integration purposes
  CoordConn MD = CC;
  for(int i=0; i<2; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  
  // Create elements
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
    }
  
  // Points for fitting & normals
  std::vector<double> PC({}), normals({});
  double rad = 0.25;
  const int nPoints = 50;
  for(int i=0; i<nPoints; ++i)
    {
      double theta = static_cast<double>(i)*2.*M_PI/static_cast<double>(nPoints);
      PC.push_back( rad*std::cos(theta) );
      PC.push_back( rad*std::sin(theta) );
      normals.push_back( std::cos(theta) );
      normals.push_back( std::sin(theta) );
    }
  
  // Curve fitting parameters
  mx::MaxEntSSDParams params;
  params.Tol0 = 1.e-6;
  params.TolNR = 1.e-6;
  params.nnb = 1;
  params.gamma = 0.8;
  params.alpha = 0.01;
  params.nodeset = &NodeSet;
  params.ElmArray = &ElmArray;
  params.PC = &PC;
  params.normals = &normals;
  params.nMaxThreads = nThreads;
  params.verbose = true;

  // Fit curve
  Fit = new MaxEntCurveFit(params);
  for(int n=0; n<MD.nodes; ++n)
    for(int k=0; k<2; ++k)
      MD.coordinates[2*n+k] *= 0.95;
  Fit->PlotTec((char*)"sol.tec", MD);

  for(auto& it:ElmArray) delete it;
  return;
}


// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coord,
			std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( 0.4*std::cos(theta) );
      coord.push_back( 0.4*std::sin(theta) );
    }

  // Subdivide
  for(int i=0; i<5; i++)
    msh::SubdivideTriangles(2, conn, coord);

  return;
}
