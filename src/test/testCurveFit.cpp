// Sriramajayam

#include <geom_MaxEnt.h>
#include <bp_Laplacian2DModule>
#include <bp_MaxEntCurveFit.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <bp_MaxEntCurve.h>
#include <random>

using namespace bp;
using namespace geom;

std::vector<double>
GetNumericalGradSD(const Codim1Manifold &curve,
		   const double* pt, const double EPS); 

// Test class object
void Test(const ImplicitMaxEntCurve& curve);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  const int nThreads = omp_get_max_threads()/2;
  omp_set_num_threads(nThreads);
  
  // Read mesh. Its vertices will be the max-ent nodes
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"square.msh", MD);
  for(int i=0; i<1; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  
  // Nodes for max-ent functions
  std::vector<double> NodeSet(MD.coordinates);

  // Subdivide mesh for integration purposes
  for(int i=0; i<2; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Create elements
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
    }
  
  // Points for fitting & normals
  std::vector<double> PC({}), normals({});
  double rad = 0.25;
  const int nPoints = 50;
  for(int i=0; i<nPoints; ++i)
    {
      double theta = static_cast<double>(i)*2.*M_PI/static_cast<double>(nPoints);
      PC.push_back( rad*std::cos(theta) );
      PC.push_back( rad*std::sin(theta) );
      normals.push_back( std::cos(theta) );
      normals.push_back( std::sin(theta) );
    }
  
  // Curve fitting parameters
  MaxEntCurveFitParams params;
  params.Tol0 = 1.e-8;
  params.TolNR = 1.e-8;
  params.nnb = 1;
  params.gamma = 0.8;
  params.alpha = 0.01;
  params.nodeset = &NodeSet;
  params.ElmArray = &ElmArray;
  params.PC = &PC;
  params.normals = &normals;
  params.nMaxThreads = nThreads;
  params.verbose = true;

  // Fit curve
  MaxEntCurveFit Fit(params);
  for(int n=0; n<MD.nodes; ++n)
    for(int k=0; k<2; ++k)
      MD.coordinates[2*n+k] *= 0.95;
  Fit.PlotTec((char*)"sol.tec", MD);

  // create implicit curve
  NLSolverParams nlparams({1.e-8, 1.e-8, 25}); // ftol, ttol, max_iter
  ImplicitMaxEntCurve curve(nlparams, Fit, 1); // 1 thread

  // Test this object
  Test(curve);
  
  for(auto& it:ElmArray) delete it;
  PetscFinalize();
}

  
// test class
void Test(const ImplicitMaxEntCurve& curve)
{
  assert(curve.GetManifoldName()=="Implicit-max-ent-curve");
  assert(curve.GetParametricDimension()==1);
  assert(curve.GetEmbeddingDimension()==2);
  assert(curve.GetCodimension()==1);
  
  // Generate a random point near the zero-level set
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(-0.1,0.1); // small perturbation
  std::uniform_real_distribution<> thetadis(0., std::atan(1.)*8.); // Random angle
  double theta = thetadis(gen);
  double X[2] = {0.25*std::cos(theta)+dis(gen), 0.25*std::sin(theta)+dis(gen)};
  double sd = 0.;
  double dSD[2];
  curve.GetSignedDistance(X, sd, dSD);

  // Tolerances for consistency tests
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-1;
  
  // test consistency of 1st derivatives
  std::vector<double> dSDnum = GetNumericalGradSD(curve, X, pertEPS);
  for(int i=0; i<2; ++i)
    assert(std::abs(dSDnum[i]-dSD[i])<tolEPS);
  
  // test consistency of closest point projection
  double cpt[2];
  curve.GetClosestPoint(X, cpt);
  double zero;
  curve.GetSignedDistance(cpt, zero);
  assert(std::abs(zero)<tolEPS);
  
  // Check that (x-cpt).n = phi
  double normal[2];
  curve.GetNormal(cpt, normal);
  double dot = (X[0]-cpt[0])*normal[0] + (X[1]-cpt[1])*normal[1];
  assert(std::abs(dot-sd)<tolEPS);
}

std::vector<double>
GetNumericalGradSD(const Codim1Manifold &curve, const double* pt, const double h)
{
  const int spd = curve.GetEmbeddingDimension();
  std::vector<double> mypt(spd,0.);
  std::vector<double> gradSD(spd,0.);
  double sd1=0, sd2=0;

  for(int i=0; i<spd; ++i)
    {
      for(int j=0; j<spd; ++j)
	mypt[j] = pt[j];

      mypt[i] = pt[i]+h;
      curve.GetSignedDistance(&mypt[0], sd1);

      mypt[i] = pt[i]-h;
      curve.GetSignedDistance(&mypt[0], sd2);

      gradSD[i] = (sd1-sd2)/(2.*h);
    }  
  return gradSD;
}

