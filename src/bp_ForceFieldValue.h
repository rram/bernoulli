// Sriramajayam

#ifndef BP_FORCE_FIELD_VALUE_H
#define BP_FORCE_FIELD_VALUE_H

#include <ForceField.h>
#include <functional>

namespace bp
{
  using ForceFieldFunction = std::function<bool(const std::vector<double>,
						const std::vector<double>,
						std::vector<double>*,
						std::vector<double>*,
						void*)>;
  
  //! Class for defining a force field in which the force evaluation
  //! is specified through a given function.
  //! Templated by the spatial dimension
  template<int SPD>
    class ForceFieldValue: public DForceField
    {
    public:
      //! Constructor
      //! \param[in] func Function to evaluate force field and derivatives
      inline ForceFieldValue(ForceFieldFunction func, void* usr=nullptr)
	:FrcFunc(func), params(usr) {}

      //! Destructor
      inline virtual ~ForceFieldValue() {}

      //! Copy constructor
      //! \param[in] obj Object to be copied from
      inline ForceFieldValue(const ForceFieldValue<SPD>& obj)
	:FrcFunc(obj.FrcFunc), params(obj.params) {}

      //! Cloning
      inline ForceFieldValue<SPD>* Clone() const override
      { return new ForceFieldValue<SPD>(*this); }

      //! Returns the expected length of the vector u
      inline virtual unsigned int GetULength() const override
      { return 1; }

      //! Returns the expected length of the vector X
      inline virtual unsigned int GetXLength() const override
      { return SPD; }

      //! Returns the expected length of the Force vector (output)
      inline virtual unsigned int GetForceLength() const override
      { return 1; }
  
      //! Returns the name of the force field
      inline virtual const std::string GetForceFieldName() const override
      { return "ForceFieldValue"; }

      //! Computes the value of the force field.
      //! Returns true if successful.
      //! @param u  vector of input values
      //! @param X  vector of input position values
      //! @param ForceFieldVal pointer to the vector with output values for forces.
      //! ForceFieldVal[i] contains the i-th component of the force. If the array does not 
      //! have enough space for all the components, it is resized.
      inline virtual bool GetForceField(const std::vector<double>  u, 
					const std::vector<double>  X,
					std::vector<double> *ForceFieldVal) const override
      { return GetDForceField(u, X, ForceFieldVal, nullptr); }

      //! Computes the value of the force field
      //! Returns true if successful.
      //! @param u  vector of input values
      //! @param X  vector of input position values
      //! @param ForceFieldVal pointer to the vector with output values for forces.
      //! ForceFieldVal[i] contains the i-th component of the force. If the array does not 
      //! have enough space for all the components, it is resized.
      //! @param DForceFieldVal pointer to the vector with output values for force derivatives.
      //! DForceFieldVal[i*GetULength()+a] contains the derivative of ForceFieldVal[i]
      //!  with respect to u[a]. 
      //! If the vector does not have the appropriate size, it is resized.
      //! If argument not provided (=0), the derivative is not computed.
      inline virtual bool GetDForceField(const std::vector<double> u, 
					 const std::vector<double> X,
					 std::vector<double> *ForceFieldVal,
					 std::vector<double> *DForceFieldVal) const override
      { return FrcFunc(u, X, ForceFieldVal, DForceFieldVal, params); }

    private:
      const ForceFieldFunction FrcFunc;
      void* params;
    };
    
}

#endif
