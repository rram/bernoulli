// Sriramajayam

#include <bp_L2Norm.h>
#include <cassert>
#include <cmath>

using namespace bp;


// Compute the energy at a given state
double L2Norm::GetEnergy(const void* arg) const
{
  // Get this state
  const ScalarMap* state = static_cast<const ScalarMap*>(arg);
  assert(state!=nullptr && "bp::L2Norm::GetEnergy- Invalid pointer to state");
  assert(state->IsInitialized() && "bp::L2Norm::GetEnergy- State uninitialized");

  // Local node numbers to access scalar field
  const auto& LocNodes = SMAccess.Get();

  // Fields, dofs, shape functions
  const int field = Fields[0];
  const int nDof = GetFieldDof(0);
  const auto& Shapes = Elm->GetShape(field);

  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Integrate
  double Energy = 0.;
  double uval;
  for(int q=0; q<nQuad; ++q)
    {
      // Shape function values at this quadrature point
      const auto* qShp = &Shapes[nDof*q];

      // Function value here
      uval = 0.;
      for(int a=0; a<nDof; ++a)
	uval += state->Get(LocNodes[a])[0]*qShp[a];
      
      // u^2/2
      Energy += 0.5*Qwts[q]*uval*uval;
    }
  return Energy;
}
      


// Compute the element force vector and stiffness matrix
void L2Norm::GetDVal(const void* arg,
		     std::vector<std::vector<double>>* funcval,
		     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  // Get the state
  const ScalarMap* state = static_cast<const ScalarMap*>(arg);
  assert(state!=nullptr && "bp::L2Norm::GetDVal- Invalid pointer to state");
  assert(state->IsInitialized() && "bp::L2Norm::GetDVal- State uninitialized");
    
  // Local node numbers to access scalar field
  const auto& LocNodes = SMAccess.Get();

  // Zero the outputs
  SetZero(funcval, dfuncval);

  // Fields, ndofs
  const int field = Fields[0];
  const int nDof = GetFieldDof(field);

  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Shape functions
  const auto& Shapes = Elm->GetShape(field);

  // Integrate
  double uval;
  for(int q=0; q<nQuad; ++q)
    {
      // Shape function values at this quadrature point
      const auto* qShp = &Shapes[nDof*q];

      // Function value
      uval = 0.;
      for(int a=0; a<nDof; ++a)
	uval += state->Get(LocNodes[a])[0]*qShp[a];

      // Force vector
      if(funcval!=nullptr)
	for(int a=0; a<nDof; ++a)
	  (*funcval)[0][a] += Qwts[q]*uval*qShp[a];

      // Mass matrix
      if(dfuncval!=nullptr)
	for(int a=0; a<nDof; ++a)
	  for(int b=0; b<nDof; ++b)
	    (*dfuncval)[0][a][0][b] += Qwts[q]*qShp[a]*qShp[b];
    }
  return;
}


// Consistency test
bool L2Norm::ConsistencyTest(const void* arg,
			     const double pertEPS, const double tolEPS) const
{
  const ScalarMap* ptr = static_cast<const ScalarMap*>(arg);
  assert(ptr!=nullptr && "bp::L2Norm::ConsistencyTest: Invalud pointer to state");
  const ScalarMap& state = *ptr;
  assert(state.IsInitialized() && "bp::L2Norm::ConsistencyTest: Dofs not initialized");

  // Local node numbers to access scalar map
  const auto& LocNodes = SMAccess.Get();

  // Fields and dofs
  const int field = Fields[0];
  const int nDof = GetFieldDof(field);

  // Size arrays
  std::vector<std::vector<double>> res(1), resnum(1), resplus(1), resminus(1);
  res[0].resize(nDof);
  resnum[0].resize(nDof);
  resplus[0].resize(nDof);
  resminus[0].resize(nDof);

  std::vector<std::vector<std::vector<std::vector<double>>>> dres(1), dresnum(1);
  dres[0].resize(nDof);
  dresnum[0].resize(nDof);
  for(int a=0; a<nDof; ++a)
    {
      dres[0][a].resize(1);
      dres[0][a][0].resize(nDof);
      dresnum[0][a].resize(1);
      dresnum[0][a][0].resize(nDof);
    }

  // Correct residual and stiffness
  GetDVal(arg, &res, &dres);

  // Check consistency of the residual
  const double mpertEPS = -pertEPS;
  for(int a=0; a<nDof; ++a)
    {
      // Positive and negative perturbations
      ScalarMap pstate(state);
      pstate.Increment(LocNodes[a], &pertEPS);
      pstate.SetInitialized();
      ScalarMap mstate(state);
      mstate.Increment(LocNodes[a], &mpertEPS);
      mstate.SetInitialized();

      // Compute the perturbed energies
      double Eplus = GetEnergy(&pstate);
      double Eminus = GetEnergy(&mstate);
      double nres = (Eplus-Eminus)/(2.*pertEPS);
      assert(std::abs(nres-res[0][a])<tolEPS &&
	     "bp::L2Norm::Consistency of residuals failed");

      // Compute perturbed residuals
      GetVal(&pstate, &resplus);
      GetVal(&mstate, &resminus);

      // Check consistency of stiffness
      for(int b=0; b<nDof; ++b)
	{
	  double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	  assert(std::abs(ndres-dres[0][a][0][b])<tolEPS &&
		 "bp::L2Norm::Consistency of deresiduals failed");
	}
    }
  return true;
}

