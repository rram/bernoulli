// Sriramajayam

#ifndef BP_P12D_SEGMENT_H
#define BP_P12D_SEGMENT_H

#include <P12DElement.h>
#include <ShapesEvaluated.h>

namespace bp
{
  //! Linear segments in 2D
  template<int nFields>
    class P12DSegment: public Element
    {
    public:
      //! Constructor
      //! \param[in] i1, i2 Global numbering for nodes
      inline P12DSegment(int i1, int i2)
	:Element()
	{
	  SegGeom = new Segment<2>(i1, i2);
	  const Quadrature* qrule = Line_1::Bulk;
	  Linear<1> LinShp;
	  ShapesEvaluated SE(qrule, &LinShp, SegGeom);
	  AddBasisFunctions(SE);
	  for(int i=0; i<nFields; ++i)
	    AppendField(i);
	}

      //! Copy constructor
      inline P12DSegment(const P12DSegment<nFields>& Obj)
	:Element(Obj)
      { SegGeom = Obj.SegGeom->Clone(); }

      //! Destuctor
      inline ~P12DSegment()
      { delete SegGeom; }

      //! Cloning
      inline virtual P12DSegment<nFields>* Clone() const override
      { return new P12DSegment<nFields>(*this); }

      //! Returns the element geometry
      const Segment<2>& GetElementGeometry() const
      { return *SegGeom; }

    protected:
      int GetFieldIndex(int fields) const override
      { return 0; }

    private:
      Segment<2>* SegGeom;
    };

  //! Local to global map
  typedef StandardP12DMap StandardP12DSegmentMap;
  
}

#endif
