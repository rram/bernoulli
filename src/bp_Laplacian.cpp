// Sriramajayam

#include <bp_Laplacian.h>
#include <cstdlib>
#include <cassert>
#include <cmath>

using namespace bp;

// Compute the energy at a given state
double Laplacian::GetEnergy(const void* arg) const
{
  // Get this state
  const ScalarMap* state = static_cast<const ScalarMap*>(arg);
  assert(state!=nullptr && "Laplacian::GetEnergy- Invalid pointer to state");
  assert(state->IsInitialized() && "Laplacian::GetEnergy- State uninitialized");

  // Local node numbers to access scalar field
  const auto& LocNodes = SMAccess.Get();
  
  // Fields, ndofs, shape function derivatives
  const int field = Fields[0];
  const int nDof = GetFieldDof(0);
  const int nDeriv = Elm->GetNumDerivatives(field);
  const auto& DShapes = Elm->GetDShape(field);
  
  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Integrate
  double Energy = 0.;
  double val = 0.;
  double dot2 = 0.;
  for(int q=0; q<nQuad; ++q)
    {
      // Derivatives of shape functions at this quadrature point
      const auto* qDShapes = &DShapes[q*nDof*nDeriv];

      // gradU.gradU
      dot2 = 0.;
      for(int i=0; i<nDeriv; ++i)
	{
	  val = 0.;
	  for(int a=0; a<nDof; ++a)
	    val += state->Get(LocNodes[a])[0]*qDShapes[a*nDeriv+i];
	  dot2 += val*val;
	}

      // Update energy
      Energy += 0.5*Qwts[q]*dot2;
    }
  return Energy;
}
 


// Compute the element stiffness matrix and force vector
void Laplacian::
GetDVal(const void* arg,
	std::vector<std::vector<double>>* funcval,
	std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  // Get the state
  const ScalarMap* state = static_cast<const ScalarMap*>(arg);
  assert(state!=nullptr && "Laplacian::GetDVal- Invalid pointer to state.");
  assert(state->IsInitialized() && "Laplacian::GetDVal- State uninitialized");

  // Local node numbers to access scalar field
  const auto& LocNodes = SMAccess.Get();
  
  // Zero the outputs
  SetZero(funcval, dfuncval);
  
  // Fields, ndofs
  const int field = Fields[0];
  const int nDof = GetFieldDof(field);

  // Spatial dimension
  const int spdim = Elm->GetElementGeometry().GetEmbeddingDimension();

  // Quadrature
  const auto& Qwts = Elm->GetIntegrationWeights(field);
  const int nQuad = static_cast<int>(Qwts.size());

  // Derivatives of shape functions
  const auto nDeriv = Elm->GetNumDerivatives(field);
  assert(nDeriv==spdim && "Laplacian::GetDVal- Expected spdim=nDeriv");
  const auto& DShapes = Elm->GetDShape(field);
  
  double dF[spdim];
  double dot = 0.;
  for(int q=0; q<nQuad; ++q)
    {
      // Derivatives of shape functions at this quadrature point
      const auto* qDShapes = &DShapes[q*nDof*nDeriv];
      
      // Compute the gradient of function here
      for(int J=0; J<spdim; ++J)
	{
	  dF[J] = 0.;
	  for(int a=0; a<nDof; ++a)
	    dF[J] += state->Get(LocNodes[a])[0]*qDShapes[a*nDeriv+J];
	}

      // Update the force vector
      if(funcval!=nullptr)
	for(int a=0; a<nDof; ++a)
	  {
	    // This variation: delta F = N_a
	    // Compute dF:dN_a
	    dot = 0.;
	    for(int i=0; i<spdim; ++i)
	      dot += dF[i]*qDShapes[a*nDeriv+i];

	    // Update force vector
	    (*funcval)[0][a] += Qwts[q]*dot;
	  }

      // Update stiffness matrix
      if(dfuncval!=nullptr)
	for(auto a=0; a<nDof; ++a)
	  // This variation: Na
	  for(auto b=0; b<nDof; ++b)
	    // This variation Nb
	    {
	      // Compute dNa:dNb
	      dot = 0.;
	      for(auto i=0; i<spdim; ++i)
		dot += qDShapes[a*nDeriv+i]*qDShapes[b*nDeriv+i];

	      // Update stiffness
	      (*dfuncval)[0][a][0][b] += Qwts[q]*dot;
	    }
    }
  return;
}


// Consistency test
bool Laplacian::ConsistencyTest(const void* arg, 
				const double pertEPS,
				const double tolEPS) const
{
  assert(arg!=nullptr && "Laplacian::ConsistencyTest- invalid pointer to state");
  const ScalarMap* ptr = static_cast<const ScalarMap*>(arg);
  assert(ptr!=nullptr && "Laplacian::ConsistencyTest: Invalid pointer to state");
  const ScalarMap& state = *ptr;
  assert(state.IsInitialized() && "Laplacian::ConsistencyTest: Dofs not initialized");

  // Local node numbers to access scalar map
  const auto& LocNodes = SMAccess.Get();
  
  // Fields and their dofs
  const int field = Fields[0];
  const int nDof = GetFieldDof(field);
  
  // Size arrays
  std::vector<std::vector<double>> res(1), resnum(1), resplus(1), resminus(1);
  res[0].resize(nDof);
  resnum[0].resize(nDof);
  resplus[0].resize(nDof);
  resminus[0].resize(nDof);
  
  std::vector<std::vector<std::vector<std::vector<double>>>> dres(1), dresnum(1);
  dres[0].resize(nDof);
  dresnum[0].resize(nDof);
  for(int a=0; a<nDof; ++a)
    {
      dres[0][a].resize(1);
      dres[0][a][0].resize(nDof);
      dresnum[0][a].resize(1);
      dresnum[0][a][0].resize(nDof);
    }
  
  // Correct residual and stiffness
  GetDVal(arg, &res, &dres);

  // Check consistency of residual
  const double mpertEPS = -pertEPS;
  for(int a=0; a<nDof; ++a)
    {
      // Positive & negative perturbations
      ScalarMap pstate(state);
      pstate.Increment(LocNodes[a], &pertEPS);
      pstate.SetInitialized();
      ScalarMap mstate(state);
      mstate.Increment(LocNodes[a], &mpertEPS);
      mstate.SetInitialized();
      
      // Compute the perturbed energies
      double Eplus = GetEnergy(&pstate);
      double Eminus = GetEnergy(&mstate);
      double nres = (Eplus-Eminus)/(2.*pertEPS);
      assert(std::abs(nres-res[0][a])<tolEPS &&
	     "bp::Laplacian::Consistency of residuals failed");
      
      // Compute perturbed residuals
      GetVal(&pstate, &resplus);
      GetVal(&mstate, &resminus);

      // Check consistency of stiffness
      for(int b=0; b<nDof; ++b)
	{
	  double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	  assert(std::abs(ndres-dres[0][a][0][b])<tolEPS &&
		 "bp::Laplacian::Consistency of dresiduals failed");
	}
    }
  return true;
}
