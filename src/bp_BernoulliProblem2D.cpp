// Sriramajayam

#include <bp_BernoulliProblem2D.h>
#include <P12DElement.h>
#include <MeshUtils.h>
#include <cassert>
#include <iostream>

namespace bp
{
    // Constructor
  BernoulliProblem2D::BernoulliProblem2D(msh::StdTriMesh& bg,
					 std::vector<double>& bdpts,
					 std::vector<double>& bdnormals,
					 const SSD_Options& opt1,
					 const Meshing_Options& opt2,
					 const geom::NLSolverParams& opt3,
					 const ForceFieldValue<2>* frc)
    :BG(bg),
     BdPoints(bdpts),
     BdNormals(bdnormals),
     ssd_options(opt1),
     mesh_options(opt2),
     geom_nlparams(opt3),
     Frc(frc),
     UMap(nullptr),
     BdFit(nullptr),
     Geom(nullptr)
  {
    // Check that PETSc has been initialized
    PetscBool flag;
    PetscErrorCode ierr = PetscInitialized(&flag);  CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "bp::BernoulliProblem2D- PETSc not initialized");

    // Flags
    is_state_iteration_setup = false;
    is_state_iteration_finalized = true;
    is_state_iteration_computed = false;
    is_boundary_updated = true;
    is_flux_computed = false;
  }
  
  // Destructor- default
  BernoulliProblem2D::~BernoulliProblem2D()
  {
    // Check that the state has been finalized
    assert(is_state_iteration_finalized==true &&
	   "bp::BernoulliProblem:Destructor state not finalized");
  }


  // Helper function to setup the geometry and mesh
  void BernoulliProblem2D::SetupBoundaryAndMesh()
  { 
    GetSmoothedSignedDistance(BdPoints, BdNormals, ssd_options, BdFit);
    
    // Create an implicit geometry
    Geom = new geom::ImplicitMaxEntManifold<mx::MaxEnt2D>(BdFit->GetMaxEntFunctions(),
							  BdFit->GetDofs(),
							  geom_nlparams,
							  1); // Number of threads

    // recover a conforming mesh (usenamespace to distinguish from member function)
    bp::GetConformingMesh<2>(BG, *Geom, mesh_options,
    			     MD.coordinates, MD.connectivity, PosVerts, PosElmsNodes);
    MD.nodes_element = 3;
    MD.spatial_dimension = 2;
    MD.nodes = static_cast<int>(MD.coordinates.size()/2);
    MD.elements = static_cast<int>(MD.connectivity.size()/3);

    // -- done --
    return;
  }
    
    
  // Helper function to setup elements and operations
  void BernoulliProblem2D::SetupElementsAndOperations()
  {
    // Create elements and operations
    Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
    Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
    ElmArray.resize(MD.elements);
    LapArray.resize(MD.elements);
    const std::vector<int> Field({0});
    for(int e=0; e<MD.elements; ++e)
      {
	const int* conn = &MD.connectivity[3*e];
	ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
	ScalarMapAccess sma(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
	LapArray[e] = new Laplacian(ElmArray[e], sma, 0);
      }

    // Local to global map
    L2GMap = new StandardP12DMap(ElmArray);

    // Create state
    UMap = new ScalarMap(MD.nodes);
    for(int n=0; n<MD.nodes; ++n)
      { double val = 0.; UMap->Set(n, &val); }
    UMap->SetInitialized();

    // Create assemblers
    Lap_Asm = new StandardAssembler<Laplacian>(LapArray, *L2GMap);
    

    // Forcing related data structures
    if(Frc!=nullptr)
      {
	FrcArray.resize(MD.elements);
	for(int e=0; e<MD.elements; ++e)
	  {
	    const int* conn = &MD.connectivity[3*e];
	    ScalarMapAccess sma(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
	    FrcArray[e] = new BodyForceWork(ElmArray[e], *Frc, Field, sma);
	    Frc_Asm = new StandardAssembler<BodyForceWork>(FrcArray, *L2GMap);
	  }
      }

    // Setup sparse data structures
    std::vector<int> nnz;
    Lap_Asm->CountNonzeros(nnz);
    PD.Initialize(nnz);

    // Set LU Solver
    PC pc;
    PetscErrorCode ierr = KSPGetPC(PD.kspSOLVER, &pc); CHKERRV(ierr);
    ierr = PCSetType(pc, PCLU); CHKERRV(ierr);

    // -- done --
    return;
  }
  
  // Helper method to setup a run
  // Set up boundary, elements, operations, assemblers, sparse data structures
  void BernoulliProblem2D::SetupIteration()
  {
     // Check that the boundary has been updated
    assert(is_boundary_updated==true && "bp::BernoulliProblem2D::SetupIteration- Boundary not updated");

    // Check that this iteration is not being set twice
    assert(is_state_iteration_setup==false &&
	   "bp::BernoulliProblem2D::SetupIteration- Duplicate setup call");
    assert(is_state_iteration_finalized==true &&
	   "bp::BernoulliProblem2D::SetupIteration- Finalize iterations before recomputing");

    // Check that a current state is not active
    assert(is_state_iteration_computed==false &&
	   "bp::BernoulliProblem2D::SetupIteration- Existing state not cleared");

    // Compute an implicit function fitting the boundary
    assert(BdFit==nullptr && "bp::BernoulliProblem2D::SetupIteration- Boundary pointer not null");

    // Setup the boundary geometry and the mesh
    SetupBoundaryAndMesh();

    // Setup elements and operations
    SetupElementsAndOperations();
    
    // Set flags
    is_state_iteration_setup = true;
    is_state_iteration_finalized = false;
    is_boundary_updated = false;
    is_flux_computed = false;
  }


  // Helper method to setup a run
  void BernoulliProblem2D::SetupIteration(const std::vector<double>& points,
					  const std::vector<double>& normals)
  { assert(points.size()==normals.size() &&
	   "bp::BernoulliProblem2D::SetupIteration- Unexpected number of points");
    BdPoints = points;
    BdNormals = normals;
    SetupIteration(); }
    

  // Helper method to finalize a run
  // Destroy the boundary, elements, operations, assemblers, sparse data structures
  void BernoulliProblem2D::FinalizeIteration()
  {
    assert(is_state_iteration_finalized==false &&
	   "bp::BernoulliProblem2D::FinalizeIteration- Duplicate finalize call");

    delete BdFit; BdFit=nullptr;
    delete Geom; Geom=nullptr;
    MD.coordinates.clear();
    MD.connectivity.clear();
    PosVerts.clear();
    PosElmsNodes.clear();
    for(auto& x:ElmArray) delete x; ElmArray.clear();
    for(auto& x:LapArray) delete x; LapArray.clear(); 
    delete L2GMap; L2GMap=nullptr;
    delete UMap; UMap=nullptr;
    delete Lap_Asm; Lap_Asm=nullptr;
    if(Frc!=nullptr)
      {
	for(auto& x:FrcArray) delete x;
	FrcArray.clear();
	delete Frc_Asm;
      }
    Frc_Asm=nullptr;
    PD.Destroy();
    BdFluxMap.clear();

    // Set flags
    is_state_iteration_setup = false;
    is_state_iteration_finalized = true;
    is_state_iteration_computed = false;
    is_flux_computed = false;
  }


  // Compute the state at the current boundary guess and with given Dirichlet bcs
  void BernoulliProblem2D::ComputeState(const BCParams& bc)
  {
    // Check that thois uteration has been setup
    assert(is_state_iteration_setup && "bp::BernoulliProblem2D::ComputeState- Call SetupIteration.");

    // Dirichlet bcs
    const auto& boundary = bc.dirichlet_dofs;
    const auto& bvalues = bc.dirichlet_values;
    
    // Set dirichet bcs in the state
    const int nbd_dofs = static_cast<int>(boundary.size());
    assert(static_cast<int>(bvalues.size())==nbd_dofs &&
	   "bp::BernoulliProblem2D::ComputeState- Unexpected length of boundary conditions");
    for(int i=0; i<nbd_dofs; ++i)
      UMap->Set(boundary[i], &bvalues[i]);
    UMap->SetInitialized();
    std::vector<double> zero(nbd_dofs);
    std::fill(zero.begin(), zero.end(), 0.);

    // Assemble
    Lap_Asm->Assemble(UMap, PD.resVEC, PD.stiffnessMAT);
    if(Frc!=nullptr)
      Frc_Asm->Assemble(UMap, PD.resVEC, false); // Don't zero. Only residual.

    // Set boundary conditions
    PD.SetDirichletBCs(boundary, zero);
    
    // Solve
    PD.Solve();

    // Update the solution
    PetscErrorCode ierr = VecScale(PD.solutionVEC, -1.); CHKERRV(ierr);
    double* sol;
    ierr = VecGetArray(PD.solutionVEC, &sol); CHKERRV(ierr);
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    for(int n=0; n<nTotalDofs; ++n)
      UMap->Increment(n, &sol[n]);
    UMap->SetInitialized();
    ierr = VecRestoreArray(PD.solutionVEC, &sol); CHKERRV(ierr);

    // --- done --- //
    
    // Set flags
    is_state_iteration_computed = true;
    is_boundary_updated = false;
    is_flux_computed = false;
  }

  // Compute the boundary fluxes at the previously computed state
  void BernoulliProblem2D::ComputeBoundaryFluxes()
  {
    // Check flags
    assert((is_state_iteration_setup==true &&
	    is_state_iteration_computed==true &&
	    is_state_iteration_finalized==false) &&
	   "bp::BernoulliProblem2D::ComputeBoundaryFluxes- Incorrect state");
    assert(UMap->IsInitialized() && "bp::BernoulliProblem2D:: State is not initialized");

    // Assemble KU-F
    auto& rVec = PD.resVEC;
    if(Frc!=nullptr)
      { Frc_Asm->Assemble(UMap, rVec);
	Lap_Asm->Assemble(UMap, rVec, false); }// Don't zero 
    else
      Lap_Asm->Assemble(UMap, rVec);
	
    PetscErrorCode ierr;
    ierr = VecAssemblyBegin(rVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(rVec); CHKERRV(ierr);

    // Compute nodal masses for boundary nodes
    BdFluxMap.clear();
    std::vector<std::vector<int>> EN;
    GetCoordConnFaceNeighborList(MD, EN);
    for(int e=0; e<MD.elements; ++e)
      for(int f=0; f<3; ++f)
	if(EN[e][2*f]<0)
	  {
	    // This segment is on the boundary
	    const int n0 = MD.connectivity[3*e+f]-1;
	    const double* X = &MD.coordinates[2*n0];
	    const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	    const double* Y = &MD.coordinates[2*n1];
	    const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	    const double mass = 0.5*lenXY;

	    // Update mass for node 0
	    auto it0 = BdFluxMap.find(n0);
	    if(it0==BdFluxMap.end()) { BdFluxMap.insert(std::make_pair(n0, mass)); }
	    else { it0->second += mass; }

	    // Update mass for node 1
	    auto it1 = BdFluxMap.find(n1);
	    if(it1==BdFluxMap.end()) { BdFluxMap.insert(std::make_pair(n1, mass)); }
	    else { it1->second += mass; }
	  }

    // Recover fluxes
    for(auto& it:BdFluxMap)
      { const int n = it.first;
	const double mass = it.second;
	double res;
	ierr = VecGetValues(rVec, 1, &n, &res); CHKERRV(ierr);
	it.second = res/mass;
      }

    // Update flag
    is_flux_computed = true;
  }


  // Access the SSD function
  const mx::MaxEntSSD<2>& BernoulliProblem2D::GetSSD() const
  { assert(is_state_iteration_setup &&
	   "bp::BernoulliProblem2D::GetSSD- iteration not initialized");
    return *BdFit; }
  
  // Access the conforming mesh
  const CoordConn& BernoulliProblem2D::GetConformingMesh() const
  { assert(is_state_iteration_setup &&
	   "bp::BernoulliProblem2D::GetConformingMesh- iteration not initialized");
    return MD; }

  // Access the boundary geometry
  const geom::ImplicitManifold& BernoulliProblem2D::GetBoundary() const
  {  assert(is_state_iteration_setup &&
	    "bp::BernoulliProblem2D::GetBoundary- iteration not initialized");
    return *Geom; }

  // Access the boundary fluxes
  const std::map<int, double>& BernoulliProblem2D::GetBoundaryFluxes() const
  { assert(is_flux_computed &&
	   "bp::BernoulliProblem2D::GetBoundaryFluxes- not computed yet");
    return BdFluxMap; }

  // Access the state at the current iteration
  const ScalarMap& BernoulliProblem2D::GetState() const
  { assert(is_state_iteration_computed &&
	   "bp::BernoulliProblem2D::GetState- state not computed");
    return *UMap; }

  // Access the positive vertices
  const std::vector<int>& BernoulliProblem2D::GetPositiveVertices() const
  { assert(is_state_iteration_setup &&
	   "bp::BernoulliProblem2D::GetPositiveVertices- state not computed");
    return PosVerts; }

  // Access positively cut elements and local nodes on the positive face
  const std::vector<int>& BernoulliProblem2D::GetPositiveElmFacePairs() const
  { assert(is_state_iteration_setup &&
	   "bp::BernoulliProblem2D::GetPositiveElmsFacePairs- state not computed");
    return PosElmsNodes; }
  
  
  // Access the boundary points
  const std::vector<double>& BernoulliProblem2D::GetBoundaryPoints() const
  { assert(is_boundary_updated &&
	   "bp::BernoulliProblem2D::GetBoundaryPoints- boundary not updated");
    return BdPoints; }
  
  // Access the boundary normals
  const std::vector<double>& BernoulliProblem2D::GetBoundaryNormals() const
  { assert(is_boundary_updated &&
	   "bp::BernoulliProblem2D::GetBoundaryPoints- boundary not updated");
    return BdNormals; }
  
  
  // Update the boundary location
  void BernoulliProblem2D::UpdateBoundary
  (std::function<double(const double* X, const double& dudn, void*)> f_perturb, void* params)
  {
    // Check flags
    assert((is_state_iteration_computed &&
	    !is_state_iteration_finalized &&
	    is_flux_computed &&
	    !is_boundary_updated) && "bp::BernoulliProblem2D::UpdateBoundary- Incorrect state");
    
    // Loop over boundary nodes & compute the perturbation distances
    std::map<int, double> pertMap({});
    for(auto& n:PosVerts)
      {
	double* X = &MD.coordinates[2*n];
	auto it = BdFluxMap.find(n);
	assert(it!=BdFluxMap.end() &&
	       "bp::BernoulliProblem2D::UpdateBoundary- flux not computed as pos vert");
	pertMap[n] = f_perturb(X, it->second, params);
      }

    // Update the boundary and normals based on the computed perturbation distances
    UpdateBoundary_(pertMap);

    // Update flags
    is_boundary_updated = true;
  }

  // Access to the assembler
  StandardAssembler<Laplacian>& BernoulliProblem2D::GetLaplacianAssembler() const
  { return *Lap_Asm; }

  // Access to PETSc data structures
  PetscData& BernoulliProblem2D::GetPetscData()
  { return PD; }
  
  // Access the local to global map
  const LocalToGlobalMap& BernoulliProblem2D::GetLocalToGlobalMap() const
  { return *L2GMap; }


  const std::vector<Element*>& BernoulliProblem2D::GetElementArray() const
  { return ElmArray; }
  
  // Helper function that updates the boundary given
  // the perturbations at the +ve vertices
  void BernoulliProblem2D::UpdateBoundary_(const std::map<int, double>& pertMap)
  {
    // Loop over boundary nodes
    double normal[2];
    double sdval;
    const int nPosVerts = static_cast<int>(PosVerts.size());
    BdPoints.clear();
    BdNormals.clear();
    BdPoints.resize(2*nPosVerts);
    BdNormals.resize(2*nPosVerts);
    int count = 0;
    std::map<int, int> PosVert2BdNodeMap; // Positive vertex -> index in BdPoints.
    for(auto& n:PosVerts)
      {
	const double* X = &MD.coordinates[2*n];
	
	// Normal to the boundary at this vertex
	Geom->GetSignedDistance(X, sdval, normal);

	// Perturbation distance for this node
	auto it = pertMap.find(n);
	assert(it!=pertMap.end() &&
	       "bp::BernoulliProblem2D::UpdateBoundary- Could not access perturbation distance");
	const double& pert_dist = it->second;
	
	// Perturb this boundary node
	for(int k=0; k<2; ++k)
	  BdPoints[2*count+k] = X[k] + pert_dist*normal[k];
	PosVert2BdNodeMap[n] = count++;
      }

    // Make the maps a0->a1 and a1->a0 for each positive edge
    std::map<int, int> fwd_edgeMap, rev_edgeMap;
    const int nPosElms = static_cast<int>(PosElmsNodes.size()/3);
    for(int i=0; i<nPosElms; ++i)
      {
	// Positive element number
	const int& e = PosElmsNodes[3*i];

	// Index of the first node of this positive edge
	const int& a0 = PosElmsNodes[3*i+1];
	const int& n0 = MD.connectivity[3*e+a0]-1;
	const auto it_0 = PosVert2BdNodeMap.find(n0);
	assert(it_0!=PosVert2BdNodeMap.end() &&
	       "bp::BernoulliProblem2D::UpdateBoundary- Could not find index of +ve vertex");
	const int& i0 = it_0->second;

	// Index of the second node of this positive edge
	const int& a1 = PosElmsNodes[3*i+2];
	const int& n1 = MD.connectivity[3*e+a1]-1;
	const auto it_1 = PosVert2BdNodeMap.find(n1);
	assert(it_1!=PosVert2BdNodeMap.end() &&
	       "bp::BernoulliProblem2D::UpdateBoundary- Could not find index of +ve vertex");
	const int& i1 = it_1->second;

	// Note down this edge connections
	fwd_edgeMap.insert( std::make_pair(i0, i1) );
	rev_edgeMap.insert( std::make_pair(i1, i0) );
      }
    
    // Compute the averaged normals at each positive vertex
    for(auto& it_curr:PosVert2BdNodeMap)
      {
	// Index of this vertex
	const int& i_curr = it_curr.second;
	
	// Two edges should be incident at this vertex. Get their indices
	const auto it_next = fwd_edgeMap.find(i_curr);
	const auto it_prev = rev_edgeMap.find(i_curr);
	assert((it_next!=fwd_edgeMap.end() && it_prev!=rev_edgeMap.end()) &&
	       "bp::BernoulliProblem2D::UpdateBoundary- Could not access positive vertex in chain- boundary not closed?");
	const int i_prev = it_prev->second;
	const int i_next = it_next->second;

	// Updated coordinates of the three nodes
	const double* Xprev = &BdPoints[2*i_prev];
	const double* Xcurr = &BdPoints[2*i_curr];
	const double* Xnext = &BdPoints[2*i_next];

	// Normals to the two edges
	double Len = std::sqrt((Xprev[0]-Xcurr[0])*(Xprev[0]-Xcurr[0]) +
			       (Xprev[1]-Xcurr[1])*(Xprev[1]-Xcurr[1]) );
	const double Nleft[] = {(Xcurr[1]-Xprev[1])/Len, -(Xcurr[0]-Xprev[0])/Len};

	Len = std::sqrt((Xnext[0]-Xcurr[0])*(Xnext[0]-Xcurr[0])+
			(Xnext[1]-Xcurr[1])*(Xnext[1]-Xcurr[1]));
	const double Nright[] = {(Xnext[1]-Xcurr[1])/Len, -(Xnext[0]-Xcurr[0])/Len};

	// Average the two normals
	const double avgN[] = { 0.5*(Nleft[0]+Nright[0]), 0.5*(Nleft[1]+Nright[1]) };
	Len = std::sqrt(avgN[0]*avgN[0] + avgN[1]*avgN[1]);
	BdNormals[2*i_curr+0] = avgN[0]/Len;
	BdNormals[2*i_curr+1] = avgN[1]/Len;
      }
    
    // -- done --
    return;
  }

  // Access to options
  const SSD_Options& BernoulliProblem2D::GetSSDOptions()
  { return ssd_options; }

  const Meshing_Options& BernoulliProblem2D::GetMeshingOptions()
  { return mesh_options; }

  const geom::NLSolverParams& BernoulliProblem2D::GetGeomSolverParams()
  { return geom_nlparams; }

  // Access to the background mesh
  msh::StdTriMesh& BernoulliProblem2D::GetBackgroundMesh()
  { return BG; }
  
}

