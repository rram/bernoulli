// Sriramajayam

#ifndef BP_BERNOULLI_PROBLEM_2D_H
#define BP_BERNOULLI_PROBLEM_2D_H

#include <bp_MeshUtils.h>
#include <bp_Laplacian.h>
#include <msh_TriangleModule>
#include <bp_ForceFieldValue.h>
#include <bp_LaplacianUtils.h>
#include <BodyForceWork.h>
#include <Assembler.h>
#include <functional>
#include <map>

namespace bp
{

  //! Helper struct for passing dirichlet/Neumann bcs.
  //! \todo Add neumann bcs
  struct BCParams
  {
    std::vector<int> dirichlet_dofs;
    std::vector<double> dirichlet_values;
  };

    
  class BernoulliProblem2D
  {
  public:
    //! Constructor
    //! \param[in] bg Background mesh to be used
    //! \param[in] bdpts Initial guess for outer boundary points. Copied
    //! \param[in] bdnormals Initial guess for outer boundary normals. Copied
    //! \param[in] frc Force field to impose. Set to null of no forcing is given
    //! \param[in] opt1 Options for boundary approximation with max-ent functions
    //! \param[in] opt2 Options for meshing
    //! \param[in] opt3 Nonlinear-closest-point-solver parameters for an implicit geometry
    //! \param[in] nodeset Node set for max-ent functions
    BernoulliProblem2D(msh::StdTriMesh& bg,
		       std::vector<double>& bdpts,
		       std::vector<double>& bdnormals,
		       const SSD_Options& opt1,
		       const Meshing_Options& opt2,
		       const geom::NLSolverParams& opt3,
		       const ForceFieldValue<2>* frc=nullptr);

    //! Destructor
    virtual ~BernoulliProblem2D();

    //! Disable copy and assignment constructor
    BernoulliProblem2D(const BernoulliProblem2D&) = delete;
    BernoulliProblem2D& operator=(const BernoulliProblem2D&) = delete;

    //! Helper method to setup a run
    //! Set up boundary, elements, operations, assemblers, sparse data structures
    virtual void SetupIteration();

    //! Helper method to setup a run
    virtual void SetupIteration(const std::vector<double>& points,
			const std::vector<double>& normals);
    
    //! Main functionality: compute the state
    //! \param[in] bc Dirichlet bcs
    virtual void ComputeState(const BCParams& bc);
    

    //! Compute the boundary fluxes at the previously computed state
    virtual void ComputeBoundaryFluxes();

    //! Update the boundary location
    //! \param[in] f_perturb Function that returns the perturbation magnitude
    virtual void UpdateBoundary(std::function<double(const double* X, const double& dudn, void*)> f_perturb,
			void* params);
    
    //! Helper method to finalize a run
    //! Destroy the boundary, elements, operations, assemblers, sparse data structures
    virtual void FinalizeIteration();
    
    //! Access the SSD function
    const mx::MaxEntSSD<2>& GetSSD() const;
    
    //! Access the conforming mesh
    const CoordConn& GetConformingMesh() const;

    //! Access the boundary geometry
    const geom::ImplicitManifold& GetBoundary() const;

    //! Access the positive vertices
    const std::vector<int>& GetPositiveVertices() const;
    
    //! Access positively cut elements and local nodes on the positive face
    const std::vector<int>& GetPositiveElmFacePairs() const;
    
    //! Access the boundary fluxes
    const std::map<int, double>& GetBoundaryFluxes() const;

    //! Access the state at the current iteration
    const ScalarMap& GetState() const;

    //! Access the boundary points
    const std::vector<double>& GetBoundaryPoints() const;

    //! Access the boundary normals
    const std::vector<double>& GetBoundaryNormals() const;

    //! Access the local to global map
    const LocalToGlobalMap& GetLocalToGlobalMap() const;

    //! Access to the element array
    const std::vector<Element*>& GetElementArray() const;

     //! Access to options
    const SSD_Options& GetSSDOptions();
    const Meshing_Options& GetMeshingOptions();
    const geom::NLSolverParams& GetGeomSolverParams();

    //! Access to the background mesh
    msh::StdTriMesh& GetBackgroundMesh();
    
  protected:
    //! Access to the assembler
    StandardAssembler<Laplacian>& GetLaplacianAssembler() const;

    //! Access to PETSc data structures
    PetscData& GetPetscData();
    
    //! Helper function to setup the geometry and mesh
    virtual void SetupBoundaryAndMesh();
    
    //! Helper function to setup elements and operations
    virtual void SetupElementsAndOperations();

    //! Helper function that updates the boundary given
    //! the perturbations at the +ve vertices
    virtual void UpdateBoundary_(const std::map<int, double>& pertMap);

  private:
    msh::StdTriMesh& BG; //!< Reference to the background mesh
    std::vector<double> BdPoints; //!< Set of points defining the boundary
    std::vector<double> BdNormals; //!< Set of points defining the normals

    const SSD_Options ssd_options; //!< Options for boundary fit
    const Meshing_Options mesh_options;  //!< options for create a conforming mesh
    const geom::NLSolverParams geom_nlparams; //!< Options for signed distance calcs

    CoordConn MD;  //!< Conforming mesh
    std::vector<int> PosVerts; //!< Positive vertices in the conforming mesh
    std::vector<int> PosElmsNodes; //!< Positive elements and local nodes on the positive face

    const ForceFieldValue<2>* Frc; //!< Specified force field
    std::vector<Element*> ElmArray; //!< Element array for the Bernoulli problem
    std::vector<Laplacian*> LapArray; //!< Laplacian operations for the Bernoulli problem
    std::vector<BodyForceWork*> FrcArray; //!< External force field
    LocalToGlobalMap* L2GMap; //!< Local to global map
    ScalarMap* UMap; //!< State solution
    StandardAssembler<Laplacian>* Lap_Asm; //!< Assembler for Laplacian operations
    StandardAssembler<BodyForceWork>* Frc_Asm; //!< Assembler for body force calcs

    mx::MaxEntSSD<2>* BdFit; //!< SSD representation for the boundary
    geom::ImplicitManifold* Geom; //!< Boundary manifold
    PetscData PD; //!< Petsc data structures

    std::map<int, double> BdFluxMap; //!< Map from node to the boundary flux there
    
    // Flags for tracking state: provide direct access to derived classes
  protected:
    mutable bool is_state_iteration_setup;
    mutable bool is_state_iteration_finalized;
    mutable bool is_state_iteration_computed;
    mutable bool is_boundary_updated;
    mutable bool is_flux_computed;
  };

  // Heleshaw problem for a closed moving boundary is identical
  using HeleShawProblem = BernoulliProblem2D;
}

#endif
