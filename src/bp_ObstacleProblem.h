// Sriramajayam

#ifndef BP_OBSTACLE_PROBLEM_H
#define BP_OBSTACLE_PROBLEM_H

#include <bp_BernoulliProblem2D.h>

namespace bp
{ 
  class ObstacleProblem: public BernoulliProblem2D
  {
  public:
    //! Constructor
    //! \param[in] bg Background mesh to be used
    //! \param[in] bdpts Initial guess for outer boundary points. Copied
    //! \param[in] bdnormals Initial guess for outer boundary normals. Copied
    //! \param[in] frc Force field to impose for the u-field. Zero force for the p-field
    //! \param[in] opt1 Options for boundary approximation with max-ent functions
    //! \param[in] opt2 Options for meshing
    //! \param[in] opt3 Nonlinear-closest-point-solver parameters for an implicit geometry
    //! \param[in] nodeset Node set for max-ent functions
    ObstacleProblem(msh::StdTriMesh& bg,
		    std::vector<double>& bdpts,
		    std::vector<double>& bdnormals,
		    const SSD_Options& opt1,
		    const Meshing_Options& opt2,
		    const geom::NLSolverParams& opt3,
		    const ForceFieldValue<2>* frc=nullptr);

    //! Destructor
    virtual ~ObstacleProblem();

    //! Disable copy and assignment
    ObstacleProblem(const ObstacleProblem&) = delete;
    ObstacleProblem& operator=(const ObstacleProblem&) = delete;

    //! Helper method to setup a run
    //! Set up boundary, elements, operations, assemblers, sparse data structures
    //! Overloaded
    virtual void SetupIteration() override;

    //! Helper method to setup a run
    virtual void SetupIteration(const std::vector<double>& points,
				const std::vector<double>& normals) override;
    
    //! Main functionality: compute the state. Overloaded
    //! \param[in] BCParams Boundary conditions (for both u & p fields)
    virtual void ComputeState(const BCParams& bc) override;
    
    //! Compute the boundary fluxes at the previously computed state
    //! Overloaded
    virtual void ComputeBoundaryFluxes() override;

    //! Helper method to finalize a run
    //! Destroy the boundary, elements, operations, assemblers, sparse data structures
    //! Overloaded
    virtual void FinalizeIteration() override;

    //! Access the adjoint solution at the  current iteration
    const ScalarMap& GetAdjointState() const;

    //! Update the boundary location
    //! \param[in] f_perturb Function that returns the perturbation magnitude
    //! Overload
    virtual void UpdateBoundary(std::function<double(const double* X,
						     const double& dudn, void*)> f_perturb, void* params) override;

    void UpdateBoundary(std::function<double(const double* X, const double* normal, const double& dudn,
						     const double& dpdn, void*)> f_perturb, void* params);
    
    //! Access the pressure flux
    const std::map<int, double>& GetAdjointStateFluxes() const;
    
  private:
    ScalarMap* PMap; //!< Pressure solution
    std::map<int, double> PFluxMap; //!< Pressure fluxes along the boundary
  };


  

}
#endif
