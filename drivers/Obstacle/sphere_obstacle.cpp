// Sriramajayam

#include <mx_MaxEnt.h>
#include <bp_Bernoulli2DModule>
#include <geom_LevelSetManifold.h>
#include <MeshUtils.h>
#include <omp.h>
#include <bp_ObstacleProblem.h>

// Namespaces intentionally omitted for clarity

// Define the obstacle function
// Sphere with radius R = 1.2, positioned at Z = 2.2 below the membrane
double ObstacleFunc(const double* X, double* gradG=nullptr)
{
  const double R = 1.2;
  const double Z = 2.2;
  const double r2 =  X[0]*X[0]+X[1]*X[1];
  assert(R*R>r2 && "sphere::ObstacleFunc- Cannot evaluate obstacle function");
  const double zdiff = std::sqrt(R*R-r2);
  const double gval = -Z + zdiff;
  if(gradG!=nullptr)
    { gradG[0] = -X[0]/zdiff;
      gradG[1] = -X[1]/zdiff; }
  return gval;
}


// Solution details
struct SolDetails
{
  const double R1 = 0.8; // Fixed outer radius
  const double Reps = 0.1; // tolerance for distinguishing inner & outer boundaries
  static double GetInnerBC(const double* X) // Dirichlet bcs on inner boundary
  { return ObstacleFunc(X); }
  const double U1 = 0.; // Dirichlet bcs on outer boundary
  const double fval = -10.; // Forcing term in the state problem
};


// --------------------------------------------------
// Helper functions
#include "./Obstacle_Utils.h"
// --------------------------------------------------


int main(int argc, char** argv)
{
  // Spatial dimension
  constexpr int SPD = 2;
  
  // Intialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  int nThreads = 1;
  omp_set_num_threads(1);

  // Subdivision options
  int ndiv_h = 5;
  PetscOptionsGetInt(NULL, NULL, (char*)"-ndivh", &ndiv_h, NULL);
  int ndiv_g = 3;
  PetscOptionsGetInt(NULL, NULL, (char*)"-ndivg", &ndiv_g, NULL);
  std::cout<<"\nSubdivision options: "
	   <<"ndivh = "<<ndiv_h<<", ndivg = "<<ndiv_g<<std::flush;
   
  
  // Nonlinear geometrical solver parameters
  geom::NLSolverParams geom_nlparams({
      .ftol=1.e-6,
	.ttol=1.e-6,
	.max_iter=25});

  // Meshing parameters
  bp::Meshing_Options mesh_options({
      .nMaxThreads=1,
	.MaxVertValency=6,
	.MaxElmValency=6,
	.nSteps=5,
	.nIters=8});
  
  // SSD Options
  // Node set for max-ent calculations
  std::vector<double> maxent_coordinates;
  std::vector<int> maxent_connectivity;
  GetEquilateralMesh(ndiv_g, maxent_coordinates, maxent_connectivity);
  // Mild-expansion to avoid evaluation at the boundary of the convex hull of max-ent nodes
  std::vector<double> maxent_nodeset = maxent_coordinates;
  for(auto& x:maxent_nodeset) x *= 1.01;
  // Mesh for max-ent calculations
  for(int i=0; i<2; ++i)
    msh::SubdivideTriangles(2, maxent_connectivity, maxent_coordinates);
  bp::SSD_Options ssd_options({
      .Tol0=1.e-6,
	.TolNR=1.e-6,
	.nnb=1,
	.gamma=0.8,
	.alpha=0.01,
	.nodeset=&maxent_nodeset,
	.coordinates=&maxent_coordinates,
	.connectivity=&maxent_connectivity,
	.nMaxThreads=nThreads,
	.verbose=false});
  
  // --------------------------------------------------------------------------- //
  // Solution details
  SolDetails sol_details;
  
  // Create an equilateral mesh to serve as the background mesh
  std::vector<double> Eq_coordinates;
  std::vector<int> Eq_connectivity;
  const double meshsize = GetEquilateralMesh(ndiv_h, Eq_coordinates, Eq_connectivity);
  msh::SeqCoordinates EqCoord(2, Eq_coordinates);
  msh::StdTriConnectivity EqConn(Eq_connectivity);
  msh::StdTriMesh EqTri(EqCoord, EqConn);
  msh::PlotTecStdTriMesh("Eq.tec", EqTri);

  // Level set function for outer circle
  double outer_rad = sol_details.R1;
  geom::LevelSetFunction f_outer_circ = OuterCircle;
  geom::LevelSetManifold<SPD> Geom_outer_circ(geom_nlparams, f_outer_circ, &outer_rad, nThreads);
  
  // Create a background mesh with the outer circle extracted
  std::vector<double> bg_coordinates;
  std::vector<int> bg_connectivity;
  { std::vector<int> temp1, temp2;
    bp::GetConformingMesh<SPD>(EqTri, Geom_outer_circ, mesh_options,
			       bg_coordinates, bg_connectivity, temp1, temp2); }
  for(auto& i:bg_connectivity) --i;
  msh::SeqCoordinates BgCoord(2, bg_coordinates);
  msh::StdTriConnectivity BgConn(bg_connectivity);
  msh::StdTriMesh BG(BgCoord, BgConn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Initial guess for the radius of the inner boundary
  std::vector<double> bdPoints, bdNormals;
  const double PI = 4.*atan(1.);
  const int nBdPoints = 40;
  for(int n=0; n<nBdPoints; ++n)
    {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(nBdPoints);
      bdPoints.push_back( 0.35*std::cos(theta) );
      bdPoints.push_back( 0.35*std::sin(theta) );
      bdNormals.push_back( -std::cos(theta) );
      bdNormals.push_back( -std::sin(theta) );
    }
  
  // Constant force field
  bp::ForceFieldValue<2> Frc(ConstForceFunc, &sol_details);
  
  // Initialize bernoulli problem
  bp::ObstacleProblem *bp2D =
    new bp::ObstacleProblem(BG, bdPoints, bdNormals,
			    ssd_options, mesh_options, geom_nlparams, &Frc);

  const int nTotalIter = 50;
  std::vector<double> j_val(nTotalIter);
  std::vector<double> r_err(nTotalIter);
  for(int iter=0; iter<nTotalIter; ++iter)
    {
      std::cout<<"\n\nIteration: "<<iter<< std::flush;

      //Plot boundary points and normals
      const auto& BdPoints = bp2D->GetBoundaryPoints();
      const auto& BdNormals = bp2D->GetBoundaryNormals();
      std::string filename = "bd-"+ std::to_string(iter) + ".dat";
      PlotPointsAndNormals(filename, BdPoints, BdNormals);

      // Setup this iteration
      bp2D->SetupIteration();

      // Dirichlet bcs
      const auto& MD = bp2D->GetConformingMesh();
      bp::BCParams bc;
      DirichletBCs(MD, bc.dirichlet_dofs, bc.dirichlet_values, sol_details); 

      // Details of this iteration
      const auto& ssdfit = bp2D->GetSSD();
      filename = "ssd-" + std::to_string(iter) + ".tec";
      ssdfit.PlotTec(filename.c_str(), MD);
	
      // Compute the state
      bp2D->ComputeState(bc);
	
      const auto& UMap = bp2D->GetState();
      filename = "sol-" + std::to_string(iter) + ".tec";
      PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &UMap.Get()[0], 1);

      // Compute fluxes
      std::cout<<"\nComputing boundary fluxes..."<<std::flush;
      bp2D->ComputeBoundaryFluxes();
      const auto& PosVerts = bp2D->GetPositiveVertices();
      const auto& BdFluxes = bp2D->GetBoundaryFluxes();
      filename = "flux-" + std::to_string(iter) + ".dat";
      PlotFluxes(filename, MD, BdFluxes, PosVerts);

      //Plot target fluxes
      filename = "tflux-" + std::to_string(iter) + ".dat";
      PlotTargetFluxes(filename, BdPoints, BdNormals);
      
      // Compute the value of J_Omega
      j_val[iter] = ComputeJOmega(bp2D->GetElementArray(),
				  bp2D->GetLocalToGlobalMap(),
				  sol_details.fval,
				  &UMap.Get()[0]);
      

      // Compute the error in radius
      const double RExact = 0.247653948;
      r_err[iter] = ComputeRError(MD, bp2D->GetPositiveElmFacePairs(), RExact);

      // Update the boundary
      std::cout<<"\nUpdating the boundary..."<<std::flush;
      double dh_by_vnorm = meshsize/100.;
      bp2D->UpdateBoundary(f_boundary_perturb, &dh_by_vnorm);
      
      // Proceed to the next iteration
      std::cout<<"\nFinalizing..."<<std::flush;
      bp2D->FinalizeIteration();
    }

  // Print the values of J realized 
  PlotJ_Values("jvals.dat", j_val);
  // Print the values of r_error realized 
  PlotJ_Values("r_err.dat", r_err);
  
  // Done
  delete bp2D;
  PetscFinalize();
}
