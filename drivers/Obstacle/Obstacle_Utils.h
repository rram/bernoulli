// Sriramajayam

#ifndef OBSTACLE_UTILS_H
#define OBSTACLE_UTILS_H

// Create a mesh of equilateral triangles over a hexagon with given set size.
// Its vertices will be the max-ent nodes
// connectivity: numbered from 0
double GetEquilateralMesh(const double aval,
			  const int nDiv,
			  std::vector<double>& coord,
			  std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( aval*std::cos(theta) );
      coord.push_back( aval*std::sin(theta) );
    }
  double meshsize = aval;

  // Subdivide
  for(int i=0; i<nDiv; i++)
    { msh::SubdivideTriangles(2, conn, coord);
      meshsize /= 2.; }

  return meshsize;
}


// plot boundary points
void PlotPoints(const std::string filename, const std::vector<double>& BdPoints)
{
  std::fstream stream;
  stream.open(filename.c_str(), std::ios::out);
  assert(stream.good());
  stream << "# X \t Y \n";
  const int nPoints = static_cast<int>(BdPoints.size())/2;
  for(int p=0; p<nPoints; ++p)
    stream << BdPoints[2*p]<<" "<<BdPoints[2*p+1]<<"\n";
  stream.close();
}

// plot boundary points and normals
void PlotPointsAndNormals(const std::string filename,
			  const std::vector<double>& BdPoints,
			  const std::vector<double>& BdNormals)
{
  std::fstream stream;
  stream.open(filename.c_str(), std::ios::out);
  assert(stream.good());
  stream << "# X \t Y \t NX \t NY \n";
  const int nPoints = static_cast<int>(BdPoints.size())/2;
  for(int p=0; p<nPoints; ++p)
    stream << BdPoints[2*p]<<" "<<BdPoints[2*p+1]<<" "
	   <<BdNormals[2*p]<<" "<<BdNormals[2*p+1]<< "\n";
  stream.close();
} 



// Dirchlet bcs
void DirichletBCs(const CoordConn& MD,
		  std::vector<int>& boundary, std::vector<double>& bvalues,
		  const SolDetails& sol_details)
{
  boundary.clear();
  bvalues.clear();
  
  // Get the neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify boundary nodes
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int* conn = &MD.connectivity[3*e];
	  bdnodes.insert(conn[f]-1);
	  bdnodes.insert(conn[(f+1)%3]-1);
	}

  // Set boundary values
  for(auto& n:bdnodes)
    {
      boundary.push_back( n );
      const double* X = &MD.coordinates[2*n];
      double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      double U0 = sol_details.GetInnerBC(X);
      if(std::abs(sol_details.R1-rX)>sol_details.Reps)
	// Inner boundary
	bvalues.push_back( U0 );
      else
	// Outer boundary
	bvalues.push_back( sol_details.U1 );
    }
  return;
}

// Level set function for outer circle
void OuterCircle(const double* X, double& F, double* dF, double* d2F, void* params)
{
  assert(params!=nullptr);
  const double rad = (*static_cast<double*>(params));
  
  F = -rad*rad + X[0]*X[0] + X[1]*X[1];
  if(dF!=nullptr)
    {
      dF[0] = 2.*X[0];
      dF[1] = 2.*X[1];
    }
  if(d2F!=nullptr)
    {
      d2F[0] = 2.; d2F[1] = 0.;
      d2F[2] = 0.; d2F[3] = 2.;
    }
  return;
}


// Constant force field
bool ConstForceFunc(const std::vector<double> u, const std::vector<double> X,
		    std::vector<double>* f, std::vector<double>* df, void* params)
{
  assert(params!=nullptr);
  auto* sol_details = static_cast<SolDetails*>(params);
  if(f->size()==0) f->resize(1);
  (*f)[0] = -1.*sol_details->fval; return true; } // -ve sign is a hack


// Boundary update function
struct BoundaryUpdateParams
{
  double tau; // tau value in the objective function
  double dt; // Time step
};

double BoundaryUpdate(const double* X, const double* normal,
		      const double& dudn, const double& dpdn, void* params )
{
  assert(params!=nullptr);
  const BoundaryUpdateParams* bup = static_cast<BoundaryUpdateParams*>(params);
  const double tau = bup->tau;
  const double dt = bup->dt;

  // Calculate the target flux
  double gradG[2];
  ObstacleFunc(X, gradG);
  
  // dphi/dn
  double dphidn = gradG[0]*normal[0] + gradG[1]*normal[1];  //Target flux
 
  // Velocity of this node
  double vel = (dphidn - dudn)*(0.5*(dphidn + dudn) - tau*dpdn  + (tau-1.)*dudn) ;
  //double vel = (dphidn - dudn);
  
  // Perturbation for this node
  return dt*vel;
}

// Boundary update function
std::function<double(const double*, const double*,
		     const double&, const double&, void*)> f_boundary_perturb = BoundaryUpdate;


// Computes a sequential ordering of a given set of faces
void GetFaceChain(const std::vector<int>& ElmFacePairs,
		  const CoordConn& MD,
		  std::vector<int>& VertChain)
{
  // Create a map from vertex 1 -> vertex 2 for each edge
  std::map<int, int> left2right;
  const int nFaces = static_cast<int>(ElmFacePairs.size()/3);
  assert(nFaces>0 && "Obstacle_Utils::GetFaceChain()- Unexpected number of faces");
  for(int i=0; i<nFaces; ++i)
    { const int elm = ElmFacePairs[3*i];
      const int a = ElmFacePairs[3*i+1];
      const int b = ElmFacePairs[3*i+2];
      left2right.insert(std::make_pair(MD.connectivity[3*elm+a]-1, MD.connectivity[3*elm+b]-1));
    }
  assert(static_cast<int>(left2right.size())==nFaces);

  // Insert the first edge
  auto it = left2right.begin();
  const int v0 = it->first;
  VertChain.clear();
  VertChain.push_back(v0);
  while(true)
    {
      int vnext = it->second;
      if(vnext==VertChain[0])
	break;
      else
	VertChain.push_back(vnext);

      // Find the next edge having left vertex = vnext
      it = left2right.find(vnext);
      assert(it!=left2right.end() && "Obstacle_Utils::GetFaceChain()- could not close chain");
    }
  assert(static_cast<int>(VertChain.size())==nFaces &&
	 "Obstacle_Utils::GetFaceChain()- unexpected number of vertices in chain");
  
  return;
}


// Plots the set of positive vertices as a chain
void PlotPositiveVertexChain(const std::string filename,
			 const bp::ObstacleProblem* op)
{
  // Get the list of positive element-face pairs
  const auto& PosElmFacePairs = op->GetPositiveElmFacePairs();
  const auto& MD = op->GetConformingMesh();

  // Ordered sequence of positive vertices
  std::vector<int> PosVertChain;
  GetFaceChain(PosElmFacePairs, MD, PosVertChain);

  // Get the ssd function
  const auto& ssd = op->GetSSD();

  // Plot the sequence of boundary points and the normals there
  // Repeat the first node to close the chain
  PosVertChain.push_back( PosVertChain[0] );
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  for(auto& n:PosVertChain)
    {
      const double* X = &MD.coordinates[2*n];
      double F, dF[2];
      ssd.Evaluate(X, F, dF);
      double norm = std::sqrt(dF[0]*dF[0]+dF[1]*dF[1]);
      dF[0] /= norm; dF[1] /= norm;
      pfile << X[0]<<" "<<X[1]<<" "<<dF[0]<<" "<<dF[1]<<"\n";
    }
  pfile.flush();
  pfile.close();
}


// Plot boundary fluxes along the normal
void PlotFluxes(const std::string filename,
		const std::map<int, double>& BdFluxes,
		const bp::ObstacleProblem* op)
{
  // Get the list of positive element-face pairs
  const auto& PosElmFacePairs = op->GetPositiveElmFacePairs();
  const auto& MD = op->GetConformingMesh();

  // Ordered sequence of positive vertices
  std::vector<int> PosVertChain;
  GetFaceChain(PosElmFacePairs, MD, PosVertChain);

  // Get the ssd function
  const auto& ssd = op->GetSSD();

  // Plot the sequence of boundary points and the normals scaled by the boundary flux values
  // Repeat the first node to close the chain
  PosVertChain.push_back( PosVertChain[0] );
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  pfile <<"# theta \t X \t Y \t flux*Nx \t flux*Ny \n";
  for(auto& n:PosVertChain)
    {
      const double* X = &MD.coordinates[2*n];
      double F, dF[2];
      ssd.Evaluate(X, F, dF);
      double norm = std::sqrt(dF[0]*dF[0]+dF[1]*dF[1]);
      dF[0] /= norm; dF[1] /= norm;
      auto it = BdFluxes.find(n);
      assert(it!=BdFluxes.end() && "Obstacle_Utils::PlotFluxes- Could not access flux at positive vertex");
      const double qn = it->second;
      double theta = std::atan2(X[1], X[0]); 
      pfile << theta<<" "<<X[0]<<" "<<X[1]<<" "<<qn*dF[0]<<" "<<qn*dF[1]<<"\n";
    }
  pfile.flush();
  pfile.close();
}



// Plot target boundary fluxes
void PlotTargetFluxes(const std::string filename,
		      const bp::ObstacleProblem* op)
{
   // Get the list of positive element-face pairs
  const auto& PosElmFacePairs = op->GetPositiveElmFacePairs();
  const auto& MD = op->GetConformingMesh();

  // Ordered sequence of positive vertices
  std::vector<int> PosVertChain;
  GetFaceChain(PosElmFacePairs, MD, PosVertChain);
  
  // Get the ssd function
  const auto& ssd = op->GetSSD();

  // Plot the sequence of boundary points and the normals scaled by the boundary flux values
  // Repeat the first node to close the chain
  PosVertChain.push_back( PosVertChain[0] );
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  pfile <<"# theta \t X \t Y \t flux*Nx \t flux*Ny \n";
  for(auto& n:PosVertChain)
    {
      const double* X = &MD.coordinates[2*n];
      double F, dF[2];
      ssd.Evaluate(X, F, dF);
      double norm = std::sqrt(dF[0]*dF[0]+dF[1]*dF[1]);
      dF[0] /= norm; dF[1] /= norm;

      // dphi/dn
      double gradG[2];
      ObstacleFunc(X, gradG);
      double qn = gradG[0]*dF[0] + gradG[1]*dF[1]; // Target flux

      double theta = std::atan2(X[1], X[0]); 
      pfile << theta <<" "<<X[0]<<" "<<X[1]<<" "<<qn*dF[0]<<" "<<qn*dF[1]<<"\n";
    }
  pfile.flush();
  pfile.close();
}


// Plot free boundary velocities
void PlotVelocities(const std::string filename, 
		    const bp::ObstacleProblem* op, const double tau)
{
  // Get the list of positive element-face pairs
  const auto& PosElmFacePairs = op->GetPositiveElmFacePairs();
  const auto& MD = op->GetConformingMesh();

  // Ordered sequence of positive vertices
  std::vector<int> PosVertChain;
  GetFaceChain(PosElmFacePairs, MD, PosVertChain);

  // Get the ssd function
  const auto& ssd = op->GetSSD();

  // Boundary fluxes at the positive vertices
  const auto& uflux = op->GetBoundaryFluxes();
  const auto& pflux = op->GetAdjointStateFluxes();
  
  // Plot the sequence of boundary points and the normals scaled by the boundary flux values
  // Repeat the first node to close the chain
  PosVertChain.push_back( PosVertChain[0] );
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  pfile <<"# theta \t X \t Y \t v*Nx \t v*Ny \n";

  BoundaryUpdateParams params({.tau=tau, .dt=1.});
  for(auto& n:PosVertChain)
    {
      const double* X = &MD.coordinates[2*n];
      double F, dF[2];
      ssd.Evaluate(X, F, dF);
      double norm = std::sqrt(dF[0]*dF[0]+dF[1]*dF[1]);
      dF[0] /= norm; dF[1] /= norm;

      auto it = uflux.find(n);
      assert(it!=uflux.end() && "Obstacle_Utils::PlotVelocities- Could not access u-flux at positive vertex");
      auto jt = pflux.find(n);
      assert(jt!=pflux.end() && "Obstacle_Utils::PlotVelocities- Could not access p-flux at positive vertex");
      
      const double& dudn = it->second;
      const double& dpdn = jt->second;

      double Vn = BoundaryUpdate(X, dF, dudn, dpdn, &params);
      
      double theta = std::atan2(X[1], X[0]); 
      pfile << theta<<" "<<X[0]<<" "<<X[1]<<" "<<Vn*dF[0]<<" "<<Vn*dF[1]<<"\n";
    }
  pfile.flush();
  pfile.close();
}




// Compute J-dot for a given velocity field
double ComputeJdot(const bp::ObstacleProblem* op, const double tau)
{
  double Jdot = 0.;

  // Get the positive element face pairs
  const auto& PosVerts = op->GetPositiveVertices();
  const auto& PosElmFacePairs = op->GetPositiveElmFacePairs();
  const auto& MD = op->GetConformingMesh();
  
  // Ordered sequence of positive vertices
  std::vector<int> PosVertChain;
  GetFaceChain(PosElmFacePairs, MD, PosVertChain);

  // Append the first vertex again to close the chain
  PosVertChain.push_back( PosVertChain[0] );
  
  // Boundary fluxes at the positive vertices
  const auto& uflux = op->GetBoundaryFluxes();
  const auto& pflux = op->GetAdjointStateFluxes();

  // Params to evaluate the velocity
  BoundaryUpdateParams params({.tau=tau, .dt=1.});

  const auto& ssd = op->GetSSD();
  
  // Compute the velocities at positive vertices
  std::map<int, double> Vn;
  for(auto& n:PosVerts)
    {
      auto it = uflux.find(n);
      assert(it!=uflux.end() &&
	     "Obstacle_Utils::ComputeJdot- could not access uflux at positive vertex");
      const double& dudn = it->second;
      auto jt = pflux.find(n);
      assert(jt!=pflux.end() &&
	     "Obstacle_Utils::ComputeJdot- could not access pflux at positive vertex");
      const double& dpdn = jt->second;
      
      const double* X = &MD.coordinates[2*n];

      double sdval, normal[2];
      ssd.Evaluate(X, sdval, normal);
      double norm = std::sqrt(normal[0]*normal[0]+normal[1]*normal[1]);
      normal[0] /= norm;
      normal[1] /= norm;
      Vn[n] = BoundaryUpdate(X, normal, dudn, dpdn, &params);
    }

  // Quadrature rule over the segment [0,1]
  const double qpts[] = {0.5-0.5*std::sqrt(3./5.), 0.5, 0.5+0.5*std::sqrt(3./5.)};
  const double qwts[] = {5./18., 8./18., 5./18.};
  const int nQuad = 3;
  
  // Integrate over the free boundary
  const int nFaces = static_cast<int>(PosElmFacePairs.size()/3);
  for(int f=0; f<nFaces; ++f)
    {
      // Nodes of this face
      const int n = PosVertChain[f];
      const int m = PosVertChain[f+1];
      const double* X = &MD.coordinates[2*n];
      const double* Y = &MD.coordinates[2*m];
      const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
      
      // Nodal velocities
      auto it_n = Vn.find(n); assert(it_n!=Vn.end());
      auto it_m = Vn.find(m); assert(it_m!=Vn.end());
      const double vn = it_n->second;
      const double vm = it_m->second;
      
      for(int q=0; q<nQuad; ++q)
	{
	  // Velocity at this quadrtaure point
	  const double lambda = qpts[q];
	  const double vq = lambda*vn + (1.-lambda)*vm;

	  // Update
	  Jdot -= lenXY*qwts[q]*vq*vq;
	}
    }
      
  return Jdot;
}


// Compute J over the non-coincidence set for given values of tau
double ComputeJ_NonCoincidence(const bp::ObstacleProblem* op,
			       const SolDetails& sol_details, const double tau)
{
  // Compute J over the non-coincidence set
  const auto& MD = op->GetConformingMesh();

  // Access the local to global map
  const auto& L2GMap = op->GetLocalToGlobalMap();
  
  // Access to the element array
  const auto& ElmArray = op->GetElementArray();

  // Access the state
  const auto& uvals = op->GetState().Get();

  // Integrate
  double Jval = 0.;
  for(int e=0; e<MD.elements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      
      for(int q=0; q<nQuad; ++q)
	{
	  // u-value here
	  double uval = 0.;
	  for(int a=0; a<3; ++a)
	    uval += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);

	  // grad(u) here
	  double du[2] = {0.,0.};
	  for(int a=0; a<3; ++a)
	    for(int j=0; j<2; ++j)
	      du[j] += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,q,a,j);

	  // Force value
	  double fval = sol_details.fval;

	  // Update J
	  Jval += Qwts[q]*( 0.5*(du[0]*du[0]+du[1]*du[1]) + (tau-1)*fval*uval );
	}
    }

  return Jval;
}


// Compute J over the coincidence set for given values of tau
double ComputeJ_Coincidence(msh::StdOrphanMesh& BG,
			    const mx::MaxEntSSD<2>& ssd,
			    const geom::NLSolverParams& geom_nlparams,
			    const bp::Meshing_Options& mesh_options,
			    const SolDetails& sol_details,
			    const double tau)
{
  // Geometry of the coincidence set
  std::vector<double> ssd_dofs = ssd.GetDofs();
  for(auto& x:ssd_dofs) x*= -1.;
  geom::ImplicitMaxEntManifold<mx::MaxEnt2D> ctc_geom(ssd.GetMaxEntFunctions(),
						      ssd_dofs, 
						      geom_nlparams,
						      1); // Number of threads

  // Create a mesh over the coincide
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.coordinates.clear();
  MD.connectivity.clear();
  {
    std::vector<int> temp1, temp2;
    bp::GetConformingMesh<2>(BG, ctc_geom, mesh_options,
			     MD.coordinates, MD.connectivity,
			     temp1, temp2);
    MD.nodes = static_cast<int>(MD.coordinates.size())/2;
    MD.elements = static_cast<int>(MD.connectivity.size())/3; }

  //PlotTecCoordConn((char*)"coincidence.tec", MD);

  // Integrate functional over the coincidence set
  double Jval = 0.;
  const auto* qrule = Triangle_1::Bulk;
  const int nQuad = qrule->GetNumberQuadraturePoints();
  double ref_area = 0.;
  for(int q=0; q<nQuad; ++q)
    ref_area += qrule->GetQuadratureWeights(q);
  
  for(int e=0; e<MD.elements; ++e)
    {
      // Area of this triangle
      const int* conn = &MD.connectivity[3*e];
      const double* A = &MD.coordinates[2*(conn[0]-1)];
      const double* B = &MD.coordinates[2*(conn[1]-1)];
      const double* C = &MD.coordinates[2*(conn[2]-1)];
      const double AB = std::sqrt((A[0]-B[0])*(A[0]-B[0])+(A[1]-B[1])*(A[1]-B[1]));
      const double BC = std::sqrt((B[0]-C[0])*(B[0]-C[0])+(B[1]-C[1])*(B[1]-C[1]));
      const double CA = std::sqrt((C[0]-A[0])*(C[0]-A[0])+(C[1]-A[1])*(C[1]-A[1]));
      const double semi = 0.5*(AB+BC+CA);
      const double Area = std::sqrt(semi*(semi-AB)*(semi-BC)*(semi-CA));
      
      // Sum over quadrature points
      for(int q=0; q<nQuad; ++q)
	{
	  // This quadrature point
	  const double* qpt = qrule->GetQuadraturePoint(q);
	  double X[2] = {0.,0.};
	  for(int k=0; k<2; ++k)
	    X[k] = A[k]*qpt[0] + B[k]*qpt[1] + C[k]*(1.-qpt[0]-qpt[1]);

	  // Height function here
	  double dpsi[2];
	  double psi = ObstacleFunc(X, dpsi);
	  double fval = sol_details.fval;

	  // Integrate
	  const double qwt = (Area/ref_area)*qrule->GetQuadratureWeights(q);
	  Jval += qwt*( 0.5*(dpsi[0]*dpsi[0]+dpsi[1]*dpsi[1]) + (tau-1.)*fval*psi );
	}
    }
  return Jval;
}


// Estimate the time step
double EstimateDt(const bp::ObstacleProblem* op,
		  const double tau,
		  const double dh)
{
  // Get the list of positive vertices
  const auto& PosVerts = op->GetPositiveVertices();
  const auto& MD = op->GetConformingMesh();
  
  // Boundary fluxes at the positive vertices
  const auto& uflux = op->GetBoundaryFluxes();
  const auto& pflux = op->GetAdjointStateFluxes();

  // Keep track of the nodal velocities
  std::vector<double> Vn;
  Vn.clear();

  // Access the geometry
  const auto& ssd = op->GetSSD();

  // Params to evaluate the velocity
  BoundaryUpdateParams params({.tau=tau, .dt=1.});
  
  // Compute the velocities at positive vertices
  for(auto& n:PosVerts)
    {
      auto it = uflux.find(n);
      assert(it!=uflux.end() &&
	     "Obstacle_Utils::EstimateDt- could not access uflux at positive vertex");
      const double& dudn = it->second;
      auto jt = pflux.find(n);
      assert(jt!=pflux.end() &&
	     "Obstacle_Utils::EstimateDt- could not access pflux at positive vertex");
      const double& dpdn = jt->second;
      
      const double* X = &MD.coordinates[2*n];

      double sdval, normal[2];
      ssd.Evaluate(X, sdval, normal);
      double norm = std::sqrt(normal[0]*normal[0]+normal[1]*normal[1]);
      normal[0] /= norm;
      normal[1] /= norm;
      Vn.push_back( std::abs(BoundaryUpdate(X, normal, dudn, dpdn, &params)) );
    }

  // Compute the maximum nodal velocity
  const double vmax = *std::max_element(Vn.begin(), Vn.end());
  std::cout<<"\nMax velocity: "<<vmax<<std::flush;
  
  // dt*vmax = dh
  return dh/vmax;
}


// Read the free boundary point cloud
void ReadPointCloud(const std::string filename,
		    std::vector<double>& points,
		    std::vector<double>& normals)
{
  points.clear();
  normals.clear();
  
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::in);
  assert(pfile.good());
  double val;
  pfile >> val;
  while(pfile.good())
    {
      points.push_back( val );
      pfile >> val; points.push_back( val );
      pfile >> val; normals.push_back( val );
      pfile >> val; normals.push_back( val );
      pfile >> val;
    }
  pfile.close();
  assert(points.size()==normals.size());
  return;
}



// Gradient descent algorithm with fixed time stepping
void GradientDescent(bp::ObstacleProblem* op,
		     const SolDetails& sol_details, // Solution details
		     const double tau, // Algorithmic parameter
		     const int nTotalIter, // max number of iterations
		     const double dt, // Time stepping
		     const std::string jfilename) // File with details of J
{
  assert(nTotalIter>0);
  assert(tau>1.);
  assert(dt>0.);

  // Info
  auto& BG = op->GetBackgroundMesh();
  auto& geom_nlparams = op->GetGeomSolverParams();
  auto& mesh_options = op->GetMeshingOptions();

  // Record values of J
  std::fstream jfile;
  jfile.open(jfilename.c_str(), std::ios::out);
  jfile << "# Iter \t JOmega \t Jdot \t Jcoincidence \t Jnoncoincidence \n";

  BoundaryUpdateParams bd_update_params({.tau=tau, .dt=dt});
  std::string filename;
  
  for(int iter=0; iter<nTotalIter; ++iter)
    {
      std::cout<<"\n\nIteration: "<<iter<<std::flush;

      // Setup this iteration
      op->SetupIteration();

      // Plot boundary points and normals
      filename = "bd-" + std::to_string(iter) + ".dat";
      PlotPositiveVertexChain(filename, op);
      const auto& MD = op->GetConformingMesh();
      const auto& ssd = op->GetSSD();
      filename = "ssd-" + std::to_string(iter) + ".tec";
      ssd.PlotTec(filename.c_str(), MD);

      // Dirichlet BCs
      bp::BCParams bc;
      DirichletBCs(MD, bc.dirichlet_dofs, bc.dirichlet_values, sol_details);

      // Compute the state and adjoint
      op->ComputeState(bc);
      const auto& UMap = op->GetState();
      const auto& PMap = op->GetAdjointState();
      filename = "u-" + std::to_string(iter) + ".tec";
      PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &UMap.Get()[0], 1);
      filename = "p-" + std::to_string(iter) + ".tec";
      PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &PMap.Get()[0], 1);

      // Compute boundary fluxes
      op->ComputeBoundaryFluxes();
      const auto& uFluxes = op->GetBoundaryFluxes();
      const auto& pFluxes = op->GetAdjointStateFluxes();
      filename = "uflux-" + std::to_string(iter) + ".dat";
      PlotFluxes(filename, uFluxes, op);
      filename = "pflux-" + std::to_string(iter) + ".dat";
      PlotFluxes(filename, pFluxes, op);

      // Plot target fluxes
      filename = "psiflux-" + std::to_string(iter) + ".dat";
      PlotTargetFluxes(filename, op);

      // Plot the velocity field
      filename = "v-" + std::to_string(iter) + ".dat";
      PlotVelocities(filename, op, tau);
      
      // Compute Jdot for the current velocity field
      double Jdot = ComputeJdot(op, tau);

      // Compute J over the non-coincidence set
      double J_nctc = ComputeJ_NonCoincidence(op, sol_details, tau);

      // Compute J over the coincidence set
      double J_ctc = ComputeJ_Coincidence(BG, ssd, geom_nlparams, mesh_options, sol_details, tau);

      // J_Omega
      double Jval = J_ctc + J_nctc;

      // Record J-values for this iteration
      jfile << iter <<" " <<Jval <<" "<<Jdot <<" "<<J_ctc<<" "<<J_nctc<<"\n";
      jfile.flush();

      // Echo the details of this iteration
      std::cout<<"\nJOmega: "<<Jval
	       <<"\nJdot: "<<Jdot
	       <<"\ndt: "<<bd_update_params.dt<<std::flush;
      
      // Update
      op->UpdateBoundary(f_boundary_perturb, &bd_update_params);

      // Proceed to the next iteration
      op->FinalizeIteration();
    }

  // Done
  jfile.close();
  return;
}



// Gradient descent algorithm
int GradientDescentWithBackTracking(bp::ObstacleProblem* op,
				    const SolDetails& sol_details, // Solution details
				    const double tau, // Algorithmic parameter
				    const int nTotalIter, // Total number of iterations
				    double dh, // Initial value for nodal perturbations
				    const double dhmin, // Minimum permissible dh
				    const std::string jfilename) // File with details of J
{
  assert(nTotalIter>0);
  assert(tau>1.);
  assert(dh>0.);

  // Info
  auto& BG = op->GetBackgroundMesh();
  auto& geom_nlparams = op->GetGeomSolverParams();
  auto& mesh_options = op->GetMeshingOptions();
  
  // Setup identifiers for backtracking
  bool backtrack = false;
  int restart_iter = -1;     // Which iteration to restart from
  int jmin_iter = -1;        // Which iteration yielded the lowest J thus far
  std::vector<double> Jvec(nTotalIter, -1.);
  std::vector<bool> IsGoodIter(nTotalIter, false);

  // Keep track of the minimum of J
  double Jmin = -1.;
  std::string filename;

  // Record of J-values
  std::fstream jfile;
  jfile.open(jfilename.c_str(), std::ios::out);
  jfile << "# Iter \t JOmega \t Jdot \t Jcoincidence \t Jnoncoincidence\n";

  BoundaryUpdateParams bd_update_params({.tau=tau, .dt=0.});
  int nItersCompleted = nTotalIter-1;
  
  for(int iter=0; iter<nTotalIter; ++iter)
    {
      std::cout<<"\n\nIteration: "<<iter<<std::flush;

      // Setup this iteration
      if(backtrack==false) op->SetupIteration();
      else
	{
	  // Restart from a good iteration
	  std::vector<double> points, normals;
	  filename = "bd-" + std::to_string(restart_iter) + ".dat";
	  ReadPointCloud(filename, points, normals);
	  
	  // Find a good iteration prior to restart to use as J value
	  jmin_iter = -1;
	  for(int j=restart_iter-1; j>=0; --j)
	    if(IsGoodIter[j])
	      { Jmin = Jvec[j]; jmin_iter = j; break; }
	  assert(jmin_iter!=-1);
	  
	  std::cout<<"\nRestarting with fb_iter: "<<restart_iter
		   <<", jmin_iter: "<<jmin_iter<<std::flush;
	  
	  op->SetupIteration(points, normals);
	  dh /= 2.;
	}

      // Plot boundary points and normals
      filename = "bd-" + std::to_string(iter) + ".dat";
      PlotPositiveVertexChain(filename, op);
      const auto& MD = op->GetConformingMesh();
      const auto& ssd = op->GetSSD();
      filename = "ssd-" + std::to_string(iter) + ".tec";
      ssd.PlotTec(filename.c_str(), MD);

      // Dirichlet BCs
      bp::BCParams bc;
      DirichletBCs(MD, bc.dirichlet_dofs, bc.dirichlet_values, sol_details);

      // Compute the state and adjoint
      op->ComputeState(bc);
      const auto& UMap = op->GetState();
      const auto& PMap = op->GetAdjointState();
      filename = "u-" + std::to_string(iter) + ".tec";
      PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &UMap.Get()[0], 1);
      filename = "p-" + std::to_string(iter) + ".tec";
      PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &PMap.Get()[0], 1);
      
      // Compute boundary fluxes
      op->ComputeBoundaryFluxes();
      const auto& uFluxes = op->GetBoundaryFluxes();
      const auto& pFluxes = op->GetAdjointStateFluxes();
      filename = "uflux-" + std::to_string(iter) + ".dat";
      PlotFluxes(filename, uFluxes, op);
      filename = "pflux-" + std::to_string(iter) + ".dat";
      PlotFluxes(filename, pFluxes, op);

      // Plot target fluxes
      filename = "psiflux-" + std::to_string(iter) + ".dat";
      PlotTargetFluxes(filename, op);

      // Plot the velocity field
      filename = "v-" + std::to_string(iter) + ".dat";
      PlotVelocities(filename, op, tau);
  
      // Compute Jdot for the current velocity field
      double Jdot = ComputeJdot(op, tau);

      // Compute J over the non-coincidence set
      double J_nctc = ComputeJ_NonCoincidence(op, sol_details, tau);

      // Compute J over the coincidence set
      double J_ctc = ComputeJ_Coincidence(BG, ssd, geom_nlparams, mesh_options, sol_details, tau);

      // J_Omega
      double Jval = J_ctc + J_nctc;
      Jvec[iter] = Jval;

      // Record the J-values for this iteration
      jfile << iter <<" "<<Jval <<" "<<Jdot <<" "<<J_ctc<<" "<<J_nctc<<"\n";
      jfile.flush();

      // March forward?
      if(iter==0 || Jval<Jmin)
	{
	  backtrack = false;
	  Jmin = Jval;
	  restart_iter = iter;
	  bd_update_params.dt = EstimateDt(op, tau, dh);
	  IsGoodIter[iter] = true;
	}
      // Or backtrack?
      else
	{
	  backtrack = true;
	  bd_update_params.dt = 0.;
	  IsGoodIter[iter] = false;
	}

      // Echo the details of this iteration
      std::cout<<"\nJOmega: "<<Jval
	       <<"\nJdot: "<<Jdot
	       <<"\ndh: "<<dh
	       <<"\ndt: "<<bd_update_params.dt<<std::flush;
      if(backtrack)
	std::cout<<"\nBacktracking..."<<std::flush;
      
      // Update the boundary
      op->UpdateBoundary(f_boundary_perturb, &bd_update_params);

      // Proceed to the next iteration
      op->FinalizeIteration();

      if(dh<dhmin)
	{ std::cout<<"\n\nTerminating based on minimal perturbation criterion."<<std::flush;
	  nItersCompleted = iter;
	  break; }
    }

  // Done
  jfile.close();
  return nItersCompleted;
}
		     
#endif
