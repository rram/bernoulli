// Sriramajayam

// Solves a laplacian problem of a annulus-shaped region with
// prescribed Dirichlet bcs on the inner and outer boundaries
// The outer boundary moves with the velocity given by a shape derivate.
// At each iteration, vertices in the mesh are relaxed

#include <bp_Laplacian2DModule>
#include <msh_TriangleModule>
#include <dvr_TriangleModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <omp.h>
#include <map>

using namespace bp;

// Helper struct for problem details
struct SolDetails
{
  static constexpr double R0 = 0.2; // Fixed inner radius
  static constexpr double Reps = 0.05; // Distance tolerance along the radial direction
  static constexpr double U0 = 1.0; // Dirichlet bc on inner radius
  static constexpr double U1 = 0.0; // Dirichlet bc on outer radius
  static constexpr double fval = 4.; // Forcing term in the state problem
  static constexpr double gval = -2.0271851144019437; // Target boundary flux

  // Returns the exact solution for the state problem
  static double GetExactSolution(const double* X);
  // Returns the flux on the outer boundary
  static double GetOuterBdFlux(const double* X)
  { return gval; }
};

// Returns the mesh size
double GetMeshSize(const CoordConn& MD);

// Project nodes of INNER boundary only
void ProjectInnerBdNodes(CoordConn& MD);

// Constant force field
bool ConstForceFunc(const std::vector<double> u, const std::vector<double> X,
		    std::vector<double>* f, std::vector<double>* df, void* params)
{ if(f->size()==0) f->resize(1);
  (*f)[0] = -1.*SolDetails::fval; return true; } // -ve sign is a hack

// Assign dirichlet BCs: U0 on inner boundary, U1 on outer boundary
void GetDirichletBCs(const CoordConn& MD, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* uvals);

// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals);

// Recover boundary fluxes
void RecoverBoundaryFluxes(const CoordConn& MD, const ScalarMap& UMap,
			   StandardAssembler<Laplacian>& LapAsm,
			   StandardAssembler<BodyForceWork>& FrcAsm,
			   PetscData& PD,
			   std::vector<int>& bdnodes, std::vector<double>& fluxes,
			   std::vector<double>& normals);

// Plot recovered boundary fluxes
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::vector<int>& bdnodes,
			 const std::vector<double>& fluxes,
			 const std::vector<double>& normals);
			 

// Perturb the boundary using the shape derivative. Set maximum perturbation value
void PerturbMesh(CoordConn& MD, const std::vector<int>& bdnodes,
		 const std::vector<double>& dudn, const std::vector<double>& normals,
		 const double dh, const int run);

// Improve mesh
void ImproveMesh(CoordConn& MD);
  
int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh over an annulus
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);
  
  // Subdivide
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      ProjectInnerBdNodes(MD);
    }
  PlotTecCoordConn("MD.tec", MD);

  for(int run=0; run<200; ++run)
    {
      std::cout<<"\nRun: "<<run<<std::flush;
      
      // Set global coordinates
      Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
      Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

      // Create triangle elements over the domain & Laplacian operations over them
      std::vector<Element*> ElmArray(MD.elements);
      std::vector<Laplacian*> OpArray(MD.elements);
      ForceFieldValue<2> ConstFrc(ConstForceFunc);
      std::vector<int> Field({0});
      std::vector<BodyForceWork*> FrcArray(MD.elements);
      for(int e=0; e<MD.elements; ++e)
	{
	  const auto* conn = &MD.connectivity[3*e];
	  ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
	  ScalarMapAccess sma(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
	  OpArray[e] = new Laplacian(ElmArray[e], sma, 0);
	  FrcArray[e] = new BodyForceWork(ElmArray[e], ConstFrc, Field, sma);
	}
      StandardP12DMap L2GMap(ElmArray);

      // Create scalar field, initialize all values to zero
      ScalarMap UMap(MD.nodes);
      for(int n=0; n<MD.nodes; ++n)
	{ double val = 0.; UMap.Set(n, &val); }
      UMap.SetInitialized();

      // Get Dirichlet bcs & update solution
      std::vector<int> boundary;
      std::vector<double> bvalues;
      GetDirichletBCs(MD, boundary, bvalues);
      for(unsigned int i=0; i<boundary.size(); ++i)
	UMap.Set(boundary[i], &bvalues[i]);
      UMap.SetInitialized();
      std::fill(bvalues.begin(), bvalues.end(), 0.);
  
      // Assemblers
      StandardAssembler<Laplacian> AsmLap(OpArray, L2GMap);
      StandardAssembler<BodyForceWork> AsmFrc(FrcArray, L2GMap);
      std::vector<int> nz;
      AsmLap.CountNonzeros(nz);

      // Create PETSc data structures
      PetscData PD;
      PD.Initialize(nz);
      VecZeroEntries(PD.solutionVEC);
  
      // Assemble
      AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
      AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero
  
      // Set boundary conditions
      PD.SetDirichletBCs(boundary, bvalues);

      // Solve KU+F = 0
      PC pc;
      KSPGetPC(PD.kspSOLVER, &pc);
      PCSetType(pc, PCLU);
      PD.Solve();
  
      // Update the solution
      double* inc;
      VecScale(PD.solutionVEC, -1.);
      VecGetArray(PD.solutionVEC, &inc);
      for(int n=0; n<MD.nodes; ++n)
	UMap.Increment(n, &inc[n]);
      UMap.SetInitialized();
      VecRestoreArray(PD.solutionVEC, &inc);

      // Plot the solution
      //char filename[100];
      //sprintf(filename, "sol-%02d.tec", run);
      //PlotTecCoordConnWithNodalFields(filename, MD, &(UMap.Get()[0]), 1);
      //sprintf(filename, "uflux-%02d.dat", run);
      //PlotSolutionFluxes(filename, MD, ElmArray, L2GMap, &(UMap.Get()[0]));

      // Compute the L2 norm of the error
      double Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get()[0]));
      std::cout<<"\nError: "<<Err<<std::flush;

      // Recover boundary fluxes
      std::vector<int> bdnodes;
      std::vector<double> rFluxes, bdnormals;
      RecoverBoundaryFluxes(MD, UMap, AsmLap, AsmFrc, PD, bdnodes, rFluxes, bdnormals); // use 0-numbering for nodes
      char filename[100];
      sprintf(filename, "rflux-%03d.dat", run);
      PlotRecoveredFluxes(filename, MD, bdnodes, rFluxes, bdnormals);

      // Perturb the boundary using the shape derivative. Set maximum perturbation value
      double meshsize = GetMeshSize(MD);
      PerturbMesh(MD, bdnodes, rFluxes, bdnormals, 0.2*meshsize, run);
      //sprintf(filename, "pert-%02d.tec", run);
      //PlotTecCoordConn(filename, MD);

      // Relax the mesh
      ImproveMesh(MD);
      sprintf(filename, "MD-%03d.tec", run);
      PlotTecCoordConn(filename, MD);
      
      // Clean up
      for(auto& it:ElmArray) delete it;
      for(auto& it:OpArray) delete it;
      for(auto& it:FrcArray) delete it;
      PD.Destroy();
    }
  
  PetscFinalize();
}  


// Project nodes of INNER boundary
void ProjectInnerBdNodes(CoordConn& MD)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    if(r<R0+Reps)
	      for(int k=0; k<2; ++k)
		MD.coordinates[2*n+k] = X[k]*R0/r;
	  }
  return;
}

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);

  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  const double U0 = SolDetails::U0;
  const double U1 = SolDetails::U1;
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& n:bdnodes)
    {
      boundary.push_back( n );
      const double *X = &MD.coordinates[2*n];
      const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      if(rX<R0+Reps) bvalues.push_back( U0 ); // Inner boundary
      else bvalues.push_back( U1 ); // Outer boundary
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}



// Returns the exact solution
double SolDetails::GetExactSolution(const double* X)
{
  const double R1 = 0.7;
  const double f = fval;
  double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
  
  return ((f*(R0 - R1)*(R0 + R1) + 4.*(U0 - U1))*std::log(r) +
	  (-(f*std::pow(r,2)) + f*R1*R1 + 4.*U1)*std::log(R0) + 
	  (f*(r - R0)*(r + R0) - 4.*U0)*std::log(R1))/(4.*(std::log(R0) - std::log(R1)));
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* uvals)
{
  const int nElements = ElmArray.size();
  double Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  const double fval = SolDetails::GetExactSolution(&Qpts[2*q]);
	  double fnum = 0.;
	  for(int a=0; a<3; ++a)
	    fnum += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return std::sqrt(Err);
}


// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals)
{
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  std::fstream pfile;
  pfile.open(filename, std::ios::out);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>R0+Reps)
	    {
	      // Compute nabla u
	      double du[] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int i=0; i<2; ++i)
		  du[i] += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,i);

	      // Normal to this segment
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY =
		std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      const double normal[] = {(Y[1]-X[1])/lenXY, -(Y[0]-X[0])/lenXY};

	      // du/dn
	      double duhdn = du[0]*normal[0] + du[1]*normal[1];

	      // Exact solution at X and Yhere
	      double dudnX = SolDetails::GetOuterBdFlux(X);
	      double dudnY = SolDetails::GetOuterBdFlux(Y);

	      // Angles
	      double thetaX = std::atan2(X[1],X[0]);
	      double thetaY = std::atan2(Y[1],Y[0]);

	      // Write to file
	      pfile<<thetaX<<" "<<dudnX<<" "<<duhdn<<"\n"
		   <<thetaY<<" "<<dudnY<<" "<<duhdn<<"\n";
	      pfile.flush();
	    }
	}
  pfile.close();
  return;
}


struct BdStruct
{
  double mass;
  double normal[2];
};

// Recover boundary fluxes
void RecoverBoundaryFluxes(const CoordConn& MD, const ScalarMap& UMap,
			   StandardAssembler<Laplacian>& LapAsm,
			   StandardAssembler<BodyForceWork>& FrcAsm,
			   PetscData& PD,
			   std::vector<int>& bdnodes,
			   std::vector<double>& rFluxes,
			   std::vector<double>& bdnormals)
{
  // Useful alias
  auto& rVec = PD.resVEC;

  std::map<int, BdStruct> BdMap;
  
  // Assemble KU-F
  FrcAsm.Assemble(&UMap, rVec);
  LapAsm.Assemble(&UMap, rVec, false); // don't zero
  VecAssemblyBegin(rVec); VecAssemblyEnd(rVec);

  // Identify segments on the outer boundary and compute their lumped mass
  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  BdStruct bddata;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  // Is this a segment of the outer boundary
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>R0+Reps)
	    {
	      // Yes. Update the nodal masses
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));

	      // Boundary data for this face
	      bddata.mass = 0.5*lenXY;
	      bddata.normal[0] = (Y[1]-X[1])/lenXY;
	      bddata.normal[1] = -(Y[0]-X[0])/lenXY;
	      
	      // Update mass & normals for node 0
	      auto it0 = BdMap.find(n0);
	      if(it0==BdMap.end()) BdMap.insert(std::make_pair(n0, bddata));
	      else
		{
		  it0->second.mass += bddata.mass;
		  it0->second.normal[0] += bddata.normal[0];
		  it0->second.normal[1] += bddata.normal[1];
		}
		  
	      // Update mass & normals for node 1
	      auto it1 = BdMap.find(n1);
	      if(it1==BdMap.end()) BdMap.insert(std::make_pair(n1, bddata));
	      else
		{
		  it1->second.mass += bddata.mass;
		  it1->second.normal[0] += bddata.normal[0];
		  it1->second.normal[1] += bddata.normal[1];
		}
	    }
	}

  // Recover fluxes as R/m. Normalize vertex normals.
  bdnodes.clear(); bdnodes.reserve(BdMap.size());
  rFluxes.clear(); rFluxes.reserve(BdMap.size());
  bdnormals.clear(); bdnormals.reserve(2*BdMap.size());
  for(auto& it:BdMap)
    {
      const int n = it.first;
      const auto& data = it.second;
      double res;
      VecGetValues(rVec, 1, &n, &res);
      double norm = std::sqrt(data.normal[0]*data.normal[0]+data.normal[1]*data.normal[1]);

      // Outputs
      bdnodes.push_back(n);
      rFluxes.push_back( res/data.mass );
      bdnormals.push_back( data.normal[0]/norm );
      bdnormals.push_back( data.normal[1]/norm );
    }
  
  
  return;
}
		      
// Plot recovered fluxes in a file
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::vector<int>& bdnodes,
			 const std::vector<double>& fluxes,
			 const std::vector<double>& normals)
{
  // Plot the recovered boundary fluxes
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  const int nNodes = static_cast<int>(bdnodes.size());
  for(int i=0; i<nNodes; ++i)
    {
      const int n = bdnodes[i];
      const double* X = &MD.coordinates[2*n];
      const double q = SolDetails::GetOuterBdFlux(X);
      const double qh = fluxes[i];
      const double theta = std::atan2(X[1], X[0]);
      pfile<<theta<<" "<<q<<" "<<qh<<" "<<normals[2*i]<<" "<<normals[2*i+1]<<"\n";
      pfile.flush();
    }
  pfile.close();
  return;
}
  

// Returns the mesh size
double GetMeshSize(const CoordConn& MD)
{
  double hsum = 0.;
  int nedges = 0;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      {
	const int n0 = MD.connectivity[3*e+f%3]-1;
	const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	const double* X = &MD.coordinates[2*n0];
	const double* Y = &MD.coordinates[2*n1];
	hsum += (X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]);
	++nedges;
      }
  hsum /= static_cast<double>(nedges);
  return std::sqrt(hsum);
}


// Perturb the boundary using the shape derivative. Set maximum perturbation value
void PerturbMesh(CoordConn& MD, const std::vector<int>& bdnodes,
		 const std::vector<double>& dudn,
		 const std::vector<double>& normals,
		 const double dh, const int run)
{
  const double gval = SolDetails::gval;

  // Normalize the velocity based on the first norm
  static double vnorm = 0.;
  const int nNodes = static_cast<int>(bdnodes.size());
  double myvnorm = 0.;
  for(int i=0; i<nNodes; ++i)
    { double vel = std::abs(gval*gval-dudn[i]*dudn[i]);
      if(vel>myvnorm) myvnorm = vel; }
  
  if(run==0 || myvnorm>vnorm) vnorm = myvnorm;
  
  // Simply perturb all nodes.
  for(int i=0; i<nNodes; ++i)
    {
      const int n = bdnodes[i];
      const double g = dudn[i];
      const double* N = &normals[2*i];
      
      double vel = -(gval*gval-g*g)/vnorm;
      double *X = &MD.coordinates[2*n];
      for(int k=0; k<2; ++k)
	X[k] += dh*vel*N[k];
    }
  return;
}


// Improve mesh
void ImproveMesh(CoordConn& MD)
{
  // List of interior nodes
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  std::set<int> boundary;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  boundary.insert(MD.connectivity[3*e+(f+i)%3]-1);
  std::set<int> allnodes;
  for(int i=0; i<MD.nodes; ++i)
    allnodes.insert(i);
  std::vector<int> Ir;
  std::set_difference(allnodes.begin(), allnodes.end(),
		      boundary.begin(), boundary.end(),
		      std::back_inserter(Ir));
      
  // Mesh and dvr data structures
  msh::SeqCoordinates Coord(2, MD.coordinates);
  for(auto& i:MD.connectivity) --i;
  msh::StdTriConnectivity Conn(MD.connectivity);
  msh::StdTriMesh CC(Coord, Conn);
    
  // Setup dvr
  dvr::ValencyHint hint({10,10}); // Max number of verts, elms in a 1-ring
  dvr::OneRingData<decltype(CC)> RData(CC, Ir, hint);
  dvr::GeomTri2DQuality<decltype(CC)> Quality(CC);
  dvr::SolverSpecs SolSpecs({1,2,3}); //nthreads, nmaxima, nextrema
  //dvr::ReconstructiveMaxMinSolver<decltype(CC)> solver(CC, RData, SolSpecs);
  dvr::PartitionedMaxMinSolver<decltype(CC)> solver(CC, RData, SolSpecs);
  dvr::RelaxationDirGenerator rdir_gen;
  rdir_gen.f_rdir = dvr::CartesianDirections<2>;
  dvr::SeqMeshOptimizer TriOpt;

  
  // Improve mesh
  for(int iter=1; iter<=20; ++iter)
    { rdir_gen.params = &iter;
      TriOpt.Optimize(Ir, CC, Quality, solver, rdir_gen); }

  // Copy the relaxed mesh
  for(auto& i:MD.connectivity) ++i;
  for(auto& n:Ir)
    {
      const auto* X = CC.coordinates(n);
      for(int k=0; k<2; ++k)
	MD.coordinates[2*n+k] = X[k];
    }
  return;      
}
