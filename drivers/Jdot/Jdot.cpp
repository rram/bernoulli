// Sriramajayam

#include <msh_TriangleModule>
#include <dvr_TriangleModule>
#include <bp_Laplacian2DModule>
#include <ConstantForceField.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <omp.h>

// Helper struct for solution information
struct SolDetails
{
  std::vector<double> center;
  double R0, R1;
  double fval, gval, U0, U1; 
  double Jval;
  double L2Err;
  // Constructors
  inline SolDetails() {}
  inline SolDetails(const SolDetails& sd)
    :center(sd.center), R0(sd.R0), R1(sd.R1),
     fval(sd.fval), gval(sd.gval), U0(sd.U0),
     U1(sd.U1), Jval(sd.Jval), L2Err(sd.L2Err) {}
};

// Project inner and outer boundaries
void ProjectBoundaries(CoordConn& MD, const double Rin, const double Rout);
  
// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol,
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Returns the exact solution
double GetExactSolution(const double* X, const SolDetails& sol);

// Compute the L2 norm of the error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* uvals,
		      const SolDetails& sol);

// Compute J
double ComputeJ(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		const double* uvals, const SolDetails& sol);

// Compute the finite element solution
void Solve(const CoordConn& MD, SolDetails& sol);

// Improve a mesh
void ImproveMesh(CoordConn& MD);
  
int main(int argc, char** argv)
{
  omp_set_num_threads(1);

  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Details of the problem to solve
  SolDetails sol;
  sol.center = std::vector<double>({0.,0.});
  sol.R0 = 0.2; sol.R1 = 0.7;
  sol.fval = 4.;
  sol.gval = 2.0271851144019437;
  sol.U0 = 1.; sol.U1 = 0.;
  
  // Read the mesh to use
  CoordConn MD;
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  ReadTecplotFile((char*)"r=0.7.msh", MD);

  // Subdivide the mesh
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  
  // Project inner and outer boundaries
  ProjectBoundaries(MD, sol.R0, sol.R1);
  PlotTecCoordConn((char*)"MD.tec", MD);
  
  // Solve
  Solve(MD, sol);

  // Print solution details
  std::cout<<"\nUnperturbed: "
    //<<"\nL2err: "<<sol.L2Err
	   <<"\nJval: "<<sol.Jval<<std::flush;

  // perturb this mesh according to a given velocity and solve
  const double epsilon0 = 1./128.;
  const double epsilon = epsilon0/std::pow(2.,ndiv);
  for(int n=0; n<MD.nodes; ++n)
    {
      auto* X = &MD.coordinates[2*n];
      double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      double f = (r-sol.R0)/(sol.R1-sol.R0);
      double vel[2] = {f*X[0]/r, f*X[1]/r};
      for(int i=0; i<2; ++i)
	X[i] += epsilon*vel[i];
    }
  PlotTecCoordConn((char*)"pert.tec", MD);
  SolDetails pertsol(sol);
  pertsol.R1 += epsilon;
  Solve(MD, pertsol);
  
  // Print solution details
  std::cout<<"\n\nPerturbed: "
    //<<"\nL2err: "<<pertsol.L2Err
	   <<"\nJval: "<<pertsol.Jval
	   <<"\nJdot: "<<(pertsol.Jval-sol.Jval)/epsilon
	   <<std::flush;
  
  // Improve the perturbed mesh and recompute
  ImproveMesh(MD);
  SolDetails dvrpertsol(pertsol);
  PlotTecCoordConn((char*)"dvr.tec", MD);
  Solve(MD, dvrpertsol);

  // Print solution details
  std::cout<<"\n\nPerturbed (with improvement): "
    //<<"\nL2err: "<<dvrpertsol.L2Err
	   <<"\nJval: "<<dvrpertsol.Jval
	   <<"\nJdot: "<<(dvrpertsol.Jval-sol.Jval)/epsilon
	   <<std::flush;
  
  // Finalize PETSc
  PetscFinalize(); 
}


// Compute the finite element solution
void Solve(const CoordConn& MD, SolDetails& sol)
{
  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    ElmArray[e] = new P12DElement<1>(MD.connectivity[3*e], MD.connectivity[3*e+1], MD.connectivity[3*e+2]);

  // Local to global map
  StandardP12DMap L2GMap(ElmArray);

  // Create a scalar field, initialize to 0
  bp::ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();

  // Create a constant force field
  std::vector<double> fval({-sol.fval});
  ConstantForceField Frc(fval);

  // Create operations
  std::vector<bp::Laplacian*> OpArray(MD.elements);
  std::vector<BodyForceWork*> FrcOps(MD.elements);
  std::vector<int> Fields({0});
  for(int e=0; e<MD.elements; ++e)
    {
      bp::ScalarMapAccess SMAccess
	(std::vector<int>({MD.connectivity[3*e]-1,   MD.connectivity[3*e+1]-1, MD.connectivity[3*e+2]-1}));
      OpArray[e] = new bp::Laplacian(ElmArray[e], SMAccess, 0);
      FrcOps[e] = new BodyForceWork(ElmArray[e], Frc, Fields, SMAccess);
    }

  // Assemblers
  StandardAssembler<bp::Laplacian> AsmLap(OpArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(FrcOps, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);

  // Get Dirichlet bcs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, sol, boundary, bvalues);

  // Update the state using the boundary conditions
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);

  // Assemble laplacian and forcing contributions
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  AsmFrc.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT, false); // Don't zero.

  // BCs
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution and update state
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);

  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);

  CoordConn CC = MD;
  CC.spatial_dimension = 3;
  CC.coordinates.resize(3*MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    {
      for(int k=0; k<2; ++k)
	CC.coordinates[3*n+k] = MD.coordinates[2*n+k];
      CC.coordinates[3*n+2] = UMap.Get()[n];
    }
  PlotTecCoordConn((char*)"sol3D.tec", CC);

  // Compute the L2 norm of the error
  sol.L2Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get())[0], sol);

  // Compute J
  sol.Jval = ComputeJ(ElmArray, L2GMap, &(UMap.Get())[0], sol);

  // Clean up
  for(int e=0; e<MD.elements; ++e)
    {
      delete ElmArray[e];
      delete OpArray[e];
      delete FrcOps[e];
    }
  PD.Destroy();
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol,
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(auto e=0; e<MD.elements; ++e)
    for(auto f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(auto i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);

  // inner and outer boundaries
  const auto& R0 = sol.R0; const auto& U0 = sol.U0;
  const auto& R1 = sol.R1; const auto& U1 = sol.U1;
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      const double* X = &MD.coordinates[2*it];
      const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      if(r<0.5*(R0+R1))  bvalues.push_back( U0 );
      else bvalues.push_back( U1 );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
}


// Returns the exact solution
double GetExactSolution(const double* X, const SolDetails& sol)
{
  //return std::exp(X[0])*std::sin(X[1]);
  auto& R0 = sol.R0; auto& R1 = sol.R1;
  auto& center = sol.center;
  auto& U0 = sol.U0; auto& U1 = sol.U1;
  double f = sol.fval;
  double r = std::sqrt((X[0]-center[0])*(X[0]-center[0]) + (X[1]-center[1])*(X[1]-center[1]));
  
  return ((f*(R0 - R1)*(R0 + R1) + 4.*(U0 - U1))*std::log(r) +
	  (-(f*std::pow(r,2)) + f*R1*R1 + 4.*U1)*std::log(R0) + 
	  (f*(r - R0)*(r + R0) - 4.*U0)*std::log(R1))/(4.*(std::log(R0) - std::log(R1)));
}



// Compute the L2 norm of the error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* uvals,
		      const SolDetails& sol)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double L2Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nDof = ElmArray[e]->GetDof(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  double uex = GetExactSolution(&Qpts[2*q], sol);

	  // FE solution
	  double uh = 0.;
	  const auto& Shp = ElmArray[e]->GetShape(0);
	  for(int a=0; a<nDof; ++a)
	    uh += uvals[L2GMap.Map(0,a,e)]*Shp[q*nDof+a];

	  // Update the L2 error
	  L2Err += Qwts[q]*(uex-uh)*(uex-uh);
	}
    }
  return std::sqrt(L2Err);
}



// Compute J
double ComputeJ(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		const double* uvals, const SolDetails& sol)
{
  const int nElements = static_cast<int>(ElmArray.size());
  double Jval = 0.;
  const auto& fval = sol.fval;
  const auto& gval = sol.gval;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const int nDof = ElmArray[e]->GetDof(0);
      for(int q=0; q<nQuad; ++q)
	{
	  // Solution and its derivative at this quadrature point
	  double du[2] = {0.,0.};
	  double u = 0.;
	  for(int a=0; a<nDof; ++a)
	    {
	      const auto& ua = uvals[L2GMap.Map(0,a,e)];
	      u += ua*ElmArray[e]->GetShape(0,q,a);
	      for(int i=0; i<2; ++i)
		du[i] += ua*ElmArray[e]->GetDShape(0,q,a,i);
	    }
	  // Update J
	  Jval += Qwts[q]*(du[0]*du[0]+du[1]*du[1]-2.*fval*u+gval*gval);
	}
    }
  return Jval;
}


// Project inner and outer boundaries
void ProjectBoundaries(CoordConn& MD, const double Rin, const double Rout)
{
    // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(auto e=0; e<MD.elements; ++e)
    for(auto f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(auto i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);

  for(auto& n:bdnodes)
    {
      double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
      double r = std::sqrt(X[0]*X[0] + X[1]*X[1]);
      double R = (r<0.5*(Rin+Rout)) ? Rin : Rout;
      for(int k=0; k<2; ++k)
	MD.coordinates[2*n+k] = R*X[k]/r;
    }
  return;
}


// Improve a mesh
void ImproveMesh(CoordConn& MD)
{
  msh::SeqCoordinates Coord(2, MD.coordinates);
  for(auto& i:MD.connectivity) --i;
  msh::StdTriConnectivity Conn(MD.connectivity);
  msh::StdTriMesh CC(Coord, Conn);

  // List of interior nodes
  const int nElements = CC.GetNumElements();
  const int nFaces = 3;
  const int nfnodes = Conn.GetNumNodesPerFace();
  std::set<int> allnodes;
  std::set<int> bdnodes;
  for(int e=0; e<nElements; ++e)
    {
      const auto* elmconn = CC.connectivity(e);
      const auto* nbs = CC.GetElementNeighbors(e);
      for(int a=0; a<3; ++a)
	allnodes.insert(elmconn[a]);
      for(int f=0; f<nFaces; ++f)
	{
	  const auto fnodes = Conn.GetLocalFaceNodes(f);
	  if(nbs[2*f]==-1)
	    for(int a=0; a<nfnodes; ++a)
	      bdnodes.insert(elmconn[fnodes[a]]);
	}
    }

  // vertices to perturb
  std::vector<int> pert({});
  std::set_difference(allnodes.begin(), allnodes.end(),
		      bdnodes.begin(), bdnodes.end(),
		      std::back_inserter(pert));//, pert.begin()));

  // Setup dvr
  dvr::ValencyHint hint({10,10});
  dvr::OneRingData<decltype(CC)> RData(CC, pert, hint);
  dvr::GeomTri2DQuality<decltype(CC)> Quality(CC);
  dvr::SolverSpecs SolSpecs({1,2,3});
  dvr::ReconstructiveMaxMinSolver<decltype(CC)> solver(CC, RData, SolSpecs);
  dvr::RelaxationDirGenerator rdir_gen;
  rdir_gen.f_rdir = dvr::CartesianDirections<2>;
  dvr::SeqMeshOptimizer TriOpt;

  // Optimize
  for(int iter=1; iter<=10; ++iter)
    {
      rdir_gen.params = &iter;
      TriOpt.Optimize(pert, CC, Quality, solver, rdir_gen);
    }

  // Copy the relaxed mesh
  for(auto& i:MD.connectivity) ++i;
  for(auto& n:pert)
    {
      const auto* X = CC.coordinates(n);
      for(int k=0; k<2; ++k)
	MD.coordinates[2*n+k] = X[k];
    }
  return;
}
