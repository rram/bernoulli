// Sriramajayam

#include <mx_MaxEntSSD.h>
#include <geom_ImplicitMaxEntManifold.h>
#include <msh_TriangleModule>
#include <dvr_TriangleModule>
#include <um_TriangleModule>
#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <random>

// Aliases
using MaxEntCurveFit = mx::MaxEntSSD<2>;
using MaxEntCurve = geom::ImplicitMaxEntManifold<mx::MaxEnt2D>;


// Namespaces intentionally omitted for clarity

// Create a mesh of equilateral triangles over a hexagon with unit size.
// Its vertices will be the max-ent nodes
// connectivity: numbered from 1
void GetEquilateralMesh(std::vector<double>& coordinates,
			std::vector<int>& connectivity);

// Computes a smoothed signed distance for a given set of points+normals
// points: List of 2D points lying on the curve phi = 0
// normals: Unit outward normal for "points"
// MD: Mesh whose nodes  define the nodeset for Max-ent functions.
// nThreads: Number of threads to use for the calculation
// iter Iteration number for file numbering
// Fit: Output, Computed Max-ent functions and dofs for phi.
void GetSmoothedSignedDistance(const std::vector<double>& points,
			       const std::vector<double>& normals,
			       const CoordConn& MD,
			       const int nThreads,
			       MaxEntCurveFit*& Fit, int iter);

// Retrieve a mesh conforming to "Geom"
// BG Background mesh
// Geom Geometry object
// MD Output, computed conforming mesh
void GetConformingMesh(CoordConn& BG, const MaxEntCurve& Geom, CoordConn& MD);

// Returns the mesh size
double GetMeshSize(const CoordConn& MD);

// Recover boundary fluxes
// MD Mesh, UMap: Solution
// AsmLap: Assembler for laplacian, AsmFrc: Assembler for forcing term
// PD PetscData object
// bdnodes: output, list of boundary nodees
// fluxes: Values of boundary fluxes
// normals: Normal to the boundary at bdnodes
void RecoverBoundaryFluxes(const CoordConn& MD, const bp::ScalarMap& UMap,
			   StandardAssembler<bp::Laplacian>& LapAsm,
			   StandardAssembler<BodyForceWork>& FrcAsm,
			   PetscData& PD,
			   std::vector<int>& bdnodes, std::vector<double>& fluxes);

// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals);

// Plot recovered fluxes in a file
// MD Mesh object
// bdnodes List of boundary nodes
// fluxes Values of fluxes at the boundary nodes
// normals List of normals at the boundary nodes
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::vector<int>& bdnodes,
			 const std::vector<double>& fluxes);

// Perturb the boundary using the shape derivative. Set maximum perturbation value
// MD mesh object,
// bdnodes Nodes to be perturbed
// dudn Normal fluxes at the boundary nodes
// Geom Geometry object to compute normals along the moving boundary
// dh Upper bound on magnitude of vertex perturbations
void PerturbMesh(CoordConn& MD, const std::vector<int>& bdnodes,
		 const std::vector<double>& dudn, const MaxEntCurve& Geom,
		 const double dh);

// Update the set of outer boundary nodes and normals
// MD Mesh object
// bdpoints output, Coordinates of boundary points
// bdnormals output, Normals at the boundary points
void GetOuterBoundary(const CoordConn& MD, std::vector<double>& bdpoints, std::vector<double>& bdnormals);

// Plot the computed set of outer boundary points and normals
void PlotBoundary(const char* filename, const std::vector<double>& points, const std::vector<double>& normals);

// Solution details
struct SolDetails
{
  static constexpr double R0 = 0.2; // Fixed inner radius
  static constexpr double Reps = 0.075; // tolerance for distinguishing inner & outer boundaries
  static constexpr double U0 = 1.; // Dirichlet bcs on inner boundary
  static constexpr double U1 = 0.; // Dirichlet bcs on outer boundary
  static constexpr double fval = 4.; // Forcing term in the state problem
  static constexpr double gval = -2.0271851144019437; // Target boundary flux
};

// Constant force field
bool ConstForceFunc(const std::vector<double> u, const std::vector<double> X,
		    std::vector<double>* f, std::vector<double>* df, void* params)
{ if(f->size()==0) f->resize(1);
  (*f)[0] = -1.*SolDetails::fval; return true; } // -ve sign is a hack


// Assign dirichlet BCs: U0 on inner boundary, U1 on outer boundary
void GetDirichletBCs(const CoordConn& MD, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  omp_set_num_threads(1);

  // Create a mesh of equilateral triangles over a hexagon with unit size.
  // Its vertices will be the max-ent nodes
  // connectivity: numbered from 1
  CoordConn EqMD;
  GetEquilateralMesh(EqMD.coordinates, EqMD.connectivity);
  EqMD.nodes_element = 3;
  EqMD.spatial_dimension = 2;
  EqMD.nodes = static_cast<int>(EqMD.coordinates.size()/2);
  EqMD.elements = static_cast<int>(EqMD.connectivity.size()/3);

  // Background mesh for triangulation
  CoordConn BG = EqMD;
  for(int i=0; i<3; ++i)
    SubdivideTriangles(BG.connectivity, BG.coordinates, BG.nodes, BG.elements);
  for(auto& x:BG.coordinates) x*=0.95;
  PlotTecCoordConn((char*)"BG.tec", BG);
  const double meshsize = GetMeshSize(BG);
  
  // initial guess for the solution.
  // Inner boundary
  std::vector<double> inBdPoints, inBdNormals;
  const int ninBdPoints = 40;
  const double PI = 4.*std::atan(1.);
  for(int n=0; n<ninBdPoints; ++n)
    {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(ninBdPoints);
      inBdPoints.push_back( SolDetails::R0*std::cos(theta) );
      inBdPoints.push_back( SolDetails::R0*std::sin(theta) );
      inBdNormals.push_back( -std::cos(theta) );
      inBdNormals.push_back( -std::sin(theta) );
    }
  // outer boundary
  std::vector<double> outBdPoints, outBdNormals;
  const int noutBdPoints = 20;
  for(int n=0; n<noutBdPoints; ++n)
    {
      double theta = 2.*PI*static_cast<double>(n)/static_cast<double>(noutBdPoints);
      outBdPoints.push_back( 0.5*std::cos(theta) );
      outBdPoints.push_back( 0.7*std::sin(theta) );
      outBdNormals.push_back( std::cos(theta) );
      outBdNormals.push_back( std::sin(theta) );
    }
  

  // Solution iterations
  char filename[100];
  for(int iter=0; iter<50; ++iter)
    {
      std::cout<<"\nIteration "<<iter<<std::flush;
      
      // Plot the outer boundary points and normals
      sprintf(filename, "bd-%03d.dat", iter);
      PlotBoundary(filename, outBdPoints, outBdNormals);
      
      // Collate all boundary points and normals
      std::vector<double> bdpoints, bdnormals;
      for(auto& i:inBdPoints) bdpoints.push_back(i);
      for(auto& i:outBdPoints) bdpoints.push_back(i);
      for(auto& i:inBdNormals) bdnormals.push_back(i);
      for(auto& i:outBdNormals) bdnormals.push_back(i);
      
      // Fit smoothed signed distance to boundary data
      MaxEntCurveFit* SDFit = nullptr;
      GetSmoothedSignedDistance(bdpoints, bdnormals, EqMD, 1, SDFit, iter);
      assert(SDFit!=nullptr);

      // Create curve for the zero level set
      geom::NLSolverParams nlparams({1.e-6, 1.e-6, 25}); //ftol, ttol, max_iter
      MaxEntCurve Geom(SDFit->GetMaxEntFunctions(),
		       SDFit->GetDofs(),
		       nlparams, 1); // 1 thread

      // Retrieve a mesh conforming to "Geom"
      CoordConn MD;
      GetConformingMesh(BG, Geom, MD);
      sprintf(filename, "MD-%03d.tec", iter);
      PlotTecCoordConn(filename, MD);
      
      // Create elements and operations
      Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
      Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
      std::vector<Element*> ElmArray(MD.elements);
      std::vector<bp::Laplacian*> LapArray(MD.elements);
      bp::ForceFieldValue<2> Frc(ConstForceFunc);
      std::vector<int> Field({0});
      std::vector<BodyForceWork*> FrcArray(MD.elements);
      for(int e=0; e<MD.elements; ++e)
	{
	  const auto* conn = &MD.connectivity[3*e];
	  ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
	  bp::ScalarMapAccess sma(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
	  LapArray[e] = new bp::Laplacian(ElmArray[e], sma, 0);
	  FrcArray[e] = new BodyForceWork(ElmArray[e], Frc, Field, sma);
	}
      StandardP12DMap L2GMap(ElmArray);


      // Create scalar field, initialize all values to zero
      bp::ScalarMap UMap(MD.nodes);
      for(int n=0; n<MD.nodes; ++n)
	{ double val = 0.; UMap.Set(n, &val); }
      UMap.SetInitialized();
      
      // Get Dirichlet bcs & update solution
      std::vector<int> boundary;
      std::vector<double> bvalues;
      GetDirichletBCs(MD, boundary, bvalues);
      for(unsigned int i=0; i<boundary.size(); ++i)
	UMap.Set(boundary[i], &bvalues[i]);
      UMap.SetInitialized();
      std::fill(bvalues.begin(), bvalues.end(), 0.);

      // Assemblers
      StandardAssembler<bp::Laplacian> AsmLap(LapArray, L2GMap);
      StandardAssembler<BodyForceWork> AsmFrc(FrcArray, L2GMap);
      std::vector<int> nz;
      AsmLap.CountNonzeros(nz);

      // Create PETSc data structures
      PetscData PD;
      PD.Initialize(nz);
      VecZeroEntries(PD.solutionVEC);
  
      // Assemble
      AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
      AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero
  
      // Set boundary conditions
      PD.SetDirichletBCs(boundary, bvalues);

      // Solve KU+F = 0
      PC pc;
      KSPGetPC(PD.kspSOLVER, &pc);
      PCSetType(pc, PCLU);
      PD.Solve();
  
      // Update the solution
      double* inc;
      VecScale(PD.solutionVEC, -1.);
      VecGetArray(PD.solutionVEC, &inc);
      for(int n=0; n<MD.nodes; ++n)
	UMap.Increment(n, &inc[n]);
      UMap.SetInitialized();
      VecRestoreArray(PD.solutionVEC, &inc);
      sprintf(filename, "sol-%03d.tec", iter);
      PlotTecCoordConnWithNodalFields(filename, MD, &UMap.Get()[0], 1);

      // Plot computed fluxes on the outer boundary
      sprintf(filename, "flux-%03d.dat", iter);
      PlotSolutionFluxes(filename, MD, ElmArray, L2GMap, &UMap.Get()[0]);
      
      // Recover boundary fluxes
      std::vector<int> outbdnodes;
      std::vector<double> rFluxes;
      RecoverBoundaryFluxes(MD, UMap, AsmLap, AsmFrc, PD, outbdnodes, rFluxes);

      // Plot the recovered fluxes
      sprintf(filename, "rflux-%03d.dat", iter);
      PlotRecoveredFluxes(filename, MD, outbdnodes, rFluxes);

      // Perturb the mesh based on the computed fluxes
      PerturbMesh(MD, outbdnodes, rFluxes, Geom, 0.2*meshsize);
      PlotTecCoordConn((char*)"pert.tec", MD);

      // Update the set of outer boundary nodes and normals
      outBdPoints.clear();
      outBdNormals.clear();
      GetOuterBoundary(MD, outBdPoints, outBdNormals);
      assert(outBdPoints.size()==2*outbdnodes.size() && outBdNormals.size()==2*outbdnodes.size());
      
      // Clean up
      PD.Destroy();
      for(auto& it:ElmArray) delete it;
      for(auto& it:LapArray) delete it;
      for(auto& it:FrcArray) delete it;
    }

  // Finalize PETSc
  PetscFinalize();
}


// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coord,
			std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( std::cos(theta) );
      coord.push_back( std::sin(theta) );
    }

  // Subdivide
  for(int i=0; i<2; i++)
    msh::SubdivideTriangles(2, conn, coord);

  // Renumber connectivity from 1
  for(auto& i:conn) ++i;
  return;
}


// Computes a smoothed signed distance for a given set of points+normals
// points: List of 2D points lying on the curve phi = 0
// normals: Unit outward normal for "points"
// MD: Mesh whose nodes  define the nodeset for Max-ent functions.
// nThreads: Number of threads to use for the calculation
// iter Iteration number for file numbering
// Fit: Output, Computed Max-ent functions and dofs for phi.
void GetSmoothedSignedDistance(const std::vector<double>& points,
			       const std::vector<double>& normals,
			       const CoordConn& CC,
			       const int nThreads,
			       MaxEntCurveFit*& Fit, int iter)
{
  // Nodes for max-ent functions: CC.coordinates

  // Subdivide mesh for integration purposes
  CoordConn MD = CC;
  for(int i=0; i<2; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);

  // Create elements
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
    }

  // Function fitting parameters
  mx::MaxEntSSDParams params;
  params.Tol0 = 1.e-6;
  params.TolNR = 1.e-6;
  params.nnb = 1;
  params.gamma  = 0.8;
  params.alpha = 0.01;
  params.nodeset = &CC.coordinates;
  params.ElmArray = &ElmArray;
  params.PC = &points;
  params.normals = &normals;
  params.nMaxThreads = nThreads;
  params.verbose = false;

  // Fit function
  Fit = new MaxEntCurveFit(params);

  // Plot the function. Shrink the mesh a little to avoid the boundary
  for(int n=0; n<MD.nodes; ++n)
    for(int k=0; k<2; ++k)
      MD.coordinates[2*n+k] *= 0.95;
  char filename[100];
  sprintf(filename, "ssd-%03d.tec", iter);
  Fit->PlotTec(filename, MD);

  // Clean up
  for(auto& e:ElmArray) delete e;
  return;
}

// Retrieve a mesh conforming to "Geom"
// BG Background mesh
// Geom Geometry object
// MD Output, computed conforming mesh
void GetConformingMesh(CoordConn& BGmesh, const MaxEntCurve& Geom, CoordConn& MD)
{
  // Convert BGmesh to the format required in msh::
  msh::SeqCoordinates Coord(2, BGmesh.coordinates);
  for(auto& i:BGmesh.connectivity) --i;
  msh::StdTriConnectivity Conn(BGmesh.connectivity);
  for(auto& i:BGmesh.connectivity) ++i;
  msh::StdTriMesh BG(Coord, Conn);

  // Workspace for the background mesh
  um::SDWorkspace<decltype(BG), decltype(Geom)> BGws(BG, Geom);

  // Evaluate implicit function at all the nodes. Signed distances are not required
  const int nbgnodes = BG.GetNumNodes();
  for(int i=0; i<nbgnodes; ++i)
    {
      const auto* X = BG.coordinates(i);
      um::SignedDistance sd;
      Geom.GetImplicitFunction(X, sd.value);
      sd.sign = (sd.value>0.) ? um::SDSignature::Plus : um::SDSignature::Minus;
      BGws.Set(i, sd);
    }

  // Create domain triangulator
  um::DomainTriangulator<decltype(BG), decltype(Geom)> Mshr(BGws);

  // get the working mesh
  auto& WM = Mshr.GetWorkingMesh();

  // Positive verts
  const auto& PosVerts = Mshr.GetPositiveVertices();

  // Interior nodes to relax
  std::vector<int> wmnodes;
  WM.GetNodes(wmnodes);
  std::sort(wmnodes.begin(), wmnodes.end());
  std::vector<int> Ir({});
  std::set_difference(wmnodes.begin(), wmnodes.end(), PosVerts.begin(), PosVerts.end(),
		      std::back_inserter(Ir));

  // 1-ring data for the working mesh
  dvr::ValencyHint vhint({6,6});
  dvr::OneRingData<decltype(WM)> RD(WM, wmnodes, vhint);

  // Quality metric
  dvr::GeomTri2DQuality<decltype(WM)> Quality(WM);

  // Max-min solver for relaxing vertices
  dvr::SolverSpecs solspecs({1,2,3}); // nThreads, nextrema, nintersections
  dvr::ReconstructiveMaxMinSolver<decltype(WM)> mmsolver(WM, RD, solspecs);

  // Vertex optimizer
  dvr::SeqMeshOptimizer Opt;

  // Relaxation direction generatpr
  dvr::RelaxationDirGenerator rdir_gen;
  rdir_gen.f_rdir = dvr::CartesianDirections<2>;

  // Project and relax in steps
  const int nSteps = 5;
  const int nIters = 8;
  for(int step=1; step<=nSteps; ++step)
    {
      // Project boundary vertices
      const double alpha = static_cast<double>(step)/static_cast<double>(nSteps);
      Mshr.Project(alpha);

      // Relax inner vertices
      for(int iter=0; iter<nIters; ++iter)
	{
	  rdir_gen.params = &iter;
	  Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen);
	}
    }

  // Plot the working mesh
  um::PlotTecWorkingMesh((char*)"wm.tec", WM, true); // Independent numbering

  // Mesh to be returned
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"wm.tec", MD);

  // Done
  return;
}


// Returns the mesh size
double GetMeshSize(const CoordConn& MD)
{
  double hsum = 0.;
  int nedges = 0;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      {
	const int n0 = MD.connectivity[3*e+f%3]-1;
	const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	const double* X = &MD.coordinates[2*n0];
	const double* Y = &MD.coordinates[2*n1];
	hsum += (X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]);
	++nedges;
      }
  hsum /= static_cast<double>(nedges);
  return std::sqrt(hsum);
}



// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);

  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  const double U0 = SolDetails::U0;
  const double U1 = SolDetails::U1;
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& n:bdnodes)
    {
      boundary.push_back( n );
      const double *X = &MD.coordinates[2*n];
      const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
      if(rX<R0+Reps) bvalues.push_back( U0 ); // Inner boundary
      else bvalues.push_back( U1 ); // Outer boundary
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}


// Recover boundary fluxes
void RecoverBoundaryFluxes(const CoordConn& MD, const bp::ScalarMap& UMap,
			   StandardAssembler<bp::Laplacian>& LapAsm,
			   StandardAssembler<BodyForceWork>& FrcAsm,
			   PetscData& PD,
			   std::vector<int>& bdnodes,
			   std::vector<double>& rFluxes)
{
  // Useful alias
  auto& rVec = PD.resVEC;

  std::map<int, double> BdMap;
  
  // Assemble KU-F
  FrcAsm.Assemble(&UMap, rVec);
  LapAsm.Assemble(&UMap, rVec, false); // don't zero
  VecAssemblyBegin(rVec); VecAssemblyEnd(rVec);

  // Identify segments on the outer boundary and compute their lumped mass
  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  // Is this a segment of the outer boundary
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>R0+Reps)
	    {
	      // Yes. Update the nodal masses
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));

	      // Boundary data for this face
	      double mass = 0.5*lenXY;
	      
	      // Update mass for node 0
	      auto it0 = BdMap.find(n0);
	      if(it0==BdMap.end()) BdMap.insert(std::make_pair(n0, mass));
	      else
		{ it0->second += mass; }
	      
	      // Update mass & normals for node 1
	      auto it1 = BdMap.find(n1);
	      if(it1==BdMap.end()) BdMap.insert(std::make_pair(n1, mass));
	      else
		{ it1->second += mass; }
	    }
	}

  // Recover fluxes as R/m. Normalize vertex normals.
  bdnodes.clear(); bdnodes.reserve(BdMap.size());
  rFluxes.clear(); rFluxes.reserve(BdMap.size());
  for(auto& it:BdMap)
    {
      const int n = it.first;
      const auto& mass = it.second;
      double res;
      VecGetValues(rVec, 1, &n, &res);
      
      // Outputs
      bdnodes.push_back(n);
      rFluxes.push_back( res/mass );
    }
  
  
  return;
}


// Plot recovered fluxes in a file
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::vector<int>& bdnodes,
			 const std::vector<double>& fluxes)
{
  // Plot the recovered boundary fluxes
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  const int nNodes = static_cast<int>(bdnodes.size());
  for(int i=0; i<nNodes; ++i)
    {
      const int n = bdnodes[i];
      const double* X = &MD.coordinates[2*n];
      const double q = SolDetails::gval;
      const double qh = fluxes[i];
      const double theta = std::atan2(X[1], X[0]);
      pfile<<theta<<" "<<q<<" "<<qh<<"\n";
      pfile.flush();
    }
  pfile.close();
  return;
}


// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals)
{
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double R0 = SolDetails::R0;
  const double Reps = SolDetails::Reps;
  std::fstream pfile;
  pfile.open(filename, std::ios::out);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>R0+Reps)
	    {
	      // Compute nabla u
	      double du[] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int i=0; i<2; ++i)
		  du[i] += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,i);

	      // Normal to this segment
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY =
		std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      const double normal[] = {(Y[1]-X[1])/lenXY, -(Y[0]-X[0])/lenXY};

	      // du/dn
	      double duhdn = du[0]*normal[0] + du[1]*normal[1];

	      // Angles
	      double thetaX = std::atan2(X[1],X[0]);
	      double thetaY = std::atan2(Y[1],Y[0]);

	      // Write to file
	      pfile<<thetaX<<" "<<duhdn<<"\n"
		   <<thetaY<<" "<<duhdn<<"\n";
	      pfile.flush();
	    }
	}
  pfile.close();
  return;
}


// Perturb the boundary using the shape derivative. Set maximum perturbation value
// MD mesh object,
// bdnodes Nodes to be perturbed
// dudn Normal fluxes at the boundary nodes
// Geom Geometry object to compute the unit normal at a node
// dh Upper bound on magnitude of vertex perturbations
void PerturbMesh(CoordConn& MD, const std::vector<int>& bdnodes,
		 const std::vector<double>& dudn, const MaxEntCurve& Geom,
		 const double dh)
{
  const double gval = SolDetails::gval;

  // Normalize the velocity based on the first norm
  static double vnorm = 0.;
  static int iter = 0;
  const int nbdNodes = static_cast<int>(bdnodes.size());
  double myvnorm = 0.;
  for(int i=0; i<nbdNodes; ++i)
    { double vel = std::abs(gval*gval-dudn[i]*dudn[i]);
      if(vel>myvnorm) myvnorm = vel; }
  if(iter==0 || myvnorm>vnorm) vnorm = myvnorm;

  // Perturb boundary nodes
  for(int i=0; i<nbdNodes; ++i)
    {
      // Velocity of this node
      const int n = bdnodes[i];
      const double g = dudn[i];
      double vel = -(gval*gval-g*g)/vnorm;

      // Direction along the normal
      double* X = &MD.coordinates[2*n];
      double normal[2], sdval;
      Geom.GetSignedDistance(X, sdval, normal);
      
      // Perturb this node
      for(int k=0; k<2; ++k)
	X[k] += dh*vel*normal[k];
    }
  return;
}


// Update the set of outer boundary nodes and normals
// MD Mesh object
// bdpoints output, Coordinates of boundary points
// bdnormals output, Normals at the boundary points
void GetOuterBoundary(const CoordConn& MD, std::vector<double>& bdpoints, std::vector<double>& bdnormals)
{
  // Map of normals for the outer boundary
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  std::map<int, std::array<double,2>> normalMap;
  std::array<double,2> N;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>SolDetails::R0+SolDetails::Reps)
	    {
	      // This edge is on the outer boundary
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));

	      // normal to this edge
	      N[0] =  (Y[1]-X[1])/lenXY;
	      N[1] = -(Y[0]-X[0])/lenXY;

	      // Update normal for node n0
	      auto it0 = normalMap.find(n0);
	      if(it0==normalMap.end()) { normalMap.insert(std::make_pair(n0, N)); }
	      else {it0->second[0] += N[0]; it0->second[1] += N[1]; }

	      // Update normal for node n1
	      auto it1 = normalMap.find(n1);
	      if(it1==normalMap.end()) { normalMap.insert(std::make_pair(n1, N)); }
	      else {it1->second[0] += N[0]; it1->second[1] += N[1]; }
	    }
	}

  // Set boundary points and normals
  bdpoints.clear();
  bdnormals.clear();
  for(auto& it:normalMap)
    {
      const auto& n = it.first;
      const auto* X = &MD.coordinates[2*n];
      const auto& N = it.second;
      const double len = std::sqrt(N[0]*N[0] + N[1]*N[1]);
      for(int k=0; k<2; ++k)
	{
	  bdpoints.push_back(X[k]);
	  bdnormals.push_back(N[k]/len);
	}
    }
  return;
}
	
	 

// Plot the computed set of outer boundary normals
void PlotBoundary(const char* filename, const std::vector<double>& points, const std::vector<double>& normals)
{
  std::fstream pfile;
  pfile.open(filename, std::ios::out);
  assert(pfile.good());
  const int nNodes = static_cast<int>(points.size()/2);
  for(int n=0; n<nNodes; ++n)
    {
      const double theta = std::atan2(points[2*n+1], points[2*n]);
      pfile<<points[2*n]<<" "<<points[2*n+1]<<" "<<theta<<" "<<normals[2*n]<<" "<<normals[2*n+1]<<"\n";
    }
  pfile.flush();
  pfile.close();
}
