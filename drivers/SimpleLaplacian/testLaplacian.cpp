// Sriramajayam

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <cassert>

using namespace bp;

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Returns an equilateral mesh
void GetMesh(CoordConn& MD);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* sol);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh
  CoordConn MD;
  GetMesh(MD);
  PlotTecCoordConn("MD.tec", MD);

  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements and operations
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e]; 
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      OpArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
    }
  
  // Local to global map
  StandardP12DMap L2GMap(ElmArray);

  // Create scalar field, initialize all values to zero
  ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();
  
  // Create assembler
  StandardAssembler<Laplacian> Asm(OpArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);

  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Get boundary conditions
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, boundary, bvalues);

  // Boundary conditions
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Assemble
  Asm.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  
  // Set boundary conditions
  PD.SetDirichletBCs(boundary, bvalues);
  
  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Update the solution
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);
  
  // Compute the L2 norm of the error
  double Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get()[0]));
  std::cout<<"\nError: "<<Err<<std::flush;

  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:OpArray) delete it;
  PD.Destroy();
  PetscFinalize();
}  


void GetMesh(CoordConn& MD)
{
  MD.coordinates.reserve(7*2);
  MD.coordinates.push_back(0.);
  MD.coordinates.push_back(0.);
  for(auto i=0; i<6; ++i)
    {
      MD.coordinates.push_back(cos(double(i)*M_PI/3.));
      MD.coordinates.push_back(sin(double(i)*M_PI/3.));
    }
  std::vector<int> conn({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.connectivity = std::move(conn);
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 7;
  MD.elements = 6;
  int ndiv = 1;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
}


// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      const auto& x = MD.coordinates[2*it];
      const auto& y = MD.coordinates[2*it+1];
      boundary.push_back( it );
      bvalues.push_back( exp(x)*sin(y) );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* sol)
{
  const int nElements = ElmArray.size();
  double Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  const double fval = exp(Qpts[2*q])*sin(Qpts[2*q+1]);
	  double fnum = 0.;
	  for(int a=0; a<3; ++a)
	    fnum += sol[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return std::sqrt(Err);
}
