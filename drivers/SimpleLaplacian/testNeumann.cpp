// Sriramajayam

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <BodyForceWork.h>
#include <ConstantForceField.h>


using namespace bp;

// Solve laplace equation on an annular domain (R0, R1).
// Exact solution: exp(x) sin(y)
// Dirichlet bcs on R0, Neumann bcs on R1.

// Helper struct for solution information
struct SolDetails
{
  static constexpr double R0 = 0.2;
  static constexpr double R1 = 0.7;
  // Returns the exact solution
  static double GetExactSolution(const double* X)
  { return std::exp(X[0])*std::sin(X[1]); }
  // Returns the normal derivative of the solution along the outer boundary
  static bool GetOuterBdFlux(const std::vector<double> u, const std::vector<double> X,
			     std::vector<double>* F, std::vector<double>* dF, void* params)
  { if(F->size()>1) F->resize(1);
    (*F)[0] = std::exp(X[0])*(X[0]*std::sin(X[1])+X[1]*std::cos(X[1]))/R1;
    return true; }
};

// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD, const SolDetails& sol);

// Enumerate outer boundary segments (r>R)
void GetNeumannSegments(const CoordConn& MD, const SolDetails& sol, 
			std::vector<int>& bdsegments);

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray, const LocalToGlobalMap& L2GMap,
		      const SolDetails& sol, const double* uvals);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // This problem's details
  SolDetails sol;

  // Read the mesh over an annulus
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);

  // Subdivide
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      ProjectBdNodes(MD, sol);
    }
  PlotTecCoordConn("MD.tec", MD);

  // Neumann boundary segments
  std::vector<int> neumannbdsegments;
  GetNeumannSegments(MD, sol, neumannbdsegments);
  const int nbdseg = static_cast<int>(neumannbdsegments.size()/2);
  assert(nbdseg>0 && nbdseg<MD.elements);
  
  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create triangle elements over the domain & Laplacian operations over them
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      OpArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
    }
  StandardP12DMap L2GMap(ElmArray);
  
  // Create edge elements over the Neumann boundary & forcing operations over them
  std::vector<Element*> BdElmArray(nbdseg);
  ForceFieldValue<2> Frc(sol.GetOuterBdFlux);
  std::vector<int> Fields({0});
  std::vector<BodyForceWork*> NeuOpArray(nbdseg);
  for(int e=0; e<nbdseg; ++e)
    {
      const auto* conn = &neumannbdsegments[2*e];
      BdElmArray[e] = new P12DSegment<1>(conn[0], conn[1]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1}));
      NeuOpArray[e] = new BodyForceWork(BdElmArray[e], Frc, Fields, SMAccess);
    }
  StandardP12DSegmentMap BdL2GMap(BdElmArray);

  // Create scalar field, initialize all values to zero
  ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();

  // Get Dirichlet bcs & update solution
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, sol, boundary, bvalues);
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Assemblers
  StandardAssembler<Laplacian> AsmLap(OpArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(NeuOpArray, BdL2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);
  
  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Assemble
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  MatScale(PD.stiffnessMAT, -1.);
  VecScale(PD.resVEC, -1.);
  AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero residual
  
  // Set boundary conditions
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Update the solution
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);
  
  // Compute the L2 norm of the error
  double Err = ComputeL2Error(ElmArray, L2GMap, sol, &(UMap.Get()[0]));
  std::cout<<"\nError: "<<Err<<std::flush;
    
  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:OpArray) delete it;
  for(auto& it:BdElmArray) delete it;
  for(auto& it:NeuOpArray) delete it;
  PD.Destroy();
  PetscFinalize();
}  


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD, const SolDetails& sol)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = sol.R0;
  const double R1 = sol.R1;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    const double R = (r<0.5*(R0+R1)) ? R0 : R1;
	    for(int k=0; k<2; ++k)
	      MD.coordinates[2*n+k] = X[k]*R/r;
	  }
  return;
}


// Neumann boundary segments
void GetNeumannSegments(const CoordConn& MD, const SolDetails& sol,
			std::vector<int>& bdsegments)
{
  bdsegments.clear();

  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double Ravg = 0.5*(sol.R0+sol.R1);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  // This is a boundary segment.
	  const int n = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n];
	  const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(r>Ravg)
	    // This segment is on the outer boundary
	    for(int a=0; a<2; ++a)
	      bdsegments.push_back( MD.connectivity[3*e+(f+a)%3] );
	}
  return;
}


// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, const SolDetails& sol,
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double Ravg = 0.5*(sol.R0+sol.R1);
  
  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  {
	    const int n = MD.connectivity[3*e+(f+i)%3]-1;
	    const double* X = &MD.coordinates[2*n];
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    if(r<Ravg)
	      bdnodes.insert(n);
	  }
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      bvalues.push_back( sol.GetExactSolution(&MD.coordinates[2*it]) );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const SolDetails& sol, const double* uvals)
{
  const int nElements = ElmArray.size();
  double Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  const double fval = sol.GetExactSolution(&Qpts[2*q]);
	  double fnum = 0.;
	  for(int a=0; a<3; ++a)
	    fnum += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return std::sqrt(Err);
}
