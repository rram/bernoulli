// Sriramajayam

#include <bp_Laplacian2DModule>
#include "PlottingUtils.h"
#include "MeshUtils.h"
#include "PetscData.h"
#include <cmath>
#include <algorithm>
#include <unordered_set>
#include <cassert>

using namespace bp;

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// Returns an equilateral mesh
void GetMesh(CoordConn& MD);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* sol);

// Function to compute body forces
bool BodyForceFunc(const std::vector<double> u, const std::vector<double> X,
		   std::vector<double>* f, std::vector<double>* df, void* params)
{ if(f->size()<1) f->resize(1);
  (*f)[0] = 4.0;
  return true; }

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh
  CoordConn MD;
  GetMesh(MD);
  PlotTecCoordConn("MD.tec", MD);

  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

    // Create scalar field, initialize to 0
  ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();
  
  // Create elements over the domain and Laplacian+forcing operations over them
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> OpArray(MD.elements);
  std::vector<BodyForceWork*> FrcOps(MD.elements);
  ForceFieldValue<2> Frc(BodyForceFunc);
  std::vector<int> Fields({0});
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      OpArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
      FrcOps[e] = new BodyForceWork(ElmArray[e], Frc, Fields, SMAccess);
    }

  // Local to global map
  StandardP12DMap L2GMap(ElmArray);
  
  // Assemblers
  StandardAssembler<Laplacian> AsmLap(OpArray, L2GMap);
  StandardAssembler<BodyForceWork> AsmFrc(FrcOps, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);
		    
  // Get boundary conditions
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, boundary, bvalues);

  // Update the state using the boundary conditions
  for(unsigned int i=0 ; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Assemble laplacian and forcing contributions
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  AsmFrc.Assemble(&UMap, PD.resVEC, false); // Don't zero the residual
  
  // BCs
  PD.SetDirichletBCs(boundary, bvalues);
  
  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Get the solution & update state
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);

  // Compute the L2 norm of the error
  double Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get()[0]));
  std::cout<<"\nError: "<<std::setprecision(8)<<Err;
  
  // Clean up
  PD.Destroy();
  for(auto& it:ElmArray) delete it;
  for(auto& it:OpArray) delete it;
  for(auto& it:FrcOps) delete it;
  PetscFinalize();
}  


void GetMesh(CoordConn& MD)
{
  MD.coordinates.reserve(7*2);
  MD.coordinates.push_back(0.);
  MD.coordinates.push_back(0.);
  for(auto i=0; i<6; ++i)
    {
      MD.coordinates.push_back(cos(double(i)*M_PI/3.));
      MD.coordinates.push_back(sin(double(i)*M_PI/3.));
    }
  std::vector<int> conn({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.connectivity = std::move(conn);
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 7;
  MD.elements = 6;
  int ndiv = 1;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(auto i=0; i<ndiv; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
}


// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(auto e=0; e<MD.elements; ++e)
    for(auto f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(auto i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      const auto& x = MD.coordinates[2*it];
      const auto& y = MD.coordinates[2*it+1];
      boundary.push_back( it );
      bvalues.push_back( x*x+y*y );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* sol)
{
  const auto nElements = ElmArray.size();
  double Err = 0.;
  for(unsigned int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const auto nQuad = Qwts.size();
      for(unsigned int q=0; q<nQuad; ++q)
	{
	  const double fval = Qpts[2*q]*Qpts[2*q] + Qpts[2*q+1]*Qpts[2*q+1];
	  double fnum = 0.;
	  for(auto a=0; a<3; ++a)
	    fnum += sol[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return sqrt(Err);
}
