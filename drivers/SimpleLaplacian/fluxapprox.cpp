// Sriramajayam

// Solves a laplacian problem with Dirichlet bcs
// Post-process the solution to recover continous and superconvergent boundary fluxes

#include <bp_Laplacian2DModule>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <map>
#include <cmath>
#include <algorithm>
#include <cassert>

using namespace bp;

// Helper struct for solution information
struct SolDetails
{
  static constexpr double R0 = 0.2;
  static constexpr double R1 = 0.7;
  // Returns the exact solution
  static double GetExactSolution(const double* X)
  { return std::exp(X[0])*std::sin(X[1]); }
  // Returns the normal derivative of the solution along the outer boundary
  static double GetOuterBdFlux(const double* X)
  { return std::exp(X[0])*(X[0]*std::sin(X[1])+X[1]*std::cos(X[1]))/R1; }
};


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD);

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* uvals);

// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals);

// Recover boundary fluxes
void RecoverBoundaryFluxes(const CoordConn& MD, const ScalarMap& UMap,
			   StandardAssembler<Laplacian>& LapAsm, PetscData& PD,
			   std::map<int, double>& rFluxes);

// Plot recovered boundary fluxes
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::map<int, double>& BdMap);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh over an annulus
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  ReadTecplotFile((char*)"r=0.7.msh", MD);

  // Subdivide
  int ndiv = 0;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    {
      SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      ProjectBdNodes(MD);
    }
  PlotTecCoordConn("MD.tec", MD);
  
  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create triangle elements over the domain & Laplacian operations over them
  std::vector<Element*> ElmArray(MD.elements);
  std::vector<Laplacian*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<1>(conn[0], conn[1], conn[2]);
      ScalarMapAccess SMAccess(std::vector<int>({conn[0]-1, conn[1]-1, conn[2]-1}));
      OpArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
    }
  StandardP12DMap L2GMap(ElmArray);

  // Create scalar field, initialize all values to zero
  ScalarMap UMap(MD.nodes);
  for(int n=0; n<MD.nodes; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();

  // Get Dirichlet bcs & update solution
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, boundary, bvalues);
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Assemblers
  StandardAssembler<Laplacian> AsmLap(OpArray, L2GMap);
  std::vector<int> nz;
  AsmLap.CountNonzeros(nz);
  
  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Assemble
  AsmLap.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  
  // Set boundary conditions
  PD.SetDirichletBCs(boundary, bvalues);

  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Update the solution
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<MD.nodes; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);
  PlotSolutionFluxes((char*)"uflux.dat", MD, ElmArray, L2GMap, &(UMap.Get()[0]));
		     
  // Compute the L2 norm of the error
  double Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get()[0]));
  std::cout<<"\nError: "<<Err<<std::flush;

  // Recover boundary fluxes
  std::map<int, double> rFluxes;
  RecoverBoundaryFluxes(MD, UMap, AsmLap, PD, rFluxes); // use 0-numbering for nodes
  PlotRecoveredFluxes((char*)"rflux.dat", MD, rFluxes);
  
  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:OpArray) delete it;
  PD.Destroy();
  PetscFinalize();
}  


// Project nodes of boundaries onto the annulus boundary
void ProjectBdNodes(CoordConn& MD)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  const double R0 = SolDetails::R0;
  const double R1 = SolDetails::R1;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int a=0; a<2; ++a)
	  {
	    const int n = MD.connectivity[3*e+(f+a)%3]-1;
	    const double X[] = {MD.coordinates[2*n], MD.coordinates[2*n+1]};
	    const double r = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	    const double R = (r<0.5*(R0+R1)) ? R0 : R1;
	    for(int k=0; k<2; ++k)
	      MD.coordinates[2*n+k] = X[k]*R/r;
	  }
  return;
}


// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  // Identify nodes on bounding faces
  std::set<int> bdnodes;
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  bdnodes.insert(MD.connectivity[3*e+(f+i)%3]-1);
  
  boundary.reserve(bdnodes.size());
  bvalues.reserve(bdnodes.size());
  for(auto& it:bdnodes)
    {
      boundary.push_back( it );
      bvalues.push_back( SolDetails::GetExactSolution(&MD.coordinates[2*it]) );
    }
  boundary.shrink_to_fit();
  bvalues.shrink_to_fit();
  return;
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* uvals)
{
  const int nElements = ElmArray.size();
  double Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  const double fval = SolDetails::GetExactSolution(&Qpts[2*q]);
	  double fnum = 0.;
	  for(int a=0; a<3; ++a)
	    fnum += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return std::sqrt(Err);
}


// Plot computed fluxes on the outer boundary
void PlotSolutionFluxes(const char* filename, const CoordConn& MD,
			const std::vector<Element*>& ElmArray,
			const LocalToGlobalMap& L2GMap, const double* uvals)
{
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  std::fstream pfile;
  pfile.open(filename, std::ios::out);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>Ravg)
	    {
	      // Compute nabla u
	      double du[] = {0.,0.};
	      for(int a=0; a<3; ++a)
		for(int i=0; i<2; ++i)
		  du[i] += uvals[L2GMap.Map(0,a,e)]*ElmArray[e]->GetDShape(0,0,a,i);

	      // Normal to this segment
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY =
		std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));
	      const double normal[] = {(Y[1]-X[1])/lenXY, -(Y[0]-X[0])/lenXY};

	      // du/dn
	      double duhdn = du[0]*normal[0] + du[1]*normal[1];

	      // Exact solution at X and Yhere
	      double dudnX = SolDetails::GetOuterBdFlux(X);
	      double dudnY = SolDetails::GetOuterBdFlux(Y);

	      // Angles
	      double thetaX = std::atan2(X[1],X[0]);
	      double thetaY = std::atan2(Y[1],Y[0]);

	      // Write to file
	      pfile<<thetaX<<" "<<dudnX<<" "<<duhdn<<"\n"
		   <<thetaY<<" "<<dudnY<<" "<<duhdn<<"\n";
	      pfile.flush();
	    }
	}
   pfile.close();
  return;
}
  
// Recover boundary fluxes
void RecoverBoundaryFluxes(const CoordConn& MD, const ScalarMap& UMap,
			   StandardAssembler<Laplacian>& LapAsm, PetscData& PD,
			   std::map<int, double>& rfluxes)
{
  // Useful aliases
  auto& rVec = PD.resVEC;
  auto& BdMap = rfluxes; 
  BdMap.clear();
  
  // Assemble the residual
  LapAsm.Assemble(&UMap, rVec);
  VecAssemblyBegin(rVec); VecAssemblyEnd(rVec);

  // Identify segments on the outer boundary and compute their lumped masses
  const double Ravg = 0.5*(SolDetails::R0+SolDetails::R1);
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  // Is this a segment on the outer boundary?
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const double* X = &MD.coordinates[2*n0];
	  const double rX = std::sqrt(X[0]*X[0]+X[1]*X[1]);
	  if(rX>Ravg)
	    {
	      // Yes. Update the masses
	      const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	      const double* Y = &MD.coordinates[2*n1];
	      const double lenXY = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));

	      // Update mass for node 0
	      auto it0 = BdMap.find(n0);
	      if(it0==BdMap.end()) BdMap.insert(std::make_pair(n0, 0.5*lenXY));
	      else it0->second += 0.5*lenXY;

	      // Update mass for node 1
	      auto it1 = BdMap.find(n1);
	      if(it1==BdMap.end()) BdMap.insert(std::make_pair(n1, 0.5*lenXY));
	      else it1->second += 0.5*lenXY;
	    }
	}

  // Recover fluxes as R/m
  for(auto& it:BdMap)
    {
      const int n = it.first;
      const double mass = it.second;
      double rval;
      VecGetValues(rVec, 1, &n, &rval);
      it.second = rval/mass;
    }
  return;
}

// Plot recovered fluxes in a file
void PlotRecoveredFluxes(const char* filename, const CoordConn& MD,
			 const std::map<int, double>& BdMap)
{
  // Plot the recovered boundary fluxes
  std::fstream pfile;
  pfile.open(filename, std::ios::out); assert(pfile.good());
  for(auto& it:BdMap)
    { 
      const int n = it.first;
      const double* X = &MD.coordinates[2*n];
      const double q = SolDetails::GetOuterBdFlux(X);
      const double qh = it.second;
      const double theta = std::atan2(X[1], X[0]);
      pfile<<theta<<" "<<q<<" "<<qh<<"\n";
      pfile.flush();
    }
  pfile.close();
  return;
}
  
