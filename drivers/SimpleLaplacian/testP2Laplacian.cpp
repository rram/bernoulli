// Sriramajayam

#include <bp_Laplacian2DModule>
#include <P22DElement.h>
#include <StandardP22DMap.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <map>

using namespace bp;

// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD, const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Returns an equilateral mesh
void GetMesh(CoordConn& MD);

// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap, const double* sol);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh
  CoordConn MD;
  GetMesh(MD);
  PlotTecCoordConn("MD.tec", MD);

  // Set global coordinates
  Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);

  // Create elements and operations
  std::vector<Element*> ElmArray(MD.elements);
  
  for(int e=0; e<MD.elements; ++e)
    {
      const auto* conn = &MD.connectivity[3*e]; 
      ElmArray[e] = new P22DElement<1>(conn[0], conn[1], conn[2]);
    }
  
  // Local to global map
  StandardP22DMap L2GMap(ElmArray);
  const int nTotalDofs = L2GMap.GetTotalNumDof();
  
  std::vector<Laplacian*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      std::vector<int> dofs(6);
      for(int a=0; a<6; ++a) dofs[a] = L2GMap.Map(0,a,e);
      ScalarMapAccess SMAccess(dofs);
      OpArray[e] = new Laplacian(ElmArray[e], SMAccess, 0);
    }
  
  // Create scalar field, initialize all values to zero
  ScalarMap UMap(nTotalDofs);
  for(int n=0; n<nTotalDofs; ++n)
    { double val = 0.; UMap.Set(n, &val); }
  UMap.SetInitialized();

  // Create assembler
  StandardAssembler<Laplacian> Asm(OpArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);

  // Create PETSc data structures
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Get boundary conditions
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, L2GMap, boundary, bvalues);

  // Boundary conditions
  for(unsigned int i=0; i<boundary.size(); ++i)
    UMap.Set(boundary[i], &bvalues[i]);
  UMap.SetInitialized();
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Assemble
  Asm.Assemble(&UMap, PD.resVEC, PD.stiffnessMAT);
  
  // Set boundary conditions
  PD.SetDirichletBCs(boundary, bvalues);
  
  // Solve
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  PD.Solve();

  // Update the solution
  double* inc;
  VecScale(PD.solutionVEC, -1.);
  VecGetArray(PD.solutionVEC, &inc);
  for(int n=0; n<nTotalDofs; ++n)
    UMap.Increment(n, &inc[n]);
  UMap.SetInitialized();
  VecRestoreArray(PD.solutionVEC, &inc);
  
  // Plot the solution
  PlotTecCoordConnWithNodalFields("sol.tec", MD, &(UMap.Get()[0]), 1);
  
  // Compute the L2 norm of the error
  double Err = ComputeL2Error(ElmArray, L2GMap, &(UMap.Get()[0]));
  std::cout<<"\nError: "<<Err<<std::flush;

  // Clean up
  for(auto& it:ElmArray) delete it;
  for(auto& it:OpArray) delete it;
  PD.Destroy();
  PetscFinalize();
}  


void GetMesh(CoordConn& MD)
{
  MD.coordinates.reserve(7*2);
  MD.coordinates.push_back(0.);
  MD.coordinates.push_back(0.);
  for(auto i=0; i<6; ++i)
    {
      MD.coordinates.push_back(cos(double(i)*M_PI/3.));
      MD.coordinates.push_back(sin(double(i)*M_PI/3.));
    }
  std::vector<int> conn({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.connectivity = std::move(conn);
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 7;
  MD.elements = 6;
  int ndiv = 1;
  PetscBool flag;
  PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, (char*)"-ndiv", &ndiv,  &flag);
  for(int i=0; i<ndiv; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
}


// Assign dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  // Get the element neighbor list
  std::vector<std::vector<int>> EN;
  GetCoordConnFaceNeighborList(MD, EN);

  boundary.clear();
  bvalues.clear();
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(EN[e][2*f]<0)
	{
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	  const double* X0 = &MD.coordinates[2*n0];
	  const double* X1 = &MD.coordinates[2*n1];
	  const double X2[] = {0.5*(X0[0]+X1[0]),
			       0.5*(X0[1]+X1[1])};
	  
	  int locdofs[] = {f, (f+1)%3, f+3};
	  double vals[] = {std::exp(X0[0])*std::sin(X0[1]),
			   std::exp(X1[0])*std::sin(X1[1]),
			   std::exp(X2[0])*std::sin(X2[1])};

	  for(int i=0; i<3; ++i)
	    bdofs2bvalues[L2GMap.Map(0,locdofs[i],e)] = vals[i];
	}
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }
}


// Compute the L2 norm of error
double ComputeL2Error(const std::vector<Element*>& ElmArray,
		      const LocalToGlobalMap& L2GMap,
		      const double* sol)
{
  const int nElements = ElmArray.size();
  double Err = 0.;
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      for(int q=0; q<nQuad; ++q)
	{
	  const double fval = exp(Qpts[2*q])*sin(Qpts[2*q+1]);
	  double fnum = 0.;
	  for(int a=0; a<6; ++a)
	    fnum += sol[L2GMap.Map(0,a,e)]*ElmArray[e]->GetShape(0,q,a);
	  Err += Qwts[q]*(fval-fnum)*(fval-fnum);
	}
    }
  return std::sqrt(Err);
}
